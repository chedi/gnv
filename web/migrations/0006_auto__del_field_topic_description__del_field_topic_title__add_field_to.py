# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Topic.description'
        db.delete_column(u'web_topic', 'description')

        # Deleting field 'Topic.title'
        db.delete_column(u'web_topic', 'title')

        # Adding field 'Topic.title_fr'
        db.add_column(u'web_topic', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'Topic.title_en'
        db.add_column(u'web_topic', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Topic.title_ar'
        db.add_column(u'web_topic', 'title_ar',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Topic.description_fr'
        db.add_column(u'web_topic', 'description_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Topic.description_en'
        db.add_column(u'web_topic', 'description_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Topic.description_ar'
        db.add_column(u'web_topic', 'description_ar',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Deal.description'
        db.delete_column(u'web_deal', 'description')

        # Deleting field 'Deal.title'
        db.delete_column(u'web_deal', 'title')

        # Deleting field 'Deal.caption'
        db.delete_column(u'web_deal', 'caption')

        # Adding field 'Deal.title_fr'
        db.add_column(u'web_deal', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'Deal.title_en'
        db.add_column(u'web_deal', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Deal.title_ar'
        db.add_column(u'web_deal', 'title_ar',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Deal.caption_fr'
        db.add_column(u'web_deal', 'caption_fr',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'Deal.caption_en'
        db.add_column(u'web_deal', 'caption_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Deal.caption_ar'
        db.add_column(u'web_deal', 'caption_ar',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Deal.description_fr'
        db.add_column(u'web_deal', 'description_fr',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'Deal.description_en'
        db.add_column(u'web_deal', 'description_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Deal.description_ar'
        db.add_column(u'web_deal', 'description_ar',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Deleting field 'TopicsCollection.title'
        db.delete_column(u'web_topicscollection', 'title')

        # Adding field 'TopicsCollection.title_fr'
        db.add_column(u'web_topicscollection', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'TopicsCollection.title_en'
        db.add_column(u'web_topicscollection', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TopicsCollection.title_ar'
        db.add_column(u'web_topicscollection', 'title_ar',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'News.caption'
        db.delete_column(u'web_news', 'caption')

        # Deleting field 'News.description'
        db.delete_column(u'web_news', 'description')

        # Deleting field 'News.title'
        db.delete_column(u'web_news', 'title')

        # Adding field 'News.title_fr'
        db.add_column(u'web_news', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'News.title_en'
        db.add_column(u'web_news', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'News.title_ar'
        db.add_column(u'web_news', 'title_ar',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'News.caption_fr'
        db.add_column(u'web_news', 'caption_fr',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'News.caption_en'
        db.add_column(u'web_news', 'caption_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'News.caption_ar'
        db.add_column(u'web_news', 'caption_ar',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'News.description_fr'
        db.add_column(u'web_news', 'description_fr',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'News.description_en'
        db.add_column(u'web_news', 'description_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'News.description_ar'
        db.add_column(u'web_news', 'description_ar',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Deleting field 'DealsCollection.title'
        db.delete_column(u'web_dealscollection', 'title')

        # Adding field 'DealsCollection.title_fr'
        db.add_column(u'web_dealscollection', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'DealsCollection.title_en'
        db.add_column(u'web_dealscollection', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'DealsCollection.title_ar'
        db.add_column(u'web_dealscollection', 'title_ar',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Topic.description'
        db.add_column(u'web_topic', 'description',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Topic.title'
        raise RuntimeError("Cannot reverse this migration. 'Topic.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Topic.title'
        db.add_column(u'web_topic', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=200),
                      keep_default=False)

        # Deleting field 'Topic.title_fr'
        db.delete_column(u'web_topic', 'title_fr')

        # Deleting field 'Topic.title_en'
        db.delete_column(u'web_topic', 'title_en')

        # Deleting field 'Topic.title_ar'
        db.delete_column(u'web_topic', 'title_ar')

        # Deleting field 'Topic.description_fr'
        db.delete_column(u'web_topic', 'description_fr')

        # Deleting field 'Topic.description_en'
        db.delete_column(u'web_topic', 'description_en')

        # Deleting field 'Topic.description_ar'
        db.delete_column(u'web_topic', 'description_ar')


        # User chose to not deal with backwards NULL issues for 'Deal.description'
        raise RuntimeError("Cannot reverse this migration. 'Deal.description' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Deal.description'
        db.add_column(u'web_deal', 'description',
                      self.gf('django.db.models.fields.TextField')(),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Deal.title'
        raise RuntimeError("Cannot reverse this migration. 'Deal.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Deal.title'
        db.add_column(u'web_deal', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=200),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Deal.caption'
        raise RuntimeError("Cannot reverse this migration. 'Deal.caption' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Deal.caption'
        db.add_column(u'web_deal', 'caption',
                      self.gf('django.db.models.fields.CharField')(max_length=200),
                      keep_default=False)

        # Deleting field 'Deal.title_fr'
        db.delete_column(u'web_deal', 'title_fr')

        # Deleting field 'Deal.title_en'
        db.delete_column(u'web_deal', 'title_en')

        # Deleting field 'Deal.title_ar'
        db.delete_column(u'web_deal', 'title_ar')

        # Deleting field 'Deal.caption_fr'
        db.delete_column(u'web_deal', 'caption_fr')

        # Deleting field 'Deal.caption_en'
        db.delete_column(u'web_deal', 'caption_en')

        # Deleting field 'Deal.caption_ar'
        db.delete_column(u'web_deal', 'caption_ar')

        # Deleting field 'Deal.description_fr'
        db.delete_column(u'web_deal', 'description_fr')

        # Deleting field 'Deal.description_en'
        db.delete_column(u'web_deal', 'description_en')

        # Deleting field 'Deal.description_ar'
        db.delete_column(u'web_deal', 'description_ar')


        # User chose to not deal with backwards NULL issues for 'TopicsCollection.title'
        raise RuntimeError("Cannot reverse this migration. 'TopicsCollection.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'TopicsCollection.title'
        db.add_column(u'web_topicscollection', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=200),
                      keep_default=False)

        # Deleting field 'TopicsCollection.title_fr'
        db.delete_column(u'web_topicscollection', 'title_fr')

        # Deleting field 'TopicsCollection.title_en'
        db.delete_column(u'web_topicscollection', 'title_en')

        # Deleting field 'TopicsCollection.title_ar'
        db.delete_column(u'web_topicscollection', 'title_ar')


        # User chose to not deal with backwards NULL issues for 'News.caption'
        raise RuntimeError("Cannot reverse this migration. 'News.caption' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'News.caption'
        db.add_column(u'web_news', 'caption',
                      self.gf('django.db.models.fields.CharField')(max_length=200),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'News.description'
        raise RuntimeError("Cannot reverse this migration. 'News.description' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'News.description'
        db.add_column(u'web_news', 'description',
                      self.gf('django.db.models.fields.TextField')(),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'News.title'
        raise RuntimeError("Cannot reverse this migration. 'News.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'News.title'
        db.add_column(u'web_news', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=200),
                      keep_default=False)

        # Deleting field 'News.title_fr'
        db.delete_column(u'web_news', 'title_fr')

        # Deleting field 'News.title_en'
        db.delete_column(u'web_news', 'title_en')

        # Deleting field 'News.title_ar'
        db.delete_column(u'web_news', 'title_ar')

        # Deleting field 'News.caption_fr'
        db.delete_column(u'web_news', 'caption_fr')

        # Deleting field 'News.caption_en'
        db.delete_column(u'web_news', 'caption_en')

        # Deleting field 'News.caption_ar'
        db.delete_column(u'web_news', 'caption_ar')

        # Deleting field 'News.description_fr'
        db.delete_column(u'web_news', 'description_fr')

        # Deleting field 'News.description_en'
        db.delete_column(u'web_news', 'description_en')

        # Deleting field 'News.description_ar'
        db.delete_column(u'web_news', 'description_ar')


        # User chose to not deal with backwards NULL issues for 'DealsCollection.title'
        raise RuntimeError("Cannot reverse this migration. 'DealsCollection.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'DealsCollection.title'
        db.add_column(u'web_dealscollection', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=200),
                      keep_default=False)

        # Deleting field 'DealsCollection.title_fr'
        db.delete_column(u'web_dealscollection', 'title_fr')

        # Deleting field 'DealsCollection.title_en'
        db.delete_column(u'web_dealscollection', 'title_en')

        # Deleting field 'DealsCollection.title_ar'
        db.delete_column(u'web_dealscollection', 'title_ar')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'filer.file': {
            'Meta': {'object_name': 'File'},
            '_file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'all_files'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'has_all_mandatory_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'original_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'owned_files'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_filer.file_set'", 'null': 'True', 'to': u"orm['contenttypes.ContentType']"}),
            'sha1': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40', 'blank': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.folder': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('parent', 'name'),)", 'object_name': 'Folder'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'filer_owned_folders'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['filer.File']},
            '_height': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            '_width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'default_caption': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'file_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['filer.File']", 'unique': 'True', 'primary_key': 'True'}),
            'must_always_publish_author_credit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'must_always_publish_copyright': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject_location': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'web.deal': {
            'Meta': {'object_name': 'Deal'},
            'caption_ar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'caption_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'caption_fr': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'collection': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.DealsCollection']"}),
            'description_ar': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fr': ('django.db.models.fields.TextField', [], {}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['filer.Image']"}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sidebar': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'title_ar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'web.dealscollection': {
            'Meta': {'object_name': 'DealsCollection'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_ar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'web.dealspluginmodel': {
            'Meta': {'object_name': 'DealsPluginModel', 'db_table': "u'cmsplugin_dealspluginmodel'", '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'deals': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.DealsCollection']"})
        },
        u'web.news': {
            'Meta': {'object_name': 'News'},
            'caption_ar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'caption_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'caption_fr': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'description_ar': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fr': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['filer.Image']"}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sidebar': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title_ar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'web.newspluginmodel': {
            'Meta': {'object_name': 'NewsPluginModel', 'db_table': "u'cmsplugin_newspluginmodel'", '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'web.topic': {
            'Meta': {'object_name': 'Topic'},
            'collection': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.TopicsCollection']"}),
            'description_ar': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['filer.Image']"}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title_ar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'})
        },
        u'web.topicpluginmodel': {
            'Meta': {'object_name': 'TopicPluginModel', 'db_table': "u'cmsplugin_topicpluginmodel'", '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'topics': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.TopicsCollection']"})
        },
        u'web.topicscollection': {
            'Meta': {'object_name': 'TopicsCollection'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_ar': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['web']