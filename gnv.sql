--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 141, true);


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: cms_cmsplugin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_cmsplugin (
    id integer NOT NULL,
    placeholder_id integer,
    parent_id integer,
    "position" smallint,
    language character varying(15) NOT NULL,
    plugin_type character varying(50) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    changed_date timestamp with time zone NOT NULL,
    level integer NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    CONSTRAINT cms_cmsplugin_level_check CHECK ((level >= 0)),
    CONSTRAINT cms_cmsplugin_lft_check CHECK ((lft >= 0)),
    CONSTRAINT cms_cmsplugin_position_check CHECK (("position" >= 0)),
    CONSTRAINT cms_cmsplugin_rght_check CHECK ((rght >= 0)),
    CONSTRAINT cms_cmsplugin_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.cms_cmsplugin OWNER TO postgres;

--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_cmsplugin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_cmsplugin_id_seq OWNER TO postgres;

--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_cmsplugin_id_seq OWNED BY cms_cmsplugin.id;


--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_cmsplugin_id_seq', 484, true);


--
-- Name: cms_globalpagepermission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_globalpagepermission (
    id integer NOT NULL,
    user_id integer,
    group_id integer,
    can_change boolean NOT NULL,
    can_add boolean NOT NULL,
    can_delete boolean NOT NULL,
    can_change_advanced_settings boolean NOT NULL,
    can_publish boolean NOT NULL,
    can_change_permissions boolean NOT NULL,
    can_move_page boolean NOT NULL,
    can_view boolean NOT NULL,
    can_recover_page boolean NOT NULL
);


ALTER TABLE public.cms_globalpagepermission OWNER TO postgres;

--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_globalpagepermission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_globalpagepermission_id_seq OWNER TO postgres;

--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_globalpagepermission_id_seq OWNED BY cms_globalpagepermission.id;


--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_globalpagepermission_id_seq', 1, false);


--
-- Name: cms_globalpagepermission_sites; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_globalpagepermission_sites (
    id integer NOT NULL,
    globalpagepermission_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.cms_globalpagepermission_sites OWNER TO postgres;

--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_globalpagepermission_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_globalpagepermission_sites_id_seq OWNER TO postgres;

--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_globalpagepermission_sites_id_seq OWNED BY cms_globalpagepermission_sites.id;


--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_globalpagepermission_sites_id_seq', 1, false);


--
-- Name: cms_page; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_page (
    id integer NOT NULL,
    created_by character varying(70) NOT NULL,
    changed_by character varying(70) NOT NULL,
    parent_id integer,
    creation_date timestamp with time zone NOT NULL,
    changed_date timestamp with time zone NOT NULL,
    publication_date timestamp with time zone,
    publication_end_date timestamp with time zone,
    in_navigation boolean NOT NULL,
    soft_root boolean NOT NULL,
    reverse_id character varying(40),
    navigation_extenders character varying(80),
    published boolean NOT NULL,
    template character varying(100) NOT NULL,
    site_id integer NOT NULL,
    level integer NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    login_required boolean NOT NULL,
    limit_visibility_in_menu smallint,
    publisher_is_draft boolean NOT NULL,
    publisher_public_id integer,
    publisher_state smallint NOT NULL,
    moderator_state integer,
    CONSTRAINT cms_page_level_check CHECK ((level >= 0)),
    CONSTRAINT cms_page_lft_check CHECK ((lft >= 0)),
    CONSTRAINT cms_page_rght_check CHECK ((rght >= 0)),
    CONSTRAINT cms_page_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.cms_page OWNER TO postgres;

--
-- Name: cms_page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_page_id_seq OWNER TO postgres;

--
-- Name: cms_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_page_id_seq OWNED BY cms_page.id;


--
-- Name: cms_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_page_id_seq', 20, true);


--
-- Name: cms_page_placeholders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_page_placeholders (
    id integer NOT NULL,
    page_id integer NOT NULL,
    placeholder_id integer NOT NULL
);


ALTER TABLE public.cms_page_placeholders OWNER TO postgres;

--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_page_placeholders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_page_placeholders_id_seq OWNER TO postgres;

--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_page_placeholders_id_seq OWNED BY cms_page_placeholders.id;


--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_page_placeholders_id_seq', 68, true);


--
-- Name: cms_pagemoderatorstate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_pagemoderatorstate (
    id integer NOT NULL,
    page_id integer NOT NULL,
    user_id integer,
    created timestamp with time zone NOT NULL,
    action character varying(3),
    message text NOT NULL
);


ALTER TABLE public.cms_pagemoderatorstate OWNER TO postgres;

--
-- Name: cms_pagemoderatorstate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_pagemoderatorstate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_pagemoderatorstate_id_seq OWNER TO postgres;

--
-- Name: cms_pagemoderatorstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_pagemoderatorstate_id_seq OWNED BY cms_pagemoderatorstate.id;


--
-- Name: cms_pagemoderatorstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_pagemoderatorstate_id_seq', 143, true);


--
-- Name: cms_pagepermission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_pagepermission (
    id integer NOT NULL,
    user_id integer,
    group_id integer,
    can_change boolean NOT NULL,
    can_add boolean NOT NULL,
    can_delete boolean NOT NULL,
    can_change_advanced_settings boolean NOT NULL,
    can_publish boolean NOT NULL,
    can_change_permissions boolean NOT NULL,
    can_move_page boolean NOT NULL,
    can_view boolean NOT NULL,
    grant_on integer NOT NULL,
    page_id integer
);


ALTER TABLE public.cms_pagepermission OWNER TO postgres;

--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_pagepermission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_pagepermission_id_seq OWNER TO postgres;

--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_pagepermission_id_seq OWNED BY cms_pagepermission.id;


--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_pagepermission_id_seq', 1, false);


--
-- Name: cms_pageuser; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_pageuser (
    user_ptr_id integer NOT NULL,
    created_by_id integer NOT NULL
);


ALTER TABLE public.cms_pageuser OWNER TO postgres;

--
-- Name: cms_pageusergroup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_pageusergroup (
    group_ptr_id integer NOT NULL,
    created_by_id integer NOT NULL
);


ALTER TABLE public.cms_pageusergroup OWNER TO postgres;

--
-- Name: cms_placeholder; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_placeholder (
    id integer NOT NULL,
    slot character varying(50) NOT NULL,
    default_width smallint,
    CONSTRAINT cms_placeholder_default_width_check CHECK ((default_width >= 0))
);


ALTER TABLE public.cms_placeholder OWNER TO postgres;

--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_placeholder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_placeholder_id_seq OWNER TO postgres;

--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_placeholder_id_seq OWNED BY cms_placeholder.id;


--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_placeholder_id_seq', 68, true);


--
-- Name: cms_title; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_title (
    id integer NOT NULL,
    language character varying(15) NOT NULL,
    title character varying(255) NOT NULL,
    menu_title character varying(255),
    slug character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    has_url_overwrite boolean NOT NULL,
    application_urls character varying(200),
    redirect character varying(255),
    meta_description text,
    meta_keywords character varying(255),
    page_title character varying(255),
    page_id integer NOT NULL,
    creation_date timestamp with time zone NOT NULL
);


ALTER TABLE public.cms_title OWNER TO postgres;

--
-- Name: cms_title_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_title_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cms_title_id_seq OWNER TO postgres;

--
-- Name: cms_title_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_title_id_seq OWNED BY cms_title.id;


--
-- Name: cms_title_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_title_id_seq', 54, true);


--
-- Name: cmsplugin_dealspluginmodel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_dealspluginmodel (
    cmsplugin_ptr_id integer NOT NULL,
    deals_id integer NOT NULL
);


ALTER TABLE public.cmsplugin_dealspluginmodel OWNER TO postgres;

--
-- Name: cmsplugin_flash; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_flash (
    cmsplugin_ptr_id integer NOT NULL,
    file character varying(100) NOT NULL,
    width character varying(6) NOT NULL,
    height character varying(6) NOT NULL
);


ALTER TABLE public.cmsplugin_flash OWNER TO postgres;

--
-- Name: cmsplugin_googlemap; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_googlemap (
    cmsplugin_ptr_id integer NOT NULL,
    title character varying(100),
    address character varying(150) NOT NULL,
    zipcode character varying(30) NOT NULL,
    city character varying(100) NOT NULL,
    content character varying(255) NOT NULL,
    zoom smallint NOT NULL,
    lat numeric(10,6),
    lng numeric(10,6),
    route_planer_title character varying(150),
    route_planer boolean NOT NULL,
    width character varying(6) NOT NULL,
    height character varying(6) NOT NULL,
    CONSTRAINT cmsplugin_googlemap_zoom_check CHECK ((zoom >= 0))
);


ALTER TABLE public.cmsplugin_googlemap OWNER TO postgres;

--
-- Name: cmsplugin_link; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_link (
    cmsplugin_ptr_id integer NOT NULL,
    name character varying(256) NOT NULL,
    url character varying(200),
    page_link_id integer,
    mailto character varying(75),
    target character varying(100) NOT NULL
);


ALTER TABLE public.cmsplugin_link OWNER TO postgres;

--
-- Name: cmsplugin_newspluginmodel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_newspluginmodel (
    cmsplugin_ptr_id integer NOT NULL,
    title character varying(200) NOT NULL
);


ALTER TABLE public.cmsplugin_newspluginmodel OWNER TO postgres;

--
-- Name: cmsplugin_picture; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_picture (
    cmsplugin_ptr_id integer NOT NULL,
    image character varying(100) NOT NULL,
    url character varying(255),
    page_link_id integer,
    alt character varying(255),
    longdesc character varying(255),
    "float" character varying(10)
);


ALTER TABLE public.cmsplugin_picture OWNER TO postgres;

--
-- Name: cmsplugin_snippetptr; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_snippetptr (
    cmsplugin_ptr_id integer NOT NULL,
    snippet_id integer NOT NULL
);


ALTER TABLE public.cmsplugin_snippetptr OWNER TO postgres;

--
-- Name: cmsplugin_text; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_text (
    cmsplugin_ptr_id integer NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.cmsplugin_text OWNER TO postgres;

--
-- Name: cmsplugin_topicpluginmodel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_topicpluginmodel (
    cmsplugin_ptr_id integer NOT NULL,
    topics_id integer NOT NULL
);


ALTER TABLE public.cmsplugin_topicpluginmodel OWNER TO postgres;

--
-- Name: cmsplugin_twitterrecententries; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_twitterrecententries (
    cmsplugin_ptr_id integer NOT NULL,
    title character varying(75) NOT NULL,
    twitter_user character varying(75) NOT NULL,
    count smallint NOT NULL,
    link_hint character varying(75) NOT NULL,
    CONSTRAINT cmsplugin_twitterrecententries_count_check CHECK ((count >= 0))
);


ALTER TABLE public.cmsplugin_twitterrecententries OWNER TO postgres;

--
-- Name: cmsplugin_twittersearch; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cmsplugin_twittersearch (
    cmsplugin_ptr_id integer NOT NULL,
    title character varying(75) NOT NULL,
    query character varying(200) NOT NULL,
    count smallint NOT NULL,
    CONSTRAINT cmsplugin_twittersearch_count_check CHECK ((count >= 0))
);


ALTER TABLE public.cmsplugin_twittersearch OWNER TO postgres;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 263, true);


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 46, true);


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Name: easy_thumbnails_source; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE easy_thumbnails_source (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    storage_hash character varying(40) NOT NULL
);


ALTER TABLE public.easy_thumbnails_source OWNER TO postgres;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE easy_thumbnails_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_source_id_seq OWNER TO postgres;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE easy_thumbnails_source_id_seq OWNED BY easy_thumbnails_source.id;


--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('easy_thumbnails_source_id_seq', 95, true);


--
-- Name: easy_thumbnails_thumbnail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE easy_thumbnails_thumbnail (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    source_id integer NOT NULL,
    storage_hash character varying(40) NOT NULL
);


ALTER TABLE public.easy_thumbnails_thumbnail OWNER TO postgres;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE easy_thumbnails_thumbnail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_thumbnail_id_seq OWNER TO postgres;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE easy_thumbnails_thumbnail_id_seq OWNED BY easy_thumbnails_thumbnail.id;


--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('easy_thumbnails_thumbnail_id_seq', 314, true);


--
-- Name: filer_clipboard; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filer_clipboard (
    id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.filer_clipboard OWNER TO postgres;

--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_clipboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.filer_clipboard_id_seq OWNER TO postgres;

--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_clipboard_id_seq OWNED BY filer_clipboard.id;


--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_clipboard_id_seq', 1, true);


--
-- Name: filer_clipboarditem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filer_clipboarditem (
    id integer NOT NULL,
    file_id integer NOT NULL,
    clipboard_id integer NOT NULL
);


ALTER TABLE public.filer_clipboarditem OWNER TO postgres;

--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_clipboarditem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.filer_clipboarditem_id_seq OWNER TO postgres;

--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_clipboarditem_id_seq OWNED BY filer_clipboarditem.id;


--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_clipboarditem_id_seq', 66, true);


--
-- Name: filer_file; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filer_file (
    id integer NOT NULL,
    folder_id integer,
    file character varying(255),
    _file_size integer,
    has_all_mandatory_data boolean NOT NULL,
    original_filename character varying(255),
    name character varying(255) NOT NULL,
    owner_id integer,
    uploaded_at timestamp with time zone NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    description text,
    is_public boolean NOT NULL,
    sha1 character varying(40) NOT NULL,
    polymorphic_ctype_id integer
);


ALTER TABLE public.filer_file OWNER TO postgres;

--
-- Name: filer_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.filer_file_id_seq OWNER TO postgres;

--
-- Name: filer_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_file_id_seq OWNED BY filer_file.id;


--
-- Name: filer_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_file_id_seq', 62, true);


--
-- Name: filer_folder; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filer_folder (
    id integer NOT NULL,
    parent_id integer,
    name character varying(255) NOT NULL,
    owner_id integer,
    uploaded_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    CONSTRAINT filer_folder_level_check CHECK ((level >= 0)),
    CONSTRAINT filer_folder_lft_check CHECK ((lft >= 0)),
    CONSTRAINT filer_folder_rght_check CHECK ((rght >= 0)),
    CONSTRAINT filer_folder_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.filer_folder OWNER TO postgres;

--
-- Name: filer_folder_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_folder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.filer_folder_id_seq OWNER TO postgres;

--
-- Name: filer_folder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_folder_id_seq OWNED BY filer_folder.id;


--
-- Name: filer_folder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_folder_id_seq', 15, true);


--
-- Name: filer_folderpermission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filer_folderpermission (
    id integer NOT NULL,
    folder_id integer,
    type smallint NOT NULL,
    user_id integer,
    group_id integer,
    everybody boolean NOT NULL,
    can_edit smallint,
    can_read smallint,
    can_add_children smallint
);


ALTER TABLE public.filer_folderpermission OWNER TO postgres;

--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_folderpermission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.filer_folderpermission_id_seq OWNER TO postgres;

--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_folderpermission_id_seq OWNED BY filer_folderpermission.id;


--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_folderpermission_id_seq', 1, false);


--
-- Name: filer_image; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filer_image (
    file_ptr_id integer NOT NULL,
    _height integer,
    _width integer,
    date_taken timestamp with time zone,
    default_alt_text character varying(255),
    default_caption character varying(255),
    author character varying(255),
    must_always_publish_author_credit boolean NOT NULL,
    must_always_publish_copyright boolean NOT NULL,
    subject_location character varying(64)
);


ALTER TABLE public.filer_image OWNER TO postgres;

--
-- Name: menus_cachekey; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE menus_cachekey (
    id integer NOT NULL,
    language character varying(255) NOT NULL,
    site integer NOT NULL,
    key character varying(255) NOT NULL,
    CONSTRAINT menus_cachekey_site_check CHECK ((site >= 0))
);


ALTER TABLE public.menus_cachekey OWNER TO postgres;

--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE menus_cachekey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.menus_cachekey_id_seq OWNER TO postgres;

--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE menus_cachekey_id_seq OWNED BY menus_cachekey.id;


--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('menus_cachekey_id_seq', 121, true);


--
-- Name: snippet_snippet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE snippet_snippet (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    html text NOT NULL,
    template character varying(50) NOT NULL
);


ALTER TABLE public.snippet_snippet OWNER TO postgres;

--
-- Name: snippet_snippet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE snippet_snippet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.snippet_snippet_id_seq OWNER TO postgres;

--
-- Name: snippet_snippet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE snippet_snippet_id_seq OWNED BY snippet_snippet.id;


--
-- Name: snippet_snippet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('snippet_snippet_id_seq', 3, true);


--
-- Name: south_migrationhistory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.south_migrationhistory OWNER TO postgres;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE south_migrationhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.south_migrationhistory_id_seq OWNER TO postgres;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE south_migrationhistory_id_seq OWNED BY south_migrationhistory.id;


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('south_migrationhistory_id_seq', 129, true);


--
-- Name: web_deal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE web_deal (
    id integer NOT NULL,
    url character varying(300) NOT NULL,
    price integer NOT NULL,
    end_date date,
    start_date date,
    image_id integer NOT NULL,
    collection_id integer NOT NULL,
    published boolean NOT NULL,
    sidebar boolean NOT NULL,
    title_fr character varying(200) NOT NULL,
    title_en character varying(200),
    title_ar character varying(200),
    caption_fr character varying(200) NOT NULL,
    caption_en character varying(200),
    caption_ar character varying(200),
    description_fr text NOT NULL,
    description_en text,
    description_ar text,
    CONSTRAINT web_deal_price_check CHECK ((price >= 0))
);


ALTER TABLE public.web_deal OWNER TO postgres;

--
-- Name: web_deal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE web_deal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.web_deal_id_seq OWNER TO postgres;

--
-- Name: web_deal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE web_deal_id_seq OWNED BY web_deal.id;


--
-- Name: web_deal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('web_deal_id_seq', 4, true);


--
-- Name: web_dealscollection; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE web_dealscollection (
    id integer NOT NULL,
    title_fr character varying(200) NOT NULL,
    title_en character varying(200),
    title_ar character varying(200)
);


ALTER TABLE public.web_dealscollection OWNER TO postgres;

--
-- Name: web_dealscollection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE web_dealscollection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.web_dealscollection_id_seq OWNER TO postgres;

--
-- Name: web_dealscollection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE web_dealscollection_id_seq OWNED BY web_dealscollection.id;


--
-- Name: web_dealscollection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('web_dealscollection_id_seq', 1, true);


--
-- Name: web_news; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE web_news (
    id integer NOT NULL,
    url character varying(300) NOT NULL,
    image_id integer NOT NULL,
    published boolean NOT NULL,
    sidebar boolean NOT NULL,
    title_fr character varying(200) NOT NULL,
    title_en character varying(200),
    title_ar character varying(200),
    caption_fr character varying(200) NOT NULL,
    caption_en character varying(200),
    caption_ar character varying(200),
    description_fr text NOT NULL,
    description_en text,
    description_ar text
);


ALTER TABLE public.web_news OWNER TO postgres;

--
-- Name: web_news_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE web_news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.web_news_id_seq OWNER TO postgres;

--
-- Name: web_news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE web_news_id_seq OWNED BY web_news.id;


--
-- Name: web_news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('web_news_id_seq', 9, true);


--
-- Name: web_topic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE web_topic (
    id integer NOT NULL,
    url character varying(300),
    image_id integer NOT NULL,
    collection_id integer NOT NULL,
    published boolean NOT NULL,
    title_fr character varying(200) NOT NULL,
    title_en character varying(200),
    title_ar character varying(200),
    description_fr text,
    description_en text,
    description_ar text
);


ALTER TABLE public.web_topic OWNER TO postgres;

--
-- Name: web_topic_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE web_topic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.web_topic_id_seq OWNER TO postgres;

--
-- Name: web_topic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE web_topic_id_seq OWNED BY web_topic.id;


--
-- Name: web_topic_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('web_topic_id_seq', 45, true);


--
-- Name: web_topicscollection; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE web_topicscollection (
    id integer NOT NULL,
    title_fr character varying(200) NOT NULL,
    title_en character varying(200),
    title_ar character varying(200)
);


ALTER TABLE public.web_topicscollection OWNER TO postgres;

--
-- Name: web_topicscollection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE web_topicscollection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.web_topicscollection_id_seq OWNER TO postgres;

--
-- Name: web_topicscollection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE web_topicscollection_id_seq OWNED BY web_topicscollection.id;


--
-- Name: web_topicscollection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('web_topicscollection_id_seq', 7, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin ALTER COLUMN id SET DEFAULT nextval('cms_cmsplugin_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission ALTER COLUMN id SET DEFAULT nextval('cms_globalpagepermission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites ALTER COLUMN id SET DEFAULT nextval('cms_globalpagepermission_sites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page ALTER COLUMN id SET DEFAULT nextval('cms_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders ALTER COLUMN id SET DEFAULT nextval('cms_page_placeholders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagemoderatorstate ALTER COLUMN id SET DEFAULT nextval('cms_pagemoderatorstate_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission ALTER COLUMN id SET DEFAULT nextval('cms_pagepermission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_placeholder ALTER COLUMN id SET DEFAULT nextval('cms_placeholder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title ALTER COLUMN id SET DEFAULT nextval('cms_title_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_source ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_source_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnail ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_thumbnail_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboard ALTER COLUMN id SET DEFAULT nextval('filer_clipboard_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboarditem ALTER COLUMN id SET DEFAULT nextval('filer_clipboarditem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file ALTER COLUMN id SET DEFAULT nextval('filer_file_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder ALTER COLUMN id SET DEFAULT nextval('filer_folder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission ALTER COLUMN id SET DEFAULT nextval('filer_folderpermission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menus_cachekey ALTER COLUMN id SET DEFAULT nextval('menus_cachekey_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY snippet_snippet ALTER COLUMN id SET DEFAULT nextval('snippet_snippet_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('south_migrationhistory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_deal ALTER COLUMN id SET DEFAULT nextval('web_deal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_dealscollection ALTER COLUMN id SET DEFAULT nextval('web_dealscollection_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_news ALTER COLUMN id SET DEFAULT nextval('web_news_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_topic ALTER COLUMN id SET DEFAULT nextval('web_topic_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_topicscollection ALTER COLUMN id SET DEFAULT nextval('web_topicscollection_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add site	6	add_site
17	Can change site	6	change_site
18	Can delete site	6	delete_site
19	Can add migration history	7	add_migrationhistory
20	Can change migration history	7	change_migrationhistory
21	Can delete migration history	7	delete_migrationhistory
22	Can add log entry	8	add_logentry
23	Can change log entry	8	change_logentry
24	Can delete log entry	8	delete_logentry
25	Can add Folder	9	add_folder
26	Can change Folder	9	change_folder
27	Can delete Folder	9	delete_folder
28	Can use directory listing	9	can_use_directory_listing
29	Can add folder permission	10	add_folderpermission
30	Can change folder permission	10	change_folderpermission
31	Can delete folder permission	10	delete_folderpermission
32	Can add file	11	add_file
33	Can change file	11	change_file
34	Can delete file	11	delete_file
35	Can add clipboard	12	add_clipboard
36	Can change clipboard	12	change_clipboard
37	Can delete clipboard	12	delete_clipboard
38	Can add clipboard item	13	add_clipboarditem
39	Can change clipboard item	13	change_clipboarditem
40	Can delete clipboard item	13	delete_clipboarditem
41	Can add image	14	add_image
42	Can change image	14	change_image
43	Can delete image	14	delete_image
44	Can publish Page	15	publish_page
45	Can add placeholder	16	add_placeholder
46	Can change placeholder	16	change_placeholder
47	Can delete placeholder	16	delete_placeholder
48	Can add cms plugin	17	add_cmsplugin
49	Can change cms plugin	17	change_cmsplugin
50	Can delete cms plugin	17	delete_cmsplugin
51	Can add page	15	add_page
52	Can change page	15	change_page
53	Can delete page	15	delete_page
54	Can view page	15	view_page
55	Can add Page moderator state	18	add_pagemoderatorstate
56	Can change Page moderator state	18	change_pagemoderatorstate
57	Can delete Page moderator state	18	delete_pagemoderatorstate
58	Can add Page global permission	19	add_globalpagepermission
59	Can change Page global permission	19	change_globalpagepermission
60	Can delete Page global permission	19	delete_globalpagepermission
61	Can add Page permission	20	add_pagepermission
62	Can change Page permission	20	change_pagepermission
63	Can delete Page permission	20	delete_pagepermission
64	Can add User (page)	21	add_pageuser
65	Can change User (page)	21	change_pageuser
66	Can delete User (page)	21	delete_pageuser
67	Can add User group (page)	22	add_pageusergroup
68	Can change User group (page)	22	change_pageusergroup
69	Can delete User group (page)	22	delete_pageusergroup
70	Can add title	23	add_title
71	Can change title	23	change_title
72	Can delete title	23	delete_title
73	Can add deals collection	24	add_dealscollection
74	Can change deals collection	24	change_dealscollection
75	Can delete deals collection	24	delete_dealscollection
76	Can add topics collection	25	add_topicscollection
77	Can change topics collection	25	change_topicscollection
78	Can delete topics collection	25	delete_topicscollection
79	Can add deal	26	add_deal
80	Can change deal	26	change_deal
81	Can delete deal	26	delete_deal
82	Can add topic	27	add_topic
83	Can change topic	27	change_topic
84	Can delete topic	27	delete_topic
85	Can add deals plugin model	28	add_dealspluginmodel
86	Can change deals plugin model	28	change_dealspluginmodel
87	Can delete deals plugin model	28	delete_dealspluginmodel
88	Can add topic plugin model	29	add_topicpluginmodel
89	Can change topic plugin model	29	change_topicpluginmodel
90	Can delete topic plugin model	29	delete_topicpluginmodel
91	Can add news	30	add_news
92	Can change news	30	change_news
93	Can delete news	30	delete_news
94	Can add news plugin model	31	add_newspluginmodel
95	Can change news plugin model	31	change_newspluginmodel
96	Can delete news plugin model	31	delete_newspluginmodel
97	Can add cache key	32	add_cachekey
98	Can change cache key	32	change_cachekey
99	Can delete cache key	32	delete_cachekey
100	Can add revision	33	add_revision
101	Can change revision	33	change_revision
102	Can delete revision	33	delete_revision
103	Can add version	34	add_version
104	Can change version	34	change_version
105	Can delete version	34	delete_version
106	Can add source	35	add_source
107	Can change source	35	change_source
108	Can delete source	35	delete_source
109	Can add thumbnail	36	add_thumbnail
110	Can change thumbnail	36	change_thumbnail
111	Can delete thumbnail	36	delete_thumbnail
112	Can add link	37	add_link
113	Can change link	37	change_link
114	Can delete link	37	delete_link
115	Can add text	38	add_text
116	Can change text	38	change_text
117	Can delete text	38	delete_text
118	Can add flash	39	add_flash
119	Can change flash	39	change_flash
120	Can delete flash	39	delete_flash
121	Can add picture	40	add_picture
122	Can change picture	40	change_picture
123	Can delete picture	40	delete_picture
124	Can add twitter recent entries	41	add_twitterrecententries
125	Can change twitter recent entries	41	change_twitterrecententries
126	Can delete twitter recent entries	41	delete_twitterrecententries
127	Can add twitter search	42	add_twittersearch
128	Can change twitter search	42	change_twittersearch
129	Can delete twitter search	42	delete_twittersearch
130	Can add Snippet	43	add_snippet
131	Can change Snippet	43	change_snippet
132	Can delete Snippet	43	delete_snippet
133	Can add Snippet	44	add_snippetptr
134	Can change Snippet	44	change_snippetptr
135	Can delete Snippet	44	delete_snippetptr
136	Can add google map	45	add_googlemap
137	Can change google map	45	change_googlemap
138	Can delete google map	45	delete_googlemap
139	Can add contact	46	add_contact
140	Can change contact	46	change_contact
141	Can delete contact	46	delete_contact
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$10000$XI3uMIAOnc3w$lp6nPhtYz10MmGoTk1FQXIKpb77UioO+eiD8/I1vapI=	2013-10-14 04:20:05.389474+01	t	gnv			a@a.com	t	t	2013-09-27 08:24:43.483037+01
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: cms_cmsplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_cmsplugin (id, placeholder_id, parent_id, "position", language, plugin_type, creation_date, changed_date, level, lft, rght, tree_id) FROM stdin;
4	2	\N	0	fr	DealsPlugin	2013-09-27 08:43:09.486457+01	2013-09-27 08:46:50.452089+01	0	1	2	3
431	44	\N	0	fr	TopicPlugin	2013-09-30 09:41:24.717756+01	2013-10-03 07:46:06.848496+01	0	1	2	360
448	65	\N	0	fr	PicturePlugin	2013-10-13 22:55:42.365441+01	2013-10-13 22:56:10.829848+01	0	1	2	377
7	24	\N	0	en	DealsPlugin	2013-09-27 11:24:19.213132+01	2013-09-27 11:24:19.217642+01	0	1	2	6
239	8	\N	1	fr	DealsPlugin	2013-09-27 19:50:54.600059+01	2013-09-27 19:51:45.829259+01	0	1	2	184
294	16	\N	0	fr	TopicPlugin	2013-09-30 09:41:24.717756+01	2013-10-03 07:45:56.527214+01	0	1	2	232
291	59	\N	0	ar	SnippetPlugin	2013-09-29 16:29:37.980573+01	2013-09-30 09:32:43.344192+01	0	1	2	230
482	67	\N	0	ar	PicturePlugin	2013-10-13 22:55:42.365441+01	2013-10-14 00:06:13.891003+01	0	1	2	387
100	17	\N	0	en	PicturePlugin	2013-09-27 18:16:57.97938+01	2013-09-27 18:17:21.520872+01	0	1	2	82
333	24	\N	0	fr	TextPlugin	2013-09-30 10:26:48.508319+01	2013-10-01 15:28:46.924444+01	0	1	2	271
223	25	\N	0	en	PicturePlugin	2013-09-27 19:47:16.690997+01	2013-09-27 19:48:20.215571+01	0	1	2	168
225	25	\N	0	ar	PicturePlugin	2013-09-27 19:47:16.690997+01	2013-09-27 19:48:40.667894+01	0	1	2	170
98	20	\N	0	en	TextPlugin	2013-09-27 18:00:07.327808+01	2013-09-27 18:40:17.848047+01	0	1	2	80
235	5	\N	0	en	PicturePlugin	2013-09-27 19:50:37.618281+01	2013-09-27 19:50:49.775901+01	0	1	2	180
427	41	\N	0	fr	PicturePlugin	2013-09-27 19:05:27.270484+01	2013-10-03 07:46:06.79514+01	0	1	2	356
237	8	\N	0	en	TextPlugin	2013-09-27 19:51:07.453716+01	2013-09-27 19:51:32.759903+01	0	1	2	182
172	12	\N	0	en	TextPlugin	2013-09-27 18:35:57.86583+01	2013-09-27 19:38:21.78947+01	0	1	2	135
139	12	\N	0	fr	TextPlugin	2013-09-27 18:35:57.86583+01	2013-09-27 19:39:09.948715+01	0	1	2	102
170	12	\N	0	ar	TextPlugin	2013-09-27 18:35:57.86583+01	2013-09-27 19:39:59.128498+01	0	1	2	133
312	12	\N	1	ar	TopicPlugin	2013-09-30 10:01:13.511227+01	2013-09-30 10:01:17.34482+01	0	1	2	250
240	8	\N	0	fr	TextPlugin	2013-09-27 19:51:07.453716+01	2013-09-27 19:51:45.909917+01	0	1	2	185
242	8	\N	1	ar	DealsPlugin	2013-09-27 19:50:54.600059+01	2013-09-27 19:51:54.493525+01	0	1	2	187
243	8	\N	0	ar	TextPlugin	2013-09-27 19:51:07.453716+01	2013-09-27 19:51:54.573544+01	0	1	2	188
428	41	\N	0	ar	PicturePlugin	2013-09-27 19:05:27.270484+01	2013-10-03 07:46:06.804727+01	0	1	2	357
292	60	\N	0	fr	SnippetPlugin	2013-09-29 16:38:58.313381+01	2013-09-30 09:32:43.358593+01	0	1	2	231
432	44	\N	0	ar	TopicPlugin	2013-09-30 09:41:24.717756+01	2013-10-03 07:46:06.857967+01	0	1	2	361
430	44	\N	0	en	TextPlugin	2013-09-27 19:04:04.272309+01	2013-10-03 07:46:06.866997+01	0	1	2	359
405	21	\N	0	ar	SnippetPlugin	2013-10-01 15:36:53.862558+01	2013-10-01 15:47:50.701796+01	0	1	2	338
385	62	\N	0	fr	TextPlugin	2013-09-30 23:15:57.998553+01	2013-10-02 08:20:23.995716+01	0	1	2	319
87	1	\N	0	en	PicturePlugin	2013-09-27 17:12:51.870818+01	2013-09-27 17:13:39.024542+01	0	1	2	69
439	33	\N	0	en	PicturePlugin	2013-09-27 19:50:37.618281+01	2013-10-13 22:36:03.35918+01	0	1	2	368
440	33	\N	0	fr	PicturePlugin	2013-09-27 19:50:37.618281+01	2013-10-13 22:36:03.368638+01	0	1	2	369
269	57	\N	0	ar	SnippetPlugin	2013-09-29 16:29:37.980573+01	2013-09-29 16:30:28.479689+01	0	1	2	214
272	58	\N	0	fr	SnippetPlugin	2013-09-29 16:38:58.313381+01	2013-09-29 16:39:01.498611+01	0	1	2	217
253	4	\N	0	fr	TopicPlugin	2013-09-28 12:09:19.062636+01	2013-09-30 09:05:17.994774+01	0	1	2	198
280	4	\N	0	ar	TopicPlugin	2013-09-30 09:06:20.435675+01	2013-09-30 09:06:24.455903+01	0	1	2	223
441	33	\N	0	ar	PicturePlugin	2013-09-27 19:50:37.618281+01	2013-10-13 22:36:03.378122+01	0	1	2	370
153	13	\N	0	fr	PicturePlugin	2013-09-27 19:05:27.270484+01	2013-09-27 19:05:38.916002+01	0	1	2	116
154	13	\N	0	ar	PicturePlugin	2013-09-27 19:05:27.270484+01	2013-09-27 19:05:52.486648+01	0	1	2	117
442	36	\N	1	en	DealsPlugin	2013-09-27 19:50:54.600059+01	2013-10-13 22:36:03.401935+01	0	1	2	371
429	41	\N	0	en	PicturePlugin	2013-09-27 19:05:27.270484+01	2013-10-03 07:46:06.814256+01	0	1	2	358
446	36	\N	1	ar	DealsPlugin	2013-09-27 19:50:54.600059+01	2013-10-13 22:36:03.439151+01	0	1	2	375
481	67	\N	0	fr	PicturePlugin	2013-10-13 22:55:42.365441+01	2013-10-14 00:06:13.881263+01	0	1	2	386
483	68	\N	0	fr	TextPlugin	2013-10-13 22:57:23.457709+01	2013-10-14 00:06:13.926204+01	0	1	2	388
444	36	\N	1	fr	DealsPlugin	2013-09-27 19:50:54.600059+01	2013-10-13 22:36:03.420757+01	0	1	2	373
484	68	\N	0	ar	TextPlugin	2013-10-13 22:57:23.457709+01	2013-10-14 00:06:13.935152+01	0	1	2	389
443	36	\N	0	en	TextPlugin	2013-09-27 19:51:07.453716+01	2013-10-13 22:36:03.458748+01	0	1	2	372
445	36	\N	0	fr	TextPlugin	2013-09-27 19:51:07.453716+01	2013-10-13 22:36:03.468547+01	0	1	2	374
447	36	\N	0	ar	TextPlugin	2013-09-27 19:51:07.453716+01	2013-10-13 22:36:03.478569+01	0	1	2	376
176	9	\N	0	en	PicturePlugin	2013-09-27 19:28:06.957023+01	2013-09-27 19:28:34.519504+01	0	1	2	139
82	28	\N	0	en	TextPlugin	2013-09-27 15:35:54.353237+01	2013-09-27 19:30:01.736328+01	0	1	2	64
157	13	\N	0	en	PicturePlugin	2013-09-27 19:05:27.270484+01	2013-09-27 19:06:06.36663+01	0	1	2	120
158	16	\N	0	en	TextPlugin	2013-09-27 19:04:04.272309+01	2013-09-27 19:06:10.291819+01	0	1	2	121
174	9	\N	0	ar	PicturePlugin	2013-09-27 19:28:06.957023+01	2013-09-27 19:28:21.140396+01	0	1	2	137
175	9	\N	0	fr	PicturePlugin	2013-09-27 19:28:06.957023+01	2013-09-27 19:28:29.659137+01	0	1	2	138
224	25	\N	0	fr	PicturePlugin	2013-09-27 19:47:16.690997+01	2013-09-27 19:48:35.299859+01	0	1	2	169
189	28	\N	0	ar	TextPlugin	2013-09-27 15:35:54.353237+01	2013-09-27 19:31:26.706985+01	0	1	2	152
226	53	\N	0	en	PicturePlugin	2013-09-27 19:47:16.690997+01	2013-09-27 19:48:46.451289+01	0	1	2	171
227	53	\N	0	fr	PicturePlugin	2013-09-27 19:47:16.690997+01	2013-09-27 19:48:46.489041+01	0	1	2	172
311	12	\N	1	fr	TopicPlugin	2013-09-30 10:01:01.765702+01	2013-09-30 10:01:06.420339+01	0	1	2	249
228	53	\N	0	ar	PicturePlugin	2013-09-27 19:47:16.690997+01	2013-09-27 19:48:46.52635+01	0	1	2	173
187	28	\N	0	fr	TextPlugin	2013-09-27 15:35:54.353237+01	2013-09-27 19:30:26.171782+01	0	1	2	150
313	37	\N	0	ar	PicturePlugin	2013-09-27 19:28:06.957023+01	2013-09-30 10:01:21.975905+01	0	1	2	251
314	37	\N	0	fr	PicturePlugin	2013-09-27 19:28:06.957023+01	2013-09-30 10:01:21.985206+01	0	1	2	252
232	56	\N	0	en	TextPlugin	2013-09-27 15:35:54.353237+01	2013-09-27 19:48:46.842989+01	0	1	2	177
233	56	\N	0	fr	TextPlugin	2013-09-27 15:35:54.353237+01	2013-09-27 19:48:46.873236+01	0	1	2	178
234	56	\N	0	ar	TextPlugin	2013-09-27 15:35:54.353237+01	2013-09-27 19:48:46.90361+01	0	1	2	179
236	8	\N	1	en	DealsPlugin	2013-09-27 19:50:54.600059+01	2013-09-27 19:51:10.650346+01	0	1	2	181
238	5	\N	0	fr	PicturePlugin	2013-09-27 19:50:37.618281+01	2013-09-27 19:51:42.798538+01	0	1	2	183
241	5	\N	0	ar	PicturePlugin	2013-09-27 19:50:37.618281+01	2013-09-27 19:51:51.292251+01	0	1	2	186
449	65	\N	0	ar	PicturePlugin	2013-10-13 22:55:42.365441+01	2013-10-13 22:56:18.770531+01	0	1	2	378
315	37	\N	0	en	PicturePlugin	2013-09-27 19:28:06.957023+01	2013-09-30 10:01:21.994359+01	0	1	2	253
386	61	\N	0	fr	PicturePlugin	2013-09-30 23:16:05.221938+01	2013-09-30 23:16:24.795376+01	0	1	2	320
388	61	\N	0	ar	PicturePlugin	2013-09-30 23:16:05.221938+01	2013-09-30 23:21:07.881373+01	0	1	2	322
409	52	\N	0	fr	TextPlugin	2013-09-30 10:26:48.508319+01	2013-10-01 15:47:55.992737+01	0	1	2	342
452	66	\N	0	fr	TextPlugin	2013-10-13 22:57:23.457709+01	2013-10-14 00:05:49.740676+01	0	1	2	381
334	24	\N	0	ar	TextPlugin	2013-09-30 10:26:48.508319+01	2013-10-01 15:30:54.663083+01	0	1	2	272
434	29	\N	0	fr	PicturePlugin	2013-09-30 23:09:55.244409+01	2013-10-13 22:34:19.34234+01	0	1	2	363
370	1	\N	0	ar	PicturePlugin	2013-09-30 23:10:52.675092+01	2013-09-30 23:11:05.252827+01	0	1	2	304
319	40	\N	1	fr	TopicPlugin	2013-09-30 10:01:01.765702+01	2013-09-30 10:01:22.04582+01	0	1	2	257
320	40	\N	1	ar	TopicPlugin	2013-09-30 10:01:13.511227+01	2013-09-30 10:01:22.055552+01	0	1	2	258
316	40	\N	0	fr	TextPlugin	2013-09-27 18:35:57.86583+01	2013-09-30 10:01:22.065181+01	0	1	2	254
317	40	\N	0	ar	TextPlugin	2013-09-27 18:35:57.86583+01	2013-09-30 10:01:22.074649+01	0	1	2	255
318	40	\N	0	en	TextPlugin	2013-09-27 18:35:57.86583+01	2013-09-30 10:01:22.083735+01	0	1	2	256
415	45	\N	0	en	PicturePlugin	2013-09-27 18:16:57.97938+01	2013-10-02 08:21:07.739436+01	0	1	2	348
480	66	\N	0	ar	TextPlugin	2013-10-13 22:57:23.457709+01	2013-10-14 00:06:07.203603+01	0	1	2	385
404	21	\N	0	fr	SnippetPlugin	2013-10-01 15:36:53.862558+01	2013-10-01 15:47:37.229781+01	0	1	2	337
416	45	\N	0	fr	SnippetPlugin	2013-09-30 23:11:46.017278+01	2013-10-02 08:21:07.749116+01	0	1	2	349
410	52	\N	0	ar	TextPlugin	2013-09-30 10:26:48.508319+01	2013-10-01 15:47:56.001356+01	0	1	2	343
406	49	\N	0	fr	SnippetPlugin	2013-10-01 15:36:53.862558+01	2013-10-01 15:47:55.938552+01	0	1	2	339
407	49	\N	0	ar	SnippetPlugin	2013-10-01 15:36:53.862558+01	2013-10-01 15:47:55.948455+01	0	1	2	340
408	52	\N	0	en	DealsPlugin	2013-10-01 15:47:55.966346+01	2013-10-01 15:47:55.967373+01	0	1	2	341
417	45	\N	0	ar	SnippetPlugin	2013-09-30 23:12:01.812858+01	2013-10-02 08:21:07.758355+01	0	1	2	350
393	62	\N	0	ar	TextPlugin	2013-09-30 23:15:57.998553+01	2013-10-02 08:20:42.883954+01	0	1	2	327
424	63	\N	0	ar	PicturePlugin	2013-09-30 23:16:05.221938+01	2013-10-02 08:21:30.596689+01	0	1	2	353
418	48	\N	0	en	TextPlugin	2013-09-27 18:00:07.327808+01	2013-10-02 08:21:07.79244+01	0	1	2	351
377	17	\N	0	fr	SnippetPlugin	2013-09-30 23:11:46.017278+01	2013-09-30 23:11:50.673836+01	0	1	2	311
378	17	\N	0	ar	SnippetPlugin	2013-09-30 23:12:01.812858+01	2013-09-30 23:12:05.641334+01	0	1	2	312
435	29	\N	0	ar	PicturePlugin	2013-09-30 23:10:52.675092+01	2013-10-13 22:34:19.352195+01	0	1	2	364
423	63	\N	0	fr	PicturePlugin	2013-09-30 23:16:05.221938+01	2013-10-02 08:21:30.586912+01	0	1	2	352
425	64	\N	0	fr	TextPlugin	2013-09-30 23:15:57.998553+01	2013-10-02 08:21:30.633713+01	0	1	2	354
426	64	\N	0	ar	TextPlugin	2013-09-30 23:15:57.998553+01	2013-10-02 08:21:30.642758+01	0	1	2	355
296	16	\N	0	ar	TopicPlugin	2013-09-30 09:41:24.717756+01	2013-10-03 07:46:03.551449+01	0	1	2	234
368	1	\N	0	fr	PicturePlugin	2013-09-30 23:09:55.244409+01	2013-10-13 22:34:13.742971+01	0	1	2	303
433	29	\N	0	en	PicturePlugin	2013-09-27 17:12:51.870818+01	2013-10-13 22:34:19.332192+01	0	1	2	362
436	30	\N	0	fr	DealsPlugin	2013-09-27 08:43:09.486457+01	2013-10-13 22:34:19.369068+01	0	1	2	365
437	32	\N	0	fr	TopicPlugin	2013-09-28 12:09:19.062636+01	2013-10-13 22:34:19.386099+01	0	1	2	366
438	32	\N	0	ar	TopicPlugin	2013-09-30 09:06:20.435675+01	2013-10-13 22:34:19.395431+01	0	1	2	367
\.


--
-- Data for Name: cms_globalpagepermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_globalpagepermission (id, user_id, group_id, can_change, can_add, can_delete, can_change_advanced_settings, can_publish, can_change_permissions, can_move_page, can_view, can_recover_page) FROM stdin;
\.


--
-- Data for Name: cms_globalpagepermission_sites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_globalpagepermission_sites (id, globalpagepermission_id, site_id) FROM stdin;
\.


--
-- Data for Name: cms_page; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_page (id, created_by, changed_by, parent_id, creation_date, changed_date, publication_date, publication_end_date, in_navigation, soft_root, reverse_id, navigation_extenders, published, template, site_id, level, lft, rght, tree_id, login_required, limit_visibility_in_menu, publisher_is_draft, publisher_public_id, publisher_state, moderator_state) FROM stdin;
13	gnv	gnv	\N	2013-09-27 08:28:03.026499+01	2013-10-01 15:47:55.831984+01	2013-09-27 08:27:58.209334+01	\N	t	f	\N		t	template1.html	1	0	1	2	15	f	\N	f	6	0	\N
11	gnv	gnv	\N	2013-09-27 08:28:00.89439+01	2013-10-03 07:46:06.688303+01	2013-09-27 08:27:56.073045+01	\N	t	f	\N		t	template1.html	1	0	1	2	12	f	\N	f	4	0	\N
12	gnv	gnv	\N	2013-09-27 08:28:01.871143+01	2013-10-02 08:21:07.635209+01	2013-09-27 08:27:57.056755+01	\N	t	f	\N		t	template1.html	1	0	1	2	13	f	\N	f	5	0	\N
16	gnv	gnv	\N	2013-09-29 16:30:36.372272+01	2013-09-30 09:32:43.259022+01	2013-09-29 16:30:08.066288+01	\N	f	f	\N		t	template1.html	1	0	1	2	17	f	\N	f	15	0	\N
10	gnv	gnv	\N	2013-09-27 08:27:59.837875+01	2013-09-30 10:01:21.87372+01	2013-09-27 08:27:55.014753+01	\N	t	f	\N		t	template1.html	1	0	1	2	11	f	\N	f	3	0	\N
14	gnv	gnv	\N	2013-09-27 08:28:04.089721+01	2013-09-27 19:48:46.139164+01	2013-09-27 08:27:59.275725+01	\N	t	f	\N		t	template1.html	1	0	1	2	16	f	\N	f	7	0	\N
4	gnv	gnv	\N	2013-09-27 08:27:01.122425+01	2013-10-03 07:46:06.881108+01	2013-09-27 08:27:56.073045+01	\N	t	f	\N		t	template1.html	1	0	1	2	4	f	\N	t	11	0	\N
15	gnv	gnv	\N	2013-09-29 16:29:21.717186+01	2013-09-30 09:32:43.371241+01	2013-09-29 16:30:08.066288+01	\N	f	f	\N		t	template1.html	1	0	1	2	19	f	\N	t	16	0	\N
6	gnv	gnv	\N	2013-09-27 08:27:25.764076+01	2013-10-01 15:47:56.012216+01	2013-09-27 08:27:58.209334+01	\N	t	f	\N		t	template1.html	1	0	1	2	7	f	\N	t	13	0	\N
5	gnv	gnv	\N	2013-09-27 08:27:12.762271+01	2013-10-02 08:21:07.803719+01	2013-09-27 08:27:57.056755+01	\N	t	f	\N		t	template1.html	1	0	1	2	5	f	\N	t	12	0	\N
3	gnv	gnv	\N	2013-09-27 08:26:36.101208+01	2013-09-30 10:01:22.097483+01	2013-09-27 08:27:55.014753+01	\N	t	f	\N		t	template1.html	1	0	1	2	3	f	\N	t	10	0	\N
18	gnv	gnv	\N	2013-09-30 23:21:20.468935+01	2013-10-02 08:21:30.493645+01	2013-09-30 23:20:53.92114+01	\N	f	f	\N		t	template1.html	1	0	1	2	10	f	\N	f	17	0	\N
8	gnv	gnv	\N	2013-09-27 08:27:55.745599+01	2013-10-13 22:34:19.223851+01	2013-09-27 08:27:50.925283+01	\N	t	f	\N		t	template1.html	1	0	1	2	9	f	\N	f	1	0	\N
7	gnv	gnv	\N	2013-09-27 08:27:42.431707+01	2013-09-27 19:48:46.94637+01	2013-09-27 08:27:59.275725+01	\N	t	f	\N		t	template1.html	1	0	1	2	8	f	\N	t	14	0	\N
17	gnv	gnv	\N	2013-09-30 23:15:38.817152+01	2013-10-02 08:21:30.654927+01	2013-09-30 23:20:53.92114+01	\N	f	f	\N		t	template1.html	1	0	1	2	2	f	\N	t	18	0	\N
20	gnv	gnv	\N	2013-10-13 22:56:56.078256+01	2013-10-14 00:06:13.797691+01	2013-10-13 22:56:51.226603+01	\N	f	f	\N		t	template1.html	1	0	1	2	18	f	\N	f	19	0	\N
1	gnv	gnv	\N	2013-09-27 08:25:45.208469+01	2013-10-13 22:34:19.409351+01	2013-09-27 08:27:50.925283+01	\N	t	f	\N		t	template1.html	1	0	1	2	1	f	\N	t	8	0	\N
19	gnv	gnv	\N	2013-10-13 22:55:17.916959+01	2013-10-14 00:06:13.946039+01	2013-10-13 22:56:51.226603+01	\N	f	f	\N		t	template1.html	1	0	1	2	20	f	\N	t	20	0	\N
9	gnv	gnv	\N	2013-09-27 08:27:58.325246+01	2013-10-13 22:36:03.234713+01	2013-09-27 08:27:53.50487+01	\N	t	f	\N		t	template1.html	1	0	1	2	14	f	\N	f	2	0	\N
2	gnv	gnv	\N	2013-09-27 08:26:22.381844+01	2013-10-13 22:36:03.489597+01	2013-09-27 08:27:53.50487+01	\N	t	f	\N		t	template1.html	1	0	1	2	6	f	\N	t	9	0	\N
\.


--
-- Data for Name: cms_page_placeholders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_page_placeholders (id, page_id, placeholder_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	1	4
5	2	5
6	2	6
7	2	7
8	2	8
9	3	9
10	3	10
11	3	11
12	3	12
13	4	13
14	4	14
15	4	15
16	4	16
17	5	17
18	5	18
19	5	19
20	5	20
21	6	21
22	6	22
23	6	23
24	6	24
25	7	25
26	7	26
27	7	27
28	7	28
29	8	29
30	8	30
31	8	31
32	8	32
33	9	33
34	9	34
35	9	35
36	9	36
37	10	37
38	10	38
39	10	39
40	10	40
41	11	41
42	11	42
43	11	43
44	11	44
45	12	45
46	12	46
47	12	47
48	12	48
49	13	49
50	13	50
51	13	51
52	13	52
53	14	53
54	14	54
55	14	55
56	14	56
57	15	57
58	15	58
59	16	59
60	16	60
61	17	61
62	17	62
63	18	63
64	18	64
65	19	65
66	19	66
67	20	67
68	20	68
\.


--
-- Data for Name: cms_pagemoderatorstate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_pagemoderatorstate (id, page_id, user_id, created, action, message) FROM stdin;
\.


--
-- Data for Name: cms_pagepermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_pagepermission (id, user_id, group_id, can_change, can_add, can_delete, can_change_advanced_settings, can_publish, can_change_permissions, can_move_page, can_view, grant_on, page_id) FROM stdin;
\.


--
-- Data for Name: cms_pageuser; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_pageuser (user_ptr_id, created_by_id) FROM stdin;
\.


--
-- Data for Name: cms_pageusergroup; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_pageusergroup (group_ptr_id, created_by_id) FROM stdin;
\.


--
-- Data for Name: cms_placeholder; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_placeholder (id, slot, default_width) FROM stdin;
1	cover_image	\N
2	deals	\N
3	news	\N
4	main_section	\N
5	cover_image	\N
6	deals	\N
7	news	\N
8	main_section	\N
9	cover_image	\N
10	deals	\N
11	news	\N
12	main_section	\N
13	cover_image	\N
14	deals	\N
15	news	\N
16	main_section	\N
17	cover_image	\N
18	deals	\N
19	news	\N
20	main_section	\N
21	cover_image	\N
22	deals	\N
23	news	\N
24	main_section	\N
25	cover_image	\N
26	deals	\N
27	news	\N
28	main_section	\N
29	cover_image	\N
30	deals	\N
31	news	\N
32	main_section	\N
33	cover_image	\N
34	deals	\N
35	news	\N
36	main_section	\N
37	cover_image	\N
38	deals	\N
39	news	\N
40	main_section	\N
41	cover_image	\N
42	deals	\N
43	news	\N
44	main_section	\N
45	cover_image	\N
46	deals	\N
47	news	\N
48	main_section	\N
49	cover_image	\N
50	deals	\N
51	news	\N
52	main_section	\N
53	cover_image	\N
54	deals	\N
55	news	\N
56	main_section	\N
57	cover_image	\N
58	main_section	\N
59	cover_image	\N
60	main_section	\N
61	cover_image	\N
62	main_section	\N
63	cover_image	\N
64	main_section	\N
65	cover_image	\N
66	main_section	\N
67	cover_image	\N
68	main_section	\N
\.


--
-- Data for Name: cms_title; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_title (id, language, title, menu_title, slug, path, has_url_overwrite, application_urls, redirect, meta_description, meta_keywords, page_title, page_id, creation_date) FROM stdin;
19	ar	إستقبال		home		f						1	2013-09-27 12:13:49.99256+01
3	fr	SERVICES		services	services	f						3	2013-09-27 08:26:36.209318+01
40	ar	الاتصال		contact	contact	f						7	2013-09-27 15:22:31.80963+01
42	ar	الاتصال		contact	contact	f						14	2013-09-27 15:22:31.80963+01
14	fr	CONTACT		contact	contact	f						14	2013-09-27 08:27:42.545299+01
41	en	CONTACT		contact	contact	f						14	2013-09-27 15:22:16.479411+01
43	fr	Booking		booking	booking	f						15	2013-09-29 16:29:21.743499+01
37	en	CAMPAIGN 2013		campaign-2013	campaign-2013	f	\N	\N				13	2013-09-27 15:21:29.735985+01
1	fr	ACCEUIL		home		f						1	2013-09-27 08:25:45.32255+01
20	ar	إستقبال		home		f						8	2013-09-27 12:13:49.99256+01
8	fr	ACCEUIL		home		f						8	2013-09-27 08:25:45.32255+01
16	en	HOME		home		f						8	2013-09-27 08:40:12.477757+01
13	fr	Communication		communication	communication	f						13	2013-09-27 08:27:25.868723+01
38	ar	اتصالات		communication	communication	f						13	2013-09-27 15:21:44.408605+01
2	fr	traversées		traversees	traversees	f						2	2013-09-27 08:26:22.494396+01
51	fr	Carte de fidélité		la-carte	la-carte	f						19	2013-10-13 22:55:17.940125+01
22	ar	المعابر		traversees	traversees	f						2	2013-09-27 14:23:26.220254+01
26	ar	الخدمات		services	services	f						3	2013-09-27 14:24:16.237009+01
10	fr	SERVICES		services	services	f						10	2013-09-27 08:26:36.209318+01
9	fr	traversées		traversees	traversees	f						9	2013-09-27 08:26:22.494396+01
28	ar	الخدمات		services	services	f						10	2013-09-27 14:24:16.237009+01
24	ar	المعابر		traversees	traversees	f						9	2013-09-27 14:23:26.220254+01
32	ar	زيارة GNV		visit-gnv	visit-gnv	f						5	2013-09-27 14:26:08.745065+01
27	en	SERVICES		services	services	f						10	2013-09-27 14:23:48.088033+01
35	en	CAMPAIGN 2013		campaign-2013	campaign-2013	f	\N	\N				6	2013-09-27 15:21:29.735985+01
34	ar	زيارة GNV		visit-gnv	visit-gnv	f						12	2013-09-27 14:26:08.745065+01
23	en	PROMOTIONS		promotions	promotions	f						9	2013-09-27 14:23:04.624244+01
12	fr	VISITER GNV		visit-gnv	visit-gnv	f						12	2013-09-27 08:27:12.869072+01
33	en	VISIT GNV		visit-gnv	visit-gnv	f						12	2013-09-27 14:25:34.850904+01
47	fr	à propos		about	about	f						17	2013-09-30 23:15:38.842303+01
48	ar	تقديم		about	about	f						17	2013-09-30 23:20:58.938895+01
7	fr	CONTACT		contact	contact	f						7	2013-09-27 08:27:42.545299+01
6	fr	Communication		communication	communication	f						6	2013-09-27 08:27:25.868723+01
52	ar	بطاقة الولاء		la-carte	la-carte	f						19	2013-10-13 22:56:51.56482+01
21	en	PROMOTIONS		promotions	promotions	f						2	2013-09-27 14:23:04.624244+01
53	fr	Carte de fidélité		la-carte	la-carte	f						20	2013-10-13 22:55:17.940125+01
5	fr	VISITER GNV		visit-gnv	visit-gnv	f						5	2013-09-27 08:27:12.869072+01
36	ar	اتصالات		communication	communication	f						6	2013-09-27 15:21:44.408605+01
54	ar	بطاقة الولاء		la-carte	la-carte	f						20	2013-10-13 22:56:51.56482+01
45	fr	Booking		booking	booking	f						16	2013-09-29 16:29:21.743499+01
46	ar	الحجز		booking	booking	f	\N	\N				16	2013-09-29 16:30:31.43869+01
31	en	VISIT GNV		visit-gnv	visit-gnv	f						5	2013-09-27 14:25:34.850904+01
49	fr	à propos		about	about	f						18	2013-09-30 23:15:38.842303+01
50	ar	تقديم		about	about	f						18	2013-09-30 23:20:58.938895+01
29	ar	وجهات السفر		destinations	destinations	f						4	2013-09-27 14:25:01.349268+01
30	ar	وجهات السفر		destinations	destinations	f						11	2013-09-27 14:25:01.349268+01
25	en	SERVICES		services	services	f						3	2013-09-27 14:23:48.088033+01
11	fr	DESTINATIONS		destinations	destinations	f						11	2013-09-27 08:27:01.240842+01
18	en	DESTINATIONS		destinations	destinations	f						11	2013-09-27 12:05:18.912154+01
4	fr	DESTINATIONS		destinations	destinations	f						4	2013-09-27 08:27:01.240842+01
17	en	DESTINATIONS		destinations	destinations	f						4	2013-09-27 12:05:18.912154+01
39	en	CONTACT		contact	contact	f						7	2013-09-27 15:22:16.479411+01
15	en	HOME		home		f						1	2013-09-27 08:40:12.477757+01
44	ar	الحجز		booking	booking	f	\N	\N				15	2013-09-29 16:30:31.43869+01
\.


--
-- Data for Name: cmsplugin_dealspluginmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_dealspluginmodel (cmsplugin_ptr_id, deals_id) FROM stdin;
4	1
236	1
239	1
242	1
436	1
442	1
444	1
446	1
\.


--
-- Data for Name: cmsplugin_flash; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_flash (cmsplugin_ptr_id, file, width, height) FROM stdin;
\.


--
-- Data for Name: cmsplugin_googlemap; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_googlemap (cmsplugin_ptr_id, title, address, zipcode, city, content, zoom, lat, lng, route_planer_title, route_planer, width, height) FROM stdin;
\.


--
-- Data for Name: cmsplugin_link; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_link (cmsplugin_ptr_id, name, url, page_link_id, mailto, target) FROM stdin;
\.


--
-- Data for Name: cmsplugin_newspluginmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_newspluginmodel (cmsplugin_ptr_id, title) FROM stdin;
\.


--
-- Data for Name: cmsplugin_picture; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_picture (cmsplugin_ptr_id, image, url, page_link_id, alt, longdesc, "float") FROM stdin;
313	cms_page_media/3/services-banner.jpg		\N			center
314	cms_page_media/3/services-banner.jpg		\N			center
315	cms_page_media/3/services-banner.jpg		\N			center
481	cms_page_media/19/visitGnv-banner.jpg		\N			center
87	cms_page_media/1/homeBanner-sample_2.jpg		\N			center
482	cms_page_media/19/visitGnv-banner.jpg		\N			center
100	cms_page_media/5/visitGnv-banner.jpg		\N			center
370	cms_page_media/1/homeAR-banner_1.jpg		\N			center
386	cms_page_media/17/visitGnv-banner.jpg		\N			center
388	cms_page_media/17/visitGnv-banner.jpg		\N			center
441	cms_page_media/2/destinations-banner.jpg		\N			center
153	cms_page_media/4/destinations-banner.jpg		\N			center
154	cms_page_media/4/destinations-banner.jpg		\N			center
157	cms_page_media/4/destinations-banner.jpg		\N			center
174	cms_page_media/3/services-banner.jpg		\N			center
175	cms_page_media/3/services-banner.jpg		\N			center
176	cms_page_media/3/services-banner.jpg		\N			center
415	cms_page_media/5/visitGnv-banner.jpg		\N			center
423	cms_page_media/17/visitGnv-banner.jpg		\N			center
424	cms_page_media/17/visitGnv-banner.jpg		\N			center
427	cms_page_media/4/destinations-banner.jpg		\N			center
428	cms_page_media/4/destinations-banner.jpg		\N			center
429	cms_page_media/4/destinations-banner.jpg		\N			center
368	cms_page_media/1/homeFR-banner.jpg		\N			center
223	cms_page_media/7/contact-banner.jpg		\N			center
224	cms_page_media/7/contact-banner.jpg		\N			center
225	cms_page_media/7/contact-banner.jpg		\N			center
226	cms_page_media/7/contact-banner.jpg		\N			center
227	cms_page_media/7/contact-banner.jpg		\N			center
228	cms_page_media/7/contact-banner.jpg		\N			center
235	cms_page_media/2/destinations-banner.jpg		\N			center
238	cms_page_media/2/destinations-banner.jpg		\N			center
241	cms_page_media/2/destinations-banner.jpg		\N			center
433	cms_page_media/1/homeBanner-sample_2.jpg		\N			center
434	cms_page_media/1/homeFR-banner.jpg		\N			center
435	cms_page_media/1/homeAR-banner_1.jpg		\N			center
439	cms_page_media/2/destinations-banner.jpg		\N			center
440	cms_page_media/2/destinations-banner.jpg		\N			center
448	cms_page_media/19/visitGnv-banner.jpg		\N			center
449	cms_page_media/19/visitGnv-banner.jpg		\N			center
\.


--
-- Data for Name: cmsplugin_snippetptr; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_snippetptr (cmsplugin_ptr_id, snippet_id) FROM stdin;
269	1
272	1
291	1
292	1
377	2
378	2
404	3
405	3
406	3
407	3
416	2
417	2
\.


--
-- Data for Name: cmsplugin_text; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_text (cmsplugin_ptr_id, body) FROM stdin;
452	<h1 class="pageTitle">La carte de fidélité</h1>\n<h2 class="pageSubTitle">LISTE DES PRIX DU PROGRAMME DE FIDÉLITÉ</h2>\n<p class="pageCaption">Choisissez votre destination et voyagez gratuitement avec GNV!</p>\n<ul class="pointsTable">\n\t<li>TRAVERSÉE: Palerme-Tunis</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">300 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">450 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t\t<li>TRAVERSÉES: Gênes-Tunis, Civitavecchia-Tunis, Barcelone-Tanger, Sète-Tanger, Sète-Nador</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">550 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">750 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t<li>TRAVERSÉE: Gênes -Tanger</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">900 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">1200 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody>\n</table>\n\t</li></ul>\n<p>* Le billet comprend l’hébergement en cabine interne à usage exclusif.</p>\n\n\n<h2 class="pageSubTitle">CARTE PREMIÈRE:</h2>\n<p>c’est votre «billet en plus». À chaque voyage avec GNV vous accumulez des points de fidélité qui vous permettront de voyager gratuitement.</p>\n<p class="pageCaption">Connectez-vous au site www.gnv.it et inscrivez-vous au Programme de fidélité avant le 30/04/2014, vous recevrez immédiatement un “Bonus de Bienvenue” de 50 points!</p>\n<p>Le mécanisme est simple : vous gagnez un point tous les 10 euros de dépense en billets GNV (par exemple, un billet du prix de 100 euros donnera droit à 10 points).</p>\n<p>Après vous être inscrit, vous recevrez par e-mail un code de validation qui rendra immédiatement opérationnelle la Carte Première ci-dessous.</p>\n<p>Écrivez ce code au verso et gardez la carte toujours avec vous pour pouvoir l’utiliser lorsque vous achèterez votre prochain billet GNV.</p>
385	<h1 class="pageTitle">A PROPOS DE GNV</h1>\n<h2 class="pageSubTitle">Grandi Navi Veloci - Passagers et marchandises</h2><p><span style="line-height: 5px;">Grandi Navi Veloci est l'une des principales compagnies de navigation italiennes, dans le secteur du cabotage et du transport de passagers, en mer Méditerranée. La compagnie a été créée en 1992 et c'est en 1993 qu'a été lancé son premier navire, le Majestic.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Le groupe possède aujourd'hui dix navires qui, avec les navires en location, au nombre de deux actuellement, assurent les liaisons vers la Sardaigne, la Sicile, l'Espagne, la Tunisie et le Maroc. Parallèlement aux services ciblés destinés aux passagers, la flotte de Grandi Navi Veloci s'est engagée dans le développement des lignes des autoroutes de la mer : le transport maritime représente une solution plus économique que le transport routier traditionnel et fournit une réponse valable au problème environnemental lié au trafic intensif de poids-lourds sur le réseau routier national.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Actuellement, le groupe Grandi Navi Veloci est détenu à 50 % par Marinvest, 35 % par Investitori Associati, 9,2 % par IDeA, 4,6 % par Charme tandis que la part restante de 1,2 % est répartie entre plusieurs autres investisseurs.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">L'histoire de Grandi Navi Veloci :</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1992 : naissance de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1999 : Grandi Navi Veloci entre en bourse – la seule compagnie italienne de navigation à l'époque – pour financer le développement de sa flotte, la plus moderne et la plus innovante d'Europe.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2003 : Grandi Navi Veloci arrive au Maghreb et inaugure la ligne Gênes – Tunis.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2004 : la famille Grimaldi et les fonds de Private Equity gérés par Permira parviennent à un accord pour poursuivre le développement de la compagnie, grâce à l'entrée dans le capital de Grandi Navi Veloci de Permira, à raison de 80 %, et de Grimaldi Holding, pour les 20 % restants.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2006 : la nouvelle structure de l'actionnariat se forme ; peu de temps avant la fin de l'année, les nouveaux partenaires - Investitori Associati, De Agostini, Charme et une partie des dirigeants - font leur entrée dans le capital de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2007 : Grandi Navi Veloci est désignée comme meilleure compagnie de navigation par les agents de voyage qui lui décernent le Bit Tourism Award 2007. Au mois de juin, Ariodante Valeri devient directeur général de la compagnie.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2008 : inauguration de la ligne Gênes – Barcelone – Tanger et premières liaisons avec le Maroc. C'est au cours de la même année que, sur les navires La Superba, La Suprema, Fantastic, Excellent et Majestic, un nouveau service réservé aux propriétaires d'animaux domestiques est mis en œuvre et proposé aux passagers.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2009 : la famille Grimaldi quitte définitivement le groupe d'actionnaires et la direction de Grandi Navi Veloci. La structure de l'actionnariat est ainsi répartie : Investitori Associati (67,1 %), IDeA Co-Investment Fund (20,1 %), Charme (9,2 %), dirigeants (2,1 %), autres actionnaires (1,5 %).</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2010 : Roberto Martinoli occupe les fonctions d'administrateur délégué et de président de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2011: le 2 mai, l’Assemblée Générale Extraordinaire des actionnaires de Grandi Navi Veloci délibère une augmentation du capital social réservé en faveur de Marinvest Srl, qui se voit attribuer une participation égale à 50% du capital social de Grandi Navi Veloci. Marinvest entre ainsi dans le sociétariat de Grandi Navi Veloci par une augmentation de capital en contrepartie d’une contribution et moyennant l’apport de trésorerie et de trois bateaux (SNAV Toscana, SNAV Lazio, SNAV Sardegna). La nouvelle équipe associée se répartit de la façon suivante : Marinvest (50%), Investitori Associati (35%), IDeA (9,2%), Charme (4,6%) et d’autres associés (1,2%).</span><br><span style="line-height: 5px;"></span></p>
139	<h1 class="pageTitle">Services à bord</h1><h2 class="pageSubTitle">Découvrez ce que vous réserve GNV pour un agréable séjour à bord de ses ferries:</h2><p>Des catégories de place qui répondent à toutes les exigences</p><ul><li><span style="line-height: 5px;">Restaurant à la carte, une cafétéria et un snack-bar.</span></li><li><span style="line-height: 5px;">Des activités de loisirs.</span></li><li><span style="line-height: 5px;">Des produits et des services conçus pour les enfants.</span></li><li><span style="line-height: 5px;">Possibilités de faire des achats.</span></li><li><span style="line-height: 5px;">Internet à bord.</span></li><li><span style="line-height: 5px;">Le service « Pets, Welcome on Board.</span></li></ul>\n\n
430	<p><span style="line-height: 5px;">Choisissez le port qui vous intéresse et trouvez toutes les informations nécessaires.</span></p><p><span style="line-height: 5px;">Lignes, cartes routières, horaires et limitations : tout ce qu'il vous faut savoir pour voyager en toute tranquillité et sécurité !</span></p>
158	<p><span style="line-height: 5px;">Choisissez le port qui vous intéresse et trouvez toutes les informations nécessaires.</span></p><p><span style="line-height: 5px;">Lignes, cartes routières, horaires et limitations : tout ce qu'il vous faut savoir pour voyager en toute tranquillité et sécurité !</span></p>
82	<h1 class="pageTitle">Contact</h1>
98	<h1 class="pageTitle">A PROPOS DE GNV</h1>\n<h2 class="pageSubTitle">Grandi Navi Veloci - Passagers et marchandises</h2><p><span style="line-height: 5px;">Grandi Navi Veloci est l'une des principales compagnies de navigation italiennes, dans le secteur du cabotage et du transport de passagers, en mer Méditerranée. La compagnie a été créée en 1992 et c'est en 1993 qu'a été lancé son premier navire, le Majestic.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Le groupe possède aujourd'hui dix navires qui, avec les navires en location, au nombre de deux actuellement, assurent les liaisons vers la Sardaigne, la Sicile, l'Espagne, la Tunisie et le Maroc. Parallèlement aux services ciblés destinés aux passagers, la flotte de Grandi Navi Veloci s'est engagée dans le développement des lignes des autoroutes de la mer : le transport maritime représente une solution plus économique que le transport routier traditionnel et fournit une réponse valable au problème environnemental lié au trafic intensif de poids-lourds sur le réseau routier national.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Actuellement, le groupe Grandi Navi Veloci est détenu à 50 % par Marinvest, 35 % par Investitori Associati, 9,2 % par IDeA, 4,6 % par Charme tandis que la part restante de 1,2 % est répartie entre plusieurs autres investisseurs.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">L'histoire de Grandi Navi Veloci :</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1992 : naissance de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1999 : Grandi Navi Veloci entre en bourse – la seule compagnie italienne de navigation à l'époque – pour financer le développement de sa flotte, la plus moderne et la plus innovante d'Europe.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2003 : Grandi Navi Veloci arrive au Maghreb et inaugure la ligne Gênes – Tunis.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2004 : la famille Grimaldi et les fonds de Private Equity gérés par Permira parviennent à un accord pour poursuivre le développement de la compagnie, grâce à l'entrée dans le capital de Grandi Navi Veloci de Permira, à raison de 80 %, et de Grimaldi Holding, pour les 20 % restants.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2006 : la nouvelle structure de l'actionnariat se forme ; peu de temps avant la fin de l'année, les nouveaux partenaires - Investitori Associati, De Agostini, Charme et une partie des dirigeants - font leur entrée dans le capital de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2007 : Grandi Navi Veloci est désignée comme meilleure compagnie de navigation par les agents de voyage qui lui décernent le Bit Tourism Award 2007. Au mois de juin, Ariodante Valeri devient directeur général de la compagnie.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2008 : inauguration de la ligne Gênes – Barcelone – Tanger et premières liaisons avec le Maroc. C'est au cours de la même année que, sur les navires La Superba, La Suprema, Fantastic, Excellent et Majestic, un nouveau service réservé aux propriétaires d'animaux domestiques est mis en œuvre et proposé aux passagers.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2009 : la famille Grimaldi quitte définitivement le groupe d'actionnaires et la direction de Grandi Navi Veloci. La structure de l'actionnariat est ainsi répartie : Investitori Associati (67,1 %), IDeA Co-Investment Fund (20,1 %), Charme (9,2 %), dirigeants (2,1 %), autres actionnaires (1,5 %).</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2010 : Roberto Martinoli occupe les fonctions d'administrateur délégué et de président de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2011: le 2 mai, l’Assemblée Générale Extraordinaire des actionnaires de Grandi Navi Veloci délibère une augmentation du capital social réservé en faveur de Marinvest Srl, qui se voit attribuer une participation égale à 50% du capital social de Grandi Navi Veloci. Marinvest entre ainsi dans le sociétariat de Grandi Navi Veloci par une augmentation de capital en contrepartie d’une contribution et moyennant l’apport de trésorerie et de trois bateaux (SNAV Toscana, SNAV Lazio, SNAV Sardegna). La nouvelle équipe associée se répartit de la façon suivante : Marinvest (50%), Investitori Associati (35%), IDeA (9,2%), Charme (4,6%) et d’autres associés (1,2%).</span><br><span style="line-height: 5px;"></span></p>
172	<h1 class="pageTitle">Services à bord</h1><h2 class="pageSubTitle">Découvrez ce que vous réserve GNV pour un agréable séjour à bord de ses ferries:</h2><p>Des catégories de place qui répondent à toutes les exigences</p><ul><li><span style="line-height: 5px;">Restaurant à la carte, une cafétéria et un snack-bar.</span></li><li><span style="line-height: 5px;">Des activités de loisirs.</span></li><li><span style="line-height: 5px;">Des produits et des services conçus pour les enfants.</span></li><li><span style="line-height: 5px;">Possibilités de faire des achats.</span></li><li><span style="line-height: 5px;">Internet à bord.</span></li><li><span style="line-height: 5px;">Le service « Pets, Welcome on Board.</span></li></ul>\n\n
170	<h1 class="pageTitle">Services à bord</h1><h2 class="pageSubTitle">Découvrez ce que vous réserve GNV pour un agréable séjour à bord de ses ferries:</h2><p>Des catégories de place qui répondent à toutes les exigences</p><ul style="font-size: medium; font-weight: normal;"><li><span style="line-height: 5px;">Restaurant à la carte, une cafétéria et un snack-bar.</span></li><li><span style="line-height: 5px;">Des activités de loisirs.</span></li><li><span style="line-height: 5px;">Des produits et des services conçus pour les enfants.</span></li><li><span style="line-height: 5px;">Possibilités de faire des achats.</span></li><li><span style="line-height: 5px;">Internet à bord.</span></li><li><span style="line-height: 5px;">Le service « Pets, Welcome on Board.</span></li></ul>\n\n
480	<h1 class="pageTitle">La carte de fidélité</h1>\n<h2 class="pageSubTitle">LISTE DES PRIX DU PROGRAMME DE FIDÉLITÉ</h2>\n<p class="pageCaption">Choisissez votre destination et voyagez gratuitement avec GNV!</p>\n<ul class="pointsTable">\n\t<li>TRAVERSÉE: Palerme-Tunis</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">300 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">450 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t\t<li>TRAVERSÉES: Gênes-Tunis, Civitavecchia-Tunis, Barcelone-Tanger, Sète-Tanger, Sète-Nador</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">550 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">750 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t<li>TRAVERSÉE: Gênes -Tanger</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">900 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">1200 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody>\n</table>\n\t</li></ul>\n<p>* Le billet comprend l’hébergement en cabine interne à usage exclusif.</p>\n\n\n<h2 class="pageSubTitle">CARTE PREMIÈRE:</h2>\n<p>c’est votre «billet en plus». À chaque voyage avec GNV vous accumulez des points de fidélité qui vous permettront de voyager gratuitement.</p>\n<p class="pageCaption">Connectez-vous au site www.gnv.it et inscrivez-vous au Programme de fidélité avant le 30/04/2014, vous recevrez immédiatement un “Bonus de Bienvenue” de 50 points!</p>\n<p>Le mécanisme est simple : vous gagnez un point tous les 10 euros de dépense en billets GNV (par exemple, un billet du prix de 100 euros donnera droit à 10 points).</p>\n<p>Après vous être inscrit, vous recevrez par e-mail un code de validation qui rendra immédiatement opérationnelle la Carte Première ci-dessous.</p>\n<p>Écrivez ce code au verso et gardez la carte toujours avec vous pour pouvoir l’utiliser lorsque vous achèterez votre prochain billet GNV.</p>
187	<h1 class="pageTitle">Contact</h1>
443	<h1 class="pageTitle">Promotions</h1>
189	<h1 class="pageTitle">إتصال</h1>
333	<h1 class="pageTitle">Nos publicités</h1>\n<h2 class="pageSubTitle">Campagne 2013</h2>\n\n<table width="100" cellpadding="0">\n<tbody><tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-reception.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-commandant.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel chef de réception </strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel commandant</strong></td>\n</tr>\n<tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-sailor.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-service.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel marin</strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel serveuse</strong></td>\n</tr>\n</tbody></table>
418	<h1 class="pageTitle">A PROPOS DE GNV</h1>\n<h2 class="pageSubTitle">Grandi Navi Veloci - Passagers et marchandises</h2><p><span style="line-height: 5px;">Grandi Navi Veloci est l'une des principales compagnies de navigation italiennes, dans le secteur du cabotage et du transport de passagers, en mer Méditerranée. La compagnie a été créée en 1992 et c'est en 1993 qu'a été lancé son premier navire, le Majestic.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Le groupe possède aujourd'hui dix navires qui, avec les navires en location, au nombre de deux actuellement, assurent les liaisons vers la Sardaigne, la Sicile, l'Espagne, la Tunisie et le Maroc. Parallèlement aux services ciblés destinés aux passagers, la flotte de Grandi Navi Veloci s'est engagée dans le développement des lignes des autoroutes de la mer : le transport maritime représente une solution plus économique que le transport routier traditionnel et fournit une réponse valable au problème environnemental lié au trafic intensif de poids-lourds sur le réseau routier national.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Actuellement, le groupe Grandi Navi Veloci est détenu à 50 % par Marinvest, 35 % par Investitori Associati, 9,2 % par IDeA, 4,6 % par Charme tandis que la part restante de 1,2 % est répartie entre plusieurs autres investisseurs.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">L'histoire de Grandi Navi Veloci :</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1992 : naissance de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1999 : Grandi Navi Veloci entre en bourse – la seule compagnie italienne de navigation à l'époque – pour financer le développement de sa flotte, la plus moderne et la plus innovante d'Europe.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2003 : Grandi Navi Veloci arrive au Maghreb et inaugure la ligne Gênes – Tunis.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2004 : la famille Grimaldi et les fonds de Private Equity gérés par Permira parviennent à un accord pour poursuivre le développement de la compagnie, grâce à l'entrée dans le capital de Grandi Navi Veloci de Permira, à raison de 80 %, et de Grimaldi Holding, pour les 20 % restants.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2006 : la nouvelle structure de l'actionnariat se forme ; peu de temps avant la fin de l'année, les nouveaux partenaires - Investitori Associati, De Agostini, Charme et une partie des dirigeants - font leur entrée dans le capital de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2007 : Grandi Navi Veloci est désignée comme meilleure compagnie de navigation par les agents de voyage qui lui décernent le Bit Tourism Award 2007. Au mois de juin, Ariodante Valeri devient directeur général de la compagnie.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2008 : inauguration de la ligne Gênes – Barcelone – Tanger et premières liaisons avec le Maroc. C'est au cours de la même année que, sur les navires La Superba, La Suprema, Fantastic, Excellent et Majestic, un nouveau service réservé aux propriétaires d'animaux domestiques est mis en œuvre et proposé aux passagers.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2009 : la famille Grimaldi quitte définitivement le groupe d'actionnaires et la direction de Grandi Navi Veloci. La structure de l'actionnariat est ainsi répartie : Investitori Associati (67,1 %), IDeA Co-Investment Fund (20,1 %), Charme (9,2 %), dirigeants (2,1 %), autres actionnaires (1,5 %).</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2010 : Roberto Martinoli occupe les fonctions d'administrateur délégué et de président de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2011: le 2 mai, l’Assemblée Générale Extraordinaire des actionnaires de Grandi Navi Veloci délibère une augmentation du capital social réservé en faveur de Marinvest Srl, qui se voit attribuer une participation égale à 50% du capital social de Grandi Navi Veloci. Marinvest entre ainsi dans le sociétariat de Grandi Navi Veloci par une augmentation de capital en contrepartie d’une contribution et moyennant l’apport de trésorerie et de trois bateaux (SNAV Toscana, SNAV Lazio, SNAV Sardegna). La nouvelle équipe associée se répartit de la façon suivante : Marinvest (50%), Investitori Associati (35%), IDeA (9,2%), Charme (4,6%) et d’autres associés (1,2%).</span><br><span style="line-height: 5px;"></span></p>
445	<h1 class="pageTitle">Promotions</h1>
447	<h1 class="pageTitle">Promotions</h1>
483	<h1 class="pageTitle">La carte de fidélité</h1>\n<h2 class="pageSubTitle">LISTE DES PRIX DU PROGRAMME DE FIDÉLITÉ</h2>\n<p class="pageCaption">Choisissez votre destination et voyagez gratuitement avec GNV!</p>\n<ul class="pointsTable">\n\t<li>TRAVERSÉE: Palerme-Tunis</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">300 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">450 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t\t<li>TRAVERSÉES: Gênes-Tunis, Civitavecchia-Tunis, Barcelone-Tanger, Sète-Tanger, Sète-Nador</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">550 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">750 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t<li>TRAVERSÉE: Gênes -Tanger</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">900 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">1200 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody>\n</table>\n\t</li></ul>\n<p>* Le billet comprend l’hébergement en cabine interne à usage exclusif.</p>\n\n\n<h2 class="pageSubTitle">CARTE PREMIÈRE:</h2>\n<p>c’est votre «billet en plus». À chaque voyage avec GNV vous accumulez des points de fidélité qui vous permettront de voyager gratuitement.</p>\n<p class="pageCaption">Connectez-vous au site www.gnv.it et inscrivez-vous au Programme de fidélité avant le 30/04/2014, vous recevrez immédiatement un “Bonus de Bienvenue” de 50 points!</p>\n<p>Le mécanisme est simple : vous gagnez un point tous les 10 euros de dépense en billets GNV (par exemple, un billet du prix de 100 euros donnera droit à 10 points).</p>\n<p>Après vous être inscrit, vous recevrez par e-mail un code de validation qui rendra immédiatement opérationnelle la Carte Première ci-dessous.</p>\n<p>Écrivez ce code au verso et gardez la carte toujours avec vous pour pouvoir l’utiliser lorsque vous achèterez votre prochain billet GNV.</p>
484	<h1 class="pageTitle">La carte de fidélité</h1>\n<h2 class="pageSubTitle">LISTE DES PRIX DU PROGRAMME DE FIDÉLITÉ</h2>\n<p class="pageCaption">Choisissez votre destination et voyagez gratuitement avec GNV!</p>\n<ul class="pointsTable">\n\t<li>TRAVERSÉE: Palerme-Tunis</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">300 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">450 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t\t<li>TRAVERSÉES: Gênes-Tunis, Civitavecchia-Tunis, Barcelone-Tanger, Sète-Tanger, Sète-Nador</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">550 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">750 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody></table>\n\t</li>\t<li>TRAVERSÉE: Gênes -Tanger</li>\t<li class="cell">\t\t<table cellspacing="0" cellpadding="0" border="0">\n\t\t\t<tbody><tr>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 1 personne + 1 voiture*\n\t\t\t\t\t<span class="info">900 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\tA/R pour 2 personne + 1 voiture*\n\t\t\t\t\t<span class="info">1200 <em>Points</em></span>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</tbody>\n</table>\n\t</li></ul>\n<p>* Le billet comprend l’hébergement en cabine interne à usage exclusif.</p>\n\n\n<h2 class="pageSubTitle">CARTE PREMIÈRE:</h2>\n<p>c’est votre «billet en plus». À chaque voyage avec GNV vous accumulez des points de fidélité qui vous permettront de voyager gratuitement.</p>\n<p class="pageCaption">Connectez-vous au site www.gnv.it et inscrivez-vous au Programme de fidélité avant le 30/04/2014, vous recevrez immédiatement un “Bonus de Bienvenue” de 50 points!</p>\n<p>Le mécanisme est simple : vous gagnez un point tous les 10 euros de dépense en billets GNV (par exemple, un billet du prix de 100 euros donnera droit à 10 points).</p>\n<p>Après vous être inscrit, vous recevrez par e-mail un code de validation qui rendra immédiatement opérationnelle la Carte Première ci-dessous.</p>\n<p>Écrivez ce code au verso et gardez la carte toujours avec vous pour pouvoir l’utiliser lorsque vous achèterez votre prochain billet GNV.</p>
232	<h1 class="pageTitle">Contact</h1>
233	<h1 class="pageTitle">Contact</h1>
234	<h1 class="pageTitle">إتصال</h1>
237	<h1 class="pageTitle">Promotions</h1>
240	<h1 class="pageTitle">Promotions</h1>
243	<h1 class="pageTitle">Promotions</h1>
334	<h1 class="pageTitle">إعلاناتنا</h1>\n<h2 class="pageSubTitle">حملة 2013</h2>\n\n<table width="100" cellpadding="0">\n<tbody><tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-reception.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-commandant.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">مدير مكتب الاستقبال</strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">قبطان</strong></td>\n</tr>\n\n<tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-sailor.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-service.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">بحار</strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">نادلة</strong></td>\n</tr>\n</tbody></table>
316	<h1 class="pageTitle">Services à bord</h1><h2 class="pageSubTitle">Découvrez ce que vous réserve GNV pour un agréable séjour à bord de ses ferries:</h2><p>Des catégories de place qui répondent à toutes les exigences</p><ul><li><span style="line-height: 5px;">Restaurant à la carte, une cafétéria et un snack-bar.</span></li><li><span style="line-height: 5px;">Des activités de loisirs.</span></li><li><span style="line-height: 5px;">Des produits et des services conçus pour les enfants.</span></li><li><span style="line-height: 5px;">Possibilités de faire des achats.</span></li><li><span style="line-height: 5px;">Internet à bord.</span></li><li><span style="line-height: 5px;">Le service « Pets, Welcome on Board.</span></li></ul>\n\n
317	<h1 class="pageTitle">Services à bord</h1><h2 class="pageSubTitle">Découvrez ce que vous réserve GNV pour un agréable séjour à bord de ses ferries:</h2><p>Des catégories de place qui répondent à toutes les exigences</p><ul style="font-size: medium; font-weight: normal;"><li><span style="line-height: 5px;">Restaurant à la carte, une cafétéria et un snack-bar.</span></li><li><span style="line-height: 5px;">Des activités de loisirs.</span></li><li><span style="line-height: 5px;">Des produits et des services conçus pour les enfants.</span></li><li><span style="line-height: 5px;">Possibilités de faire des achats.</span></li><li><span style="line-height: 5px;">Internet à bord.</span></li><li><span style="line-height: 5px;">Le service « Pets, Welcome on Board.</span></li></ul>\n\n
318	<h1 class="pageTitle">Services à bord</h1><h2 class="pageSubTitle">Découvrez ce que vous réserve GNV pour un agréable séjour à bord de ses ferries:</h2><p>Des catégories de place qui répondent à toutes les exigences</p><ul><li><span style="line-height: 5px;">Restaurant à la carte, une cafétéria et un snack-bar.</span></li><li><span style="line-height: 5px;">Des activités de loisirs.</span></li><li><span style="line-height: 5px;">Des produits et des services conçus pour les enfants.</span></li><li><span style="line-height: 5px;">Possibilités de faire des achats.</span></li><li><span style="line-height: 5px;">Internet à bord.</span></li><li><span style="line-height: 5px;">Le service « Pets, Welcome on Board.</span></li></ul>\n\n
409	<h1 class="pageTitle">Nos publicités</h1>\n<h2 class="pageSubTitle">Campagne 2013</h2>\n\n<table width="100" cellpadding="0">\n<tbody><tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-reception.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-commandant.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel chef de réception </strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel commandant</strong></td>\n</tr>\n<tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-sailor.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-service.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel marin</strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">Visuel serveuse</strong></td>\n</tr>\n</tbody></table>
410	<h1 class="pageTitle">إعلاناتنا</h1>\n<h2 class="pageSubTitle">حملة 2013</h2>\n\n<table width="100" cellpadding="0">\n<tbody><tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-reception.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/09/30/campaign2013-commandant.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">مدير مكتب الاستقبال</strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">قبطان</strong></td>\n</tr>\n\n<tr>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-sailor.jpg">\n</td>\n<td>\n<img src="/media/public/filer/filer_public/2013/10/01/campaign2013-service.jpg">\n</td>\n</tr>\n<tr>\n<td><strong style="color: #04529d; font-size: 15px;">بحار</strong></td>\n<td><strong style="color: #04529d; font-size: 15px;">نادلة</strong></td>\n</tr>\n</tbody></table>
393	<h1 class="pageTitle">A PROPOS DE GNV</h1>\n<h2 class="pageSubTitle">Grandi Navi Veloci - Passagers et marchandises</h2><p><span style="line-height: 5px;">Grandi Navi Veloci est l'une des principales compagnies de navigation italiennes, dans le secteur du cabotage et du transport de passagers, en mer Méditerranée. La compagnie a été créée en 1992 et c'est en 1993 qu'a été lancé son premier navire, le Majestic.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Le groupe possède aujourd'hui dix navires qui, avec les navires en location, au nombre de deux actuellement, assurent les liaisons vers la Sardaigne, la Sicile, l'Espagne, la Tunisie et le Maroc. Parallèlement aux services ciblés destinés aux passagers, la flotte de Grandi Navi Veloci s'est engagée dans le développement des lignes des autoroutes de la mer : le transport maritime représente une solution plus économique que le transport routier traditionnel et fournit une réponse valable au problème environnemental lié au trafic intensif de poids-lourds sur le réseau routier national.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Actuellement, le groupe Grandi Navi Veloci est détenu à 50 % par Marinvest, 35 % par Investitori Associati, 9,2 % par IDeA, 4,6 % par Charme tandis que la part restante de 1,2 % est répartie entre plusieurs autres investisseurs.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">L'histoire de Grandi Navi Veloci :</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1992 : naissance de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1999 : Grandi Navi Veloci entre en bourse – la seule compagnie italienne de navigation à l'époque – pour financer le développement de sa flotte, la plus moderne et la plus innovante d'Europe.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2003 : Grandi Navi Veloci arrive au Maghreb et inaugure la ligne Gênes – Tunis.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2004 : la famille Grimaldi et les fonds de Private Equity gérés par Permira parviennent à un accord pour poursuivre le développement de la compagnie, grâce à l'entrée dans le capital de Grandi Navi Veloci de Permira, à raison de 80 %, et de Grimaldi Holding, pour les 20 % restants.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2006 : la nouvelle structure de l'actionnariat se forme ; peu de temps avant la fin de l'année, les nouveaux partenaires - Investitori Associati, De Agostini, Charme et une partie des dirigeants - font leur entrée dans le capital de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2007 : Grandi Navi Veloci est désignée comme meilleure compagnie de navigation par les agents de voyage qui lui décernent le Bit Tourism Award 2007. Au mois de juin, Ariodante Valeri devient directeur général de la compagnie.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2008 : inauguration de la ligne Gênes – Barcelone – Tanger et premières liaisons avec le Maroc. C'est au cours de la même année que, sur les navires La Superba, La Suprema, Fantastic, Excellent et Majestic, un nouveau service réservé aux propriétaires d'animaux domestiques est mis en œuvre et proposé aux passagers.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2009 : la famille Grimaldi quitte définitivement le groupe d'actionnaires et la direction de Grandi Navi Veloci. La structure de l'actionnariat est ainsi répartie : Investitori Associati (67,1 %), IDeA Co-Investment Fund (20,1 %), Charme (9,2 %), dirigeants (2,1 %), autres actionnaires (1,5 %).</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2010 : Roberto Martinoli occupe les fonctions d'administrateur délégué et de président de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2011: le 2 mai, l’Assemblée Générale Extraordinaire des actionnaires de Grandi Navi Veloci délibère une augmentation du capital social réservé en faveur de Marinvest Srl, qui se voit attribuer une participation égale à 50% du capital social de Grandi Navi Veloci. Marinvest entre ainsi dans le sociétariat de Grandi Navi Veloci par une augmentation de capital en contrepartie d’une contribution et moyennant l’apport de trésorerie et de trois bateaux (SNAV Toscana, SNAV Lazio, SNAV Sardegna). La nouvelle équipe associée se répartit de la façon suivante : Marinvest (50%), Investitori Associati (35%), IDeA (9,2%), Charme (4,6%) et d’autres associés (1,2%).</span><br><span style="line-height: 5px;"></span></p>
425	<h1 class="pageTitle">A PROPOS DE GNV</h1>\n<h2 class="pageSubTitle">Grandi Navi Veloci - Passagers et marchandises</h2><p><span style="line-height: 5px;">Grandi Navi Veloci est l'une des principales compagnies de navigation italiennes, dans le secteur du cabotage et du transport de passagers, en mer Méditerranée. La compagnie a été créée en 1992 et c'est en 1993 qu'a été lancé son premier navire, le Majestic.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Le groupe possède aujourd'hui dix navires qui, avec les navires en location, au nombre de deux actuellement, assurent les liaisons vers la Sardaigne, la Sicile, l'Espagne, la Tunisie et le Maroc. Parallèlement aux services ciblés destinés aux passagers, la flotte de Grandi Navi Veloci s'est engagée dans le développement des lignes des autoroutes de la mer : le transport maritime représente une solution plus économique que le transport routier traditionnel et fournit une réponse valable au problème environnemental lié au trafic intensif de poids-lourds sur le réseau routier national.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Actuellement, le groupe Grandi Navi Veloci est détenu à 50 % par Marinvest, 35 % par Investitori Associati, 9,2 % par IDeA, 4,6 % par Charme tandis que la part restante de 1,2 % est répartie entre plusieurs autres investisseurs.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">L'histoire de Grandi Navi Veloci :</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1992 : naissance de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1999 : Grandi Navi Veloci entre en bourse – la seule compagnie italienne de navigation à l'époque – pour financer le développement de sa flotte, la plus moderne et la plus innovante d'Europe.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2003 : Grandi Navi Veloci arrive au Maghreb et inaugure la ligne Gênes – Tunis.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2004 : la famille Grimaldi et les fonds de Private Equity gérés par Permira parviennent à un accord pour poursuivre le développement de la compagnie, grâce à l'entrée dans le capital de Grandi Navi Veloci de Permira, à raison de 80 %, et de Grimaldi Holding, pour les 20 % restants.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2006 : la nouvelle structure de l'actionnariat se forme ; peu de temps avant la fin de l'année, les nouveaux partenaires - Investitori Associati, De Agostini, Charme et une partie des dirigeants - font leur entrée dans le capital de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2007 : Grandi Navi Veloci est désignée comme meilleure compagnie de navigation par les agents de voyage qui lui décernent le Bit Tourism Award 2007. Au mois de juin, Ariodante Valeri devient directeur général de la compagnie.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2008 : inauguration de la ligne Gênes – Barcelone – Tanger et premières liaisons avec le Maroc. C'est au cours de la même année que, sur les navires La Superba, La Suprema, Fantastic, Excellent et Majestic, un nouveau service réservé aux propriétaires d'animaux domestiques est mis en œuvre et proposé aux passagers.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2009 : la famille Grimaldi quitte définitivement le groupe d'actionnaires et la direction de Grandi Navi Veloci. La structure de l'actionnariat est ainsi répartie : Investitori Associati (67,1 %), IDeA Co-Investment Fund (20,1 %), Charme (9,2 %), dirigeants (2,1 %), autres actionnaires (1,5 %).</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2010 : Roberto Martinoli occupe les fonctions d'administrateur délégué et de président de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2011: le 2 mai, l’Assemblée Générale Extraordinaire des actionnaires de Grandi Navi Veloci délibère une augmentation du capital social réservé en faveur de Marinvest Srl, qui se voit attribuer une participation égale à 50% du capital social de Grandi Navi Veloci. Marinvest entre ainsi dans le sociétariat de Grandi Navi Veloci par une augmentation de capital en contrepartie d’une contribution et moyennant l’apport de trésorerie et de trois bateaux (SNAV Toscana, SNAV Lazio, SNAV Sardegna). La nouvelle équipe associée se répartit de la façon suivante : Marinvest (50%), Investitori Associati (35%), IDeA (9,2%), Charme (4,6%) et d’autres associés (1,2%).</span><br><span style="line-height: 5px;"></span></p>
426	<h1 class="pageTitle">A PROPOS DE GNV</h1>\n<h2 class="pageSubTitle">Grandi Navi Veloci - Passagers et marchandises</h2><p><span style="line-height: 5px;">Grandi Navi Veloci est l'une des principales compagnies de navigation italiennes, dans le secteur du cabotage et du transport de passagers, en mer Méditerranée. La compagnie a été créée en 1992 et c'est en 1993 qu'a été lancé son premier navire, le Majestic.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Le groupe possède aujourd'hui dix navires qui, avec les navires en location, au nombre de deux actuellement, assurent les liaisons vers la Sardaigne, la Sicile, l'Espagne, la Tunisie et le Maroc. Parallèlement aux services ciblés destinés aux passagers, la flotte de Grandi Navi Veloci s'est engagée dans le développement des lignes des autoroutes de la mer : le transport maritime représente une solution plus économique que le transport routier traditionnel et fournit une réponse valable au problème environnemental lié au trafic intensif de poids-lourds sur le réseau routier national.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">Actuellement, le groupe Grandi Navi Veloci est détenu à 50 % par Marinvest, 35 % par Investitori Associati, 9,2 % par IDeA, 4,6 % par Charme tandis que la part restante de 1,2 % est répartie entre plusieurs autres investisseurs.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">L'histoire de Grandi Navi Veloci :</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1992 : naissance de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">1999 : Grandi Navi Veloci entre en bourse – la seule compagnie italienne de navigation à l'époque – pour financer le développement de sa flotte, la plus moderne et la plus innovante d'Europe.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2003 : Grandi Navi Veloci arrive au Maghreb et inaugure la ligne Gênes – Tunis.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2004 : la famille Grimaldi et les fonds de Private Equity gérés par Permira parviennent à un accord pour poursuivre le développement de la compagnie, grâce à l'entrée dans le capital de Grandi Navi Veloci de Permira, à raison de 80 %, et de Grimaldi Holding, pour les 20 % restants.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2006 : la nouvelle structure de l'actionnariat se forme ; peu de temps avant la fin de l'année, les nouveaux partenaires - Investitori Associati, De Agostini, Charme et une partie des dirigeants - font leur entrée dans le capital de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2007 : Grandi Navi Veloci est désignée comme meilleure compagnie de navigation par les agents de voyage qui lui décernent le Bit Tourism Award 2007. Au mois de juin, Ariodante Valeri devient directeur général de la compagnie.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2008 : inauguration de la ligne Gênes – Barcelone – Tanger et premières liaisons avec le Maroc. C'est au cours de la même année que, sur les navires La Superba, La Suprema, Fantastic, Excellent et Majestic, un nouveau service réservé aux propriétaires d'animaux domestiques est mis en œuvre et proposé aux passagers.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2009 : la famille Grimaldi quitte définitivement le groupe d'actionnaires et la direction de Grandi Navi Veloci. La structure de l'actionnariat est ainsi répartie : Investitori Associati (67,1 %), IDeA Co-Investment Fund (20,1 %), Charme (9,2 %), dirigeants (2,1 %), autres actionnaires (1,5 %).</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2010 : Roberto Martinoli occupe les fonctions d'administrateur délégué et de président de Grandi Navi Veloci.</span><br><span style="line-height: 5px;"></span></p><p><span style="line-height: 5px;">2011: le 2 mai, l’Assemblée Générale Extraordinaire des actionnaires de Grandi Navi Veloci délibère une augmentation du capital social réservé en faveur de Marinvest Srl, qui se voit attribuer une participation égale à 50% du capital social de Grandi Navi Veloci. Marinvest entre ainsi dans le sociétariat de Grandi Navi Veloci par une augmentation de capital en contrepartie d’une contribution et moyennant l’apport de trésorerie et de trois bateaux (SNAV Toscana, SNAV Lazio, SNAV Sardegna). La nouvelle équipe associée se répartit de la façon suivante : Marinvest (50%), Investitori Associati (35%), IDeA (9,2%), Charme (4,6%) et d’autres associés (1,2%).</span><br><span style="line-height: 5px;"></span></p>
\.


--
-- Data for Name: cmsplugin_topicpluginmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_topicpluginmodel (cmsplugin_ptr_id, topics_id) FROM stdin;
280	5
294	6
296	6
311	7
312	7
319	7
320	7
431	6
432	6
437	5
438	5
253	5
\.


--
-- Data for Name: cmsplugin_twitterrecententries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_twitterrecententries (cmsplugin_ptr_id, title, twitter_user, count, link_hint) FROM stdin;
\.


--
-- Data for Name: cmsplugin_twittersearch; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cmsplugin_twittersearch (cmsplugin_ptr_id, title, query, count) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
1	2013-09-27 08:25:45.360462+01	1	15	1	ACCEUIL	1	
2	2013-09-27 08:26:22.53186+01	1	15	2	SOCIETE	1	
3	2013-09-27 08:26:36.240637+01	1	15	3	SERVICES	1	
4	2013-09-27 08:27:01.28059+01	1	15	4	DESTINATIONS	1	
5	2013-09-27 08:27:12.905955+01	1	15	5	VISISTER GNV	1	
6	2013-09-27 08:27:25.900627+01	1	15	6	COMPAGNE 2013	1	
7	2013-09-27 08:27:42.582643+01	1	15	7	CONTACT	1	
8	2013-09-27 08:29:06.346463+01	1	15	1	ACCEUIL	2	Aucun champ modifié.
9	2013-09-27 08:30:18.606916+01	1	15	2	SOCIETE	2	Modifié slug.
10	2013-09-27 08:30:31.8406+01	1	15	3	SERVICES	2	Modifié slug.
11	2013-09-27 08:30:41.247615+01	1	15	4	DESTINATIONS	2	Modifié slug.
12	2013-09-27 08:30:50.209379+01	1	15	5	VISISTER GNV	2	Modifié slug.
13	2013-09-27 08:30:58.148298+01	1	15	6	COMPAGNE 2013	2	Modifié slug.
14	2013-09-27 08:31:05.367634+01	1	15	7	CONTACT	2	Aucun champ modifié.
15	2013-09-27 08:40:12.510841+01	1	15	1	ACCEUIL	2	Modifié title, slug et language.
16	2013-09-27 08:46:44.750051+01	1	24	1	collection	1	
17	2013-09-27 08:46:53.751703+01	1	15	1	ACCEUIL	2	Aucun champ modifié.
18	2013-09-27 09:49:38.677793+01	1	24	1	Fall Promotions	2	Changed title. Added deal "GENES - MAROC OFFRE FAMILIALE". Changed title, url, price, image and description for deal "GENES - TUNIS OFFRE FAMILIALE". Changed title, url, price, image and description for deal "Promotion MAROC - GENOVA".
19	2013-09-27 09:50:44.340608+01	1	30	1	GENES - MAROC OFFRE FAMILIALE	1	
20	2013-09-27 09:51:53.641726+01	1	30	1	La campagne 2013	2	Changed title and caption.
21	2013-09-27 09:52:53.867302+01	1	30	2	1993 - 2013	1	
22	2013-09-27 11:20:54.793378+01	1	24	1	Fall Promotions	2	Changed published for deal "GENES - TUNIS OFFRE FAMILIALE". Changed published for deal "Promotion MAROC - GENOVA". Changed published for deal "GENES - MAROC OFFRE FAMILIALE".
23	2013-09-27 11:23:50.424971+01	1	24	1	Fall Promotions	2	Changed sidebar for deal "GENES - TUNIS OFFRE FAMILIALE". Changed sidebar for deal "Promotion MAROC - GENOVA". Changed sidebar for deal "GENES - MAROC OFFRE FAMILIALE".
24	2013-09-27 11:26:36.831163+01	1	30	2	1993 - 2013	2	Changed sidebar.
25	2013-09-27 11:26:41.889594+01	1	30	1	La campagne 2013	2	Changed sidebar.
26	2013-09-27 11:38:25.376167+01	1	25	1	Home Page	1	
27	2013-09-27 11:57:22.431878+01	1	15	1	HOME	2	No fields changed.
28	2013-09-27 12:05:18.944434+01	1	15	4	DESTINATIONS	2	Changed title and slug.
29	2013-09-27 12:05:32.310721+01	1	15	4	DESTINATIONS	2	Changed language.
30	2013-09-27 12:13:50.025196+01	1	15	1	HOME	2	Changed title, slug and language.
31	2013-09-27 13:12:17.166642+01	1	24	1	Fall Promotions	2	Changed caption for deal "GENES - TUNIS OFFRE FAMILIALE". Changed caption for deal "Promotion MAROC - GENOVA". Changed caption for deal "GENES - MAROC OFFRE FAMILIALE".
32	2013-09-27 13:15:53.180017+01	1	15	1	HOME	2	No fields changed.
33	2013-09-27 13:19:40.497946+01	1	43	1	Moteur de recherche	1	
34	2013-09-27 13:19:51.260501+01	1	15	1	HOME	2	No fields changed.
35	2013-09-27 13:26:52.125524+01	1	43	1	Moteur de recherche	2	Changed html.
36	2013-09-27 13:30:30.19509+01	1	43	1	Moteur de recherche	2	Changed html.
37	2013-09-27 13:42:37.012316+01	1	43	1	Moteur de recherche	2	Changed html.
38	2013-09-27 13:43:45.911129+01	1	15	1	HOME	2	No fields changed.
39	2013-09-27 14:10:08.834461+01	1	43	1	Moteur de recherche	2	Changed html.
40	2013-09-27 14:21:07.53087+01	1	15	1	ACCEUIL	2	Changed language.
41	2013-09-27 14:21:28.452191+01	1	15	1	HOME	2	Changed language.
42	2013-09-27 14:23:04.663979+01	1	15	2	COMPANY	2	Changed title and slug.
43	2013-09-27 14:23:10.613514+01	1	15	2	COMPANY	2	No fields changed.
44	2013-09-27 14:23:26.252518+01	1	15	2	COMPANY	2	Changed title, slug and language.
45	2013-09-27 14:23:48.120463+01	1	15	3	SERVICES	2	Changed title and slug.
46	2013-09-27 14:24:16.274792+01	1	15	3	SERVICES	2	Changed title, slug and language.
47	2013-09-27 14:25:01.381964+01	1	15	4	DESTINATIONS	2	Changed title, slug and language.
48	2013-09-27 14:25:25.064741+01	1	15	5	VISISTER GNV	2	Changed title, slug and language.
49	2013-09-27 14:25:34.883706+01	1	15	5	VISIT GNV	2	Changed title and slug.
50	2013-09-27 14:26:08.77754+01	1	15	5	VISIT GNV	2	Changed title, slug and language.
51	2013-09-27 15:19:58.562345+01	1	15	6	COMPAGNE 2013	2	Changed language.
52	2013-09-27 15:21:08.035675+01	1	15	6	COMPAGNE 2013	2	Changed title, slug and language.
53	2013-09-27 15:21:29.773275+01	1	15	6	CAMPAIGN 2013	2	Changed title and slug.
54	2013-09-27 15:21:44.441013+01	1	15	6	CAMPAIGN 2013	2	Changed title, slug and language.
55	2013-09-27 15:22:16.512355+01	1	15	7	CONTACT	2	Changed title and slug.
56	2013-09-27 15:22:31.842641+01	1	15	7	CONTACT	2	Changed title, slug and language.
57	2013-09-27 15:23:52.298613+01	1	15	7	CONTACT	2	No fields changed.
58	2013-09-27 15:26:45.947788+01	1	15	7	CONTACT	2	No fields changed.
59	2013-09-27 15:28:36.684395+01	1	15	7	CONTACT	2	Changed language.
60	2013-09-27 15:29:18.409228+01	1	15	7	CONTACT	2	Changed language.
61	2013-09-27 15:41:30.040862+01	1	15	7	CONTACT	2	No fields changed.
62	2013-09-27 17:13:46.580596+01	1	15	1	HOME	2	No fields changed.
63	2013-09-27 17:14:11.731875+01	1	15	1	ACCEUIL	2	Changed language.
64	2013-09-27 17:14:29.477357+01	1	15	1	HOME	2	Changed language.
65	2013-09-27 17:37:44.068535+01	1	11	5	topicThumb-destination.jpg	3	
66	2013-09-27 17:37:51.676825+01	1	11	1	1234931504682.jpg	3	
67	2013-09-27 17:37:56.21957+01	1	11	3	news-1993_2013.jpg	3	
68	2013-09-27 17:38:01.783969+01	1	11	4	promo-genes_tunis.jpg	3	
69	2013-09-27 17:38:06.703627+01	1	11	8	topicThumb-service.jpg	3	
70	2013-09-27 17:38:10.994745+01	1	11	6	topicThumb-visitGnv.jpg	3	
71	2013-09-27 17:38:14.901812+01	1	11	7	VisitGNV-3---1024_05.jpg	3	
72	2013-09-27 17:41:37.02208+01	1	25	1	Home Page	2	Changed image for topic "VISITEZ GNV". Changed image for topic "DESTINATIONS". Changed image for topic "SERVICES".
73	2013-09-27 17:42:07.160425+01	1	30	2	1993 - 2013	2	Changed image.
74	2013-09-27 17:42:44.8221+01	1	30	1	La campagne 2013	2	Changed image.
75	2013-09-27 17:49:15.793307+01	1	24	1	Fall Promotions	2	Changed image for deal "GENES - TUNIS OFFRE FAMILIALE".
76	2013-09-27 17:56:13.905355+01	1	24	1	Fall Promotions	2	Changed image for deal "Promotion MAROC - GENOVA".
77	2013-09-27 17:56:34.116354+01	1	24	1	Fall Promotions	2	Changed image for deal "GENES - MAROC OFFRE FAMILIALE".
78	2013-09-27 18:05:55.315767+01	1	15	5	VISITER GNV	2	Changed language.
79	2013-09-27 18:06:18.096768+01	1	15	5	VISIT GNV	2	Changed language.
80	2013-09-27 18:17:27.79161+01	1	15	5	VISIT GNV	2	No fields changed.
81	2013-09-27 18:17:47.554217+01	1	15	5	VISITER GNV	2	Changed language.
82	2013-09-27 18:18:30.400875+01	1	15	5	VISITER GNV	2	Changed language.
83	2013-09-27 18:18:41.486875+01	1	15	5	VISIT GNV	2	Changed language.
84	2013-09-27 18:18:44.295685+01	1	15	5	VISIT GNV	2	Changed language.
85	2013-09-27 18:20:24.699981+01	1	15	5	VISITER GNV	2	Changed language.
86	2013-09-27 18:22:35.784134+01	1	15	5	VISITER GNV	2	Changed language.
87	2013-09-27 18:22:39.087034+01	1	15	5	VISITER GNV	2	Changed language.
88	2013-09-27 18:30:10.854387+01	1	25	2	Home Topics - AR	1	
89	2013-09-27 18:30:24.787494+01	1	25	1	Home Topics - FR	2	Changed title.
90	2013-09-27 18:31:53.06772+01	1	25	2	Home Topics - AR	2	Added topic "رحلات". Added topic "الخدمات".
91	2013-09-27 18:32:08.880128+01	1	15	1	HOME	2	Changed language.
92	2013-09-27 18:34:00.101411+01	1	15	1	HOME	2	Changed language.
93	2013-09-27 18:36:57.440255+01	1	15	3	SERVICES	2	Changed language.
94	2013-09-27 18:38:10.718094+01	1	15	5	VISITER GNV	2	Changed language.
95	2013-09-27 18:40:22.707539+01	1	15	5	VISIT GNV	2	No fields changed.
96	2013-09-27 18:56:37.244813+01	1	15	3	SERVICES	2	Changed language.
97	2013-09-27 18:56:52.290658+01	1	15	3	SERVICES	2	No fields changed.
98	2013-09-27 19:02:55.281201+01	1	25	3	Destinantions Topics - FR	1	
99	2013-09-27 19:05:43.099624+01	1	15	4	DESTINATIONS	2	Changed language.
100	2013-09-27 19:06:13.043542+01	1	15	4	DESTINATIONS	2	No fields changed.
101	2013-09-27 19:24:42.702557+01	1	25	4	Services Topics - FR	1	
102	2013-09-27 19:26:21.362648+01	1	15	3	SERVICES	2	Changed language.
103	2013-09-27 19:28:23.21105+01	1	15	3	SERVICES	2	Changed language.
104	2013-09-27 19:28:36.505158+01	1	15	3	SERVICES	2	No fields changed.
105	2013-09-27 19:30:11.518622+01	1	15	7	CONTACT	2	Changed language.
106	2013-09-27 19:31:28.713329+01	1	15	7	CONTACT	2	Changed language.
107	2013-09-27 19:36:20.090085+01	1	15	3	SERVICES	2	No fields changed.
108	2013-09-27 19:37:39.261842+01	1	15	3	SERVICES	2	Changed language.
109	2013-09-27 19:40:22.852354+01	1	15	3	SERVICES	2	No fields changed.
110	2013-09-27 19:48:23.693974+01	1	15	7	CONTACT	2	No fields changed.
111	2013-09-27 19:48:42.544306+01	1	15	7	CONTACT	2	Changed language.
112	2013-09-27 19:48:52.322455+01	1	15	6	CAMPAIGN 2013	2	
113	2013-09-27 19:48:54.716523+01	1	15	2	COMPANY	2	
114	2013-09-27 19:49:48.237838+01	1	15	2	COMPANY	2	
115	2013-09-27 19:51:01.564429+01	1	15	2	COMPANY	2	Changed title and slug.
116	2013-09-27 19:51:35.960027+01	1	15	2	PROMOTIONS	2	No fields changed.
117	2013-09-27 19:52:47.67205+01	1	15	2	SOCIETE	2	Changed title, slug and language.
118	2013-09-27 19:52:59.018331+01	1	15	2	PROMOTIONS	2	Changed title, slug and language.
119	2013-09-28 11:42:31.773262+01	1	30	2		3	
120	2013-09-28 11:42:53.528468+01	1	30	1		3	
121	2013-09-28 11:46:40.157386+01	1	24	1	Promotion1	2	Modifié title_fr et title_en. title_fr, title_en, caption_fr, caption_en, description_fr, description_en et description_ar modifié pour deal « Promotion2 ». title_fr, title_en, caption_fr, caption_en, description_fr, description_en et description_ar modifié pour deal « Promotion3 ». deal « Promotion1 » supprimé.
122	2013-09-28 11:49:29.029435+01	1	30	3	News1	1	
123	2013-09-28 11:51:46.291823+01	1	24	1	عروض1	2	عدّل title_ar. غيّر title_ar و caption_ar في deal "\t صفقة". غيّر title_ar و caption_ar في deal "\t صفقة".
124	2013-09-28 11:57:07.511569+01	1	25	4	Sujets	2	Modifié title_fr, title_en et title_ar. title_fr, title_en, title_ar, description_fr, description_en et description_ar modifié pour topic « Sujet1 ». title_fr, title_en, title_ar, description_fr, description_en et description_ar modifié pour topic « sujet2 ». title_fr, title_en, title_ar, description_fr, description_en et description_ar modifié pour topic « sujet3 ». title_fr, title_en, title_ar, description_fr, description_en et description_ar modifié pour topic « sujet4 ». title_fr, title_en, title_ar, description_fr, description_en et description_ar modifié pour topic « sujet5 ». title_fr, title_en, title_ar, description_fr, description_en et description_ar modifié pour topic « sujet6 ». title_fr, title_en, title_ar, description_fr, description_en et description_ar modifié pour topic « sujet7 ».
125	2013-09-28 11:57:55.611216+01	1	25	4	Sujets1	2	Modifié title_fr, title_en et title_ar.
126	2013-09-28 11:59:32.27145+01	1	24	1	Promotions1	2	Modifié title_fr. title_fr, title_en, title_ar, caption_fr, caption_en et caption_ar modifié pour deal « Promotion1 ». title_fr, title_en, title_ar, caption_fr, caption_en et caption_ar modifié pour deal « Promotion2 ».
127	2013-09-28 12:04:42.585774+01	1	30	8	News2	2	Modifié title_fr, title_en et title_ar.
128	2013-09-28 12:04:52.554545+01	1	30	7	News3	2	Modifié title_fr, title_en et title_ar.
129	2013-09-28 12:05:02.400658+01	1	30	6	News3	2	Modifié title_fr, title_en et title_ar.
130	2013-09-28 12:05:24.456732+01	1	30	5	News4	2	Modifié title_fr, title_en, title_ar, caption_fr, caption_en et caption_ar.
131	2013-09-28 12:05:45.405548+01	1	30	9	News1	2	Modifié title_ar, caption_fr, caption_en et caption_ar.
132	2013-09-28 12:05:58.86243+01	1	30	8	News2	2	Modifié caption_fr, caption_en et caption_ar.
133	2013-09-28 12:06:11.972511+01	1	30	7	News3	2	Modifié caption_fr, caption_en et caption_ar.
134	2013-09-28 12:06:36.19395+01	1	30	6	News4	2	Modifié title_fr, title_en, title_ar, caption_fr, caption_en et caption_ar.
135	2013-09-28 12:07:01.773027+01	1	30	5	News5	2	Modifié title_fr, title_en, title_ar, caption_fr, caption_en et caption_ar.
136	2013-09-28 12:07:20.500009+01	1	30	4	News6	2	Modifié title_fr, title_en, title_ar, caption_fr, caption_en et caption_ar.
137	2013-09-28 12:07:37.420224+01	1	30	3	News7	2	Modifié title_fr, title_en, title_ar, caption_fr, caption_en et caption_ar.
138	2013-09-28 12:08:08.239795+01	1	25	3		3	
139	2013-09-28 12:08:08.24946+01	1	25	2		3	
140	2013-09-28 12:08:08.257818+01	1	25	1		3	
141	2013-09-28 12:09:27.19513+01	1	15	1	ACCEUIL	2	Aucun champ modifié.
142	2013-09-28 12:10:45.175543+01	1	15	1	HOME	2	Modifié language.
143	2013-09-29 16:29:21.75557+01	1	15	15	Booking	1	
144	2013-09-29 16:30:13.136188+01	1	15	15	Booking	2	Modifié published et in_navigation.
145	2013-09-29 16:30:31.449511+01	1	15	15	Booking	2	Modifié title, slug et language.
146	2013-09-29 16:38:39.272564+01	1	43	1	Moteur de recherche	2	Modifié html.
147	2013-09-29 16:39:08.397955+01	1	15	15	Booking	2	Aucun champ modifié.
148	2013-09-30 09:05:06.793071+01	1	25	5	Accueil	1	
149	2013-09-30 09:05:27.965999+01	1	15	1	إستقبال	2	Modifié language.
150	2013-09-30 09:05:39.225815+01	1	25	4	Sujets1	3	
151	2013-09-30 09:06:26.183398+01	1	15	1	إستقبال	2	Modifié language.
152	2013-09-30 09:11:10.784231+01	1	24	1	Promotions1	2	description_ar modifié pour deal « Promotion1 ». description_ar modifié pour deal « Promotion2 ».
153	2013-09-30 09:16:00.606455+01	1	24	1	Promotions1	2	deal « GENES - MAROC OFFRE FAMILIALE » ajouté. title_fr, title_ar, caption_fr, caption_ar, image et description_fr modifié pour deal « GENES - TUNIS OFFRE FAMILIALE ». title_fr, title_ar, caption_fr, caption_ar, price, image et description_fr modifié pour deal « Promotion MAROC - GENOVA ».
154	2013-09-30 09:16:32.080982+01	1	24	1	Promotions1	2	description_ar, published et sidebar modifié pour deal « GENES - MAROC OFFRE FAMILIALE ».
155	2013-09-30 09:21:07.529863+01	1	30	9	La campagne 2013	2	Modifié title_fr, title_ar, caption_fr, caption_ar, description_fr et description_ar.
156	2013-09-30 09:21:16.559945+01	1	30	7	News3	3	
157	2013-09-30 09:21:16.567609+01	1	30	6	News4	3	
158	2013-09-30 09:21:16.576052+01	1	30	5	News5	3	
159	2013-09-30 09:21:16.584364+01	1	30	4	News6	3	
160	2013-09-30 09:21:16.59271+01	1	30	3	News7	3	
161	2013-09-30 09:22:43.575538+01	1	30	8	1993 - 2013	2	Modifié title_fr, title_ar, image, caption_fr, caption_ar, description_fr et description_ar.
162	2013-09-30 09:32:28.638177+01	1	15	15	Booking	2	
163	2013-09-30 09:32:39.685035+01	1	15	15	Booking	2	
164	2013-09-30 09:41:00.131977+01	1	25	6	Destinations	1	
165	2013-09-30 09:41:17.2557+01	1	15	4	DESTINATIONS	2	Aucun champ modifié.
166	2013-09-30 09:41:29.491891+01	1	15	4	DESTINATIONS	2	Aucun champ modifié.
167	2013-09-30 09:41:42.925774+01	1	15	4	وجهات السفر	2	Modifié language.
168	2013-09-30 09:55:15.081677+01	1	15	3	SERVICES	2	Aucun champ modifié.
169	2013-09-30 09:55:23.880622+01	1	15	3	الخدمات	2	Modifié language.
170	2013-09-30 10:00:52.124527+01	1	25	7	Services	1	
171	2013-09-30 10:01:07.478235+01	1	15	3	SERVICES	2	Aucun champ modifié.
172	2013-09-30 10:01:18.450604+01	1	15	3	الخدمات	2	Modifié language.
173	2013-09-30 10:01:19.809298+01	1	15	3	الخدمات	2	Modifié language.
174	2013-09-30 10:05:09.71584+01	1	25	7	Services	2	description_fr et description_ar modifié pour topic « REUNION  ».
175	2013-09-30 10:17:23.794719+01	1	15	1	ACCEUIL	2	Modifié slug.
176	2013-09-30 10:17:55.704736+01	1	15	5	VISITER GNV	2	Modifié slug.
177	2013-09-30 10:18:01.62622+01	1	15	5	زيارة GNV	2	Modifié language.
178	2013-09-30 10:33:07.140333+01	1	15	6	CAMPAGNE 2013	2	Aucun champ modifié.
179	2013-09-30 10:35:07.535698+01	1	15	6	حملة 2013	2	Modifié language.
180	2013-09-30 10:35:11.81824+01	1	15	6	CAMPAGNE 2013	2	Aucun champ modifié.
181	2013-09-30 10:35:48.962123+01	1	15	6	CAMPAGNE 2013	2	Aucun champ modifié.
182	2013-09-30 10:36:01.809814+01	1	15	6	حملة 2013	2	Modifié language.
183	2013-09-30 10:38:35.248293+01	1	15	6	CAMPAGNE 2013	2	Aucun champ modifié.
184	2013-09-30 10:38:40.952498+01	1	15	6	حملة 2013	2	Modifié language.
185	2013-09-30 10:52:45.289684+01	1	11	43	homeAR-banner.jpg	3	
186	2013-09-30 10:53:32.472963+01	1	14	15	homeBanner-sample.jpg	2	Modifié file.
187	2013-09-30 10:53:59.68317+01	1	14	15	home-banner	2	Modifié name.
188	2013-09-30 10:54:13.608578+01	1	14	15	homeBanner-sample.jpg	2	Modifié name.
189	2013-09-30 10:54:28.756797+01	1	14	41	home-banner.jpg	2	Modifié name.
190	2013-09-30 10:55:12.224765+01	1	14	41	homeAR-banner.jpg	2	Modifié name.
191	2013-09-30 10:55:23.784911+01	1	14	15	home-banner.jpg	2	Modifié name.
192	2013-09-30 10:59:34.220638+01	1	43	2	homeSlider	1	
193	2013-09-30 11:00:54.236888+01	1	15	1	ACCEUIL	2	Aucun champ modifié.
194	2013-09-30 11:03:03.787787+01	1	15	1	ACCEUIL	2	Aucun champ modifié.
195	2013-09-30 11:03:13.934689+01	1	15	1	إستقبال	2	Modifié language.
196	2013-09-30 12:03:54.484452+01	1	9	11	restaurant	2	Modifié name.
197	2013-09-30 23:09:25.499829+01	1	15	6	CAMPAGNE 2013	2	Modifié title et slug.
198	2013-09-30 23:09:37.404184+01	1	15	6	حملة 2013	2	Modifié title, slug et language.
199	2013-09-30 23:10:40.248957+01	1	15	1	ACCEUIL	2	Aucun champ modifié.
200	2013-09-30 23:11:06.557692+01	1	15	1	إستقبال	2	Modifié language.
201	2013-09-30 23:12:09.509226+01	1	15	5	زيارة GNV	2	Modifié language.
202	2013-09-30 23:15:38.852784+01	1	15	17	à propos	1	
203	2013-09-30 23:18:11.300784+01	1	15	17	à propos	2	Aucun champ modifié.
204	2013-09-30 23:19:55.063698+01	1	15	17	à propos	2	Aucun champ modifié.
205	2013-09-30 23:20:58.950841+01	1	15	17	à propos	2	Modifié title, slug, published et language.
206	2013-09-30 23:21:15.983562+01	1	15	17	تقديم	2	Modifié language.
207	2013-09-30 23:22:21.05492+01	1	15	17	à propos	2	Aucun champ modifié.
208	2013-09-30 23:22:33.356926+01	1	15	17	تقديم	2	Modifié language.
209	2013-10-01 15:24:52.970258+01	1	14	37	campaign2013-commandant.jpg	2	Modifié file.
210	2013-10-01 15:25:14.314774+01	1	14	36	campaign2013-reception.jpg	2	Modifié file.
211	2013-10-01 15:28:50.082155+01	1	15	6	Communication	2	Aucun champ modifié.
212	2013-10-01 15:30:59.209038+01	1	15	6	اتصالات	2	Modifié language.
213	2013-10-01 15:47:30.309789+01	1	43	3	TVads2013	1	
214	2013-10-01 15:47:40.287759+01	1	15	6	Communication	2	Aucun champ modifié.
215	2013-10-01 15:47:52.359965+01	1	15	6	اتصالات	2	Modifié language.
216	2013-10-01 15:48:32.617931+01	1	43	3	TVads2013	2	Modifié html.
217	2013-10-02 07:27:41.883724+01	1	43	2	360°	2	Modifié name et template.
218	2013-10-02 08:20:32.638624+01	1	15	17	à propos	2	Aucun champ modifié.
219	2013-10-02 08:20:48.788891+01	1	15	17	تقديم	2	Modifié language.
220	2013-10-02 08:21:05.294951+01	1	15	5	زيارة GNV	2	Modifié language.
221	2013-10-02 08:22:04.080775+01	1	30	9	La campagne 2013	2	Modifié description_fr et description_ar.
222	2013-10-02 08:22:14.272663+01	1	30	8	1993 - 2013	2	Modifié description_fr et description_ar.
223	2013-10-02 08:25:05.792524+01	1	25	5	Accueil	2	description_fr et description_ar modifié pour topic « VISITEZ GNV ».
224	2013-10-02 08:25:35.791742+01	1	25	5	Accueil	2	description_ar modifié pour topic « VISITEZ GNV ».
225	2013-10-02 08:27:23.203838+01	1	25	5	Accueil	2	description_fr et description_ar modifié pour topic « DESTINATIONS ». description_fr et description_ar modifié pour topic « SERVICES ».
226	2013-10-03 07:28:25.562474+01	1	25	5	Accueil	2	url, description_fr et description_ar modifié pour topic « VISITEZ GNV ». url modifié pour topic « DESTINATIONS ». url modifié pour topic « SERVICES ».
227	2013-10-03 07:30:10.244164+01	1	30	9	La campagne	2	Modifié title_fr, title_ar et url.
228	2013-10-03 07:30:55.563338+01	1	30	9	La campagne	2	Modifié url.
229	2013-10-03 07:31:21.450056+01	1	30	9	La campagne	2	Modifié url.
230	2013-10-03 07:32:40.226931+01	1	30	9	La campagne	2	Modifié url.
231	2013-10-03 07:33:03.936357+01	1	25	5	Accueil	2	url modifié pour topic « VISITEZ GNV ». url modifié pour topic « DESTINATIONS ». url modifié pour topic « SERVICES ».
232	2013-10-03 07:37:36.931274+01	1	25	7	Services	2	description_fr et description_ar modifié pour topic « AVANT DE PARTIR ». description_fr et description_ar modifié pour topic « SERVICES À BORD ». description_fr et description_ar modifié pour topic « GUIDE ». description_fr et description_ar modifié pour topic « CARTE FIDÉLITÉ ». description_fr et description_ar modifié pour topic « QUALITÉ ». description_fr et description_ar modifié pour topic « ARRIVÉE ».
233	2013-10-03 07:40:36.824576+01	1	25	7	Services	2	description_fr modifié pour topic « REUNION  ».
234	2013-10-03 07:46:04.515854+01	1	15	4	وجهات السفر	2	Modifié language.
235	2013-10-03 07:48:42.684209+01	1	25	6	Destinations	2	url modifié pour topic « NAPLES (CAPANIE) ». url modifié pour topic « PALERME (SICILE) ». url modifié pour topic « TERMINI IMERSE (SICILE) ». url modifié pour topic « PORTO TORRES (SARDAIGNE) ». title_fr, title_ar et url modifié pour topic « GÉNES ( LIGURIE) ». url modifié pour topic «  CIVITAVECCHIA (ROME) ». url modifié pour topic « BARCELONE (ESPAGNE) ». url modifié pour topic « TUNIS (TUNISIE) ». url modifié pour topic « TANGER (MAROC) ». url modifié pour topic « NADOR (MAROC) ». url modifié pour topic « SÉTE (FRANCE) ».
236	2013-10-13 22:20:53.765225+01	1	25	6	Destinations	2	title_fr et title_ar modifié pour topic « NAPLES (CAMPANIE) ».
237	2013-10-13 22:29:03.548446+01	1	14	15	home-banner.jpg	2	Modifié file.
238	2013-10-13 22:32:33.460412+01	1	14	15	home-banner.jpg	2	Modifié file.
239	2013-10-13 22:33:38.989457+01	1	14	15	home-banner.jpg	2	Modifié file.
240	2013-10-13 22:33:54.601656+01	1	14	15	home-banner.jpg	2	Modifié file.
241	2013-10-13 22:34:16.467393+01	1	15	1	ACCEUIL	2	Aucun champ modifié.
242	2013-10-13 22:35:04.788562+01	1	15	2	PROMOTIONS	2	Modifié title et slug.
243	2013-10-13 22:35:17.08913+01	1	15	2	الفرص	2	Modifié slug et language.
244	2013-10-13 22:36:01.172385+01	1	15	2	الفرص	2	Modifié title et language.
245	2013-10-13 22:43:39.412143+01	1	30	8	1993 - 2013	3	
246	2013-10-13 22:46:36.001323+01	1	30	9	LE PROGRAMME DE FIDÉLITÉ	2	Modifié title_fr, title_ar, image, caption_fr et caption_ar.
247	2013-10-13 22:52:55.461472+01	1	14	62	fidelityCard.png	2	Modifié file.
248	2013-10-13 22:54:44.311269+01	1	30	9	LE PROGRAMME DE FIDÉLITÉ	2	Modifié url.
249	2013-10-13 22:55:17.952092+01	1	15	19	Carte de fidélité	1	
250	2013-10-13 22:56:12.075048+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
251	2013-10-13 22:56:51.577199+01	1	15	19	Carte de fidélité	2	Modifié title, slug et language.
252	2013-10-13 22:56:56.265847+01	1	15	19	Carte de fidélité	2	
253	2013-10-13 22:59:22.444859+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
254	2013-10-13 23:37:25.473711+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
255	2013-10-13 23:43:33.436085+01	1	15	19	بطاقة الولاء	2	Modifié language.
256	2013-10-13 23:52:58.400771+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
257	2013-10-13 23:58:11.971195+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
258	2013-10-14 00:01:56.063227+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
259	2013-10-14 00:02:28.924867+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
260	2013-10-14 00:04:25.212387+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
261	2013-10-14 00:05:27.54052+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
262	2013-10-14 00:05:51.050383+01	1	15	19	Carte de fidélité	2	Aucun champ modifié.
263	2013-10-14 00:06:10.739239+01	1	15	19	بطاقة الولاء	2	Modifié language.
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	content type	contenttypes	contenttype
5	session	sessions	session
6	site	sites	site
7	migration history	south	migrationhistory
8	log entry	admin	logentry
9	Folder	filer	folder
10	folder permission	filer	folderpermission
11	file	filer	file
12	clipboard	filer	clipboard
13	clipboard item	filer	clipboarditem
14	image	filer	image
15		cms	page
16	placeholder	cms	placeholder
17	cms plugin	cms	cmsplugin
18	Page moderator state	cms	pagemoderatorstate
19	Page global permission	cms	globalpagepermission
20	Page permission	cms	pagepermission
21	User (page)	cms	pageuser
22	User group (page)	cms	pageusergroup
23	title	cms	title
24	deals collection	web	dealscollection
25	topics collection	web	topicscollection
26	deal	web	deal
27	topic	web	topic
28	deals plugin model	web	dealspluginmodel
29	topic plugin model	web	topicpluginmodel
30	news	web	news
31	news plugin model	web	newspluginmodel
32	cache key	menus	cachekey
33	revision	reversion	revision
34	version	reversion	version
35	source	easy_thumbnails	source
36	thumbnail	easy_thumbnails	thumbnail
37	link	link	link
38	text	text	text
39	flash	flash	flash
40	picture	picture	picture
41	twitter recent entries	twitter	twitterrecententries
42	twitter search	twitter	twittersearch
43	Snippet	snippet	snippet
44	Snippet	snippet	snippetptr
45	google map	googlemap	googlemap
46	contact	cmsplugin_contact	contact
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
x05t4fjrwhshhdk97g2khlxgqp97v35c	ZDlkOGU2NjY0MTc4NjhlMGEyMDU4YzE1N2M0NjBhMTQ3YzJkOGZkYTqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUSX2F1dGhfdXNlcl9iYWNrZW5kcQNVKWRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kcQRVDV9hdXRoX3VzZXJfaWRxBUsBdS4=	2013-10-11 15:41:30.850964+01
c9q8kptssdr5mpb3vda022ecg8wbay1k	NzM2YjkyM2E0YjA0MTU4NzUyYjA3YWRmMDg2N2U5NTE4MmQ4YzY3NzqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUUZmlsZXJfbGFzdF9mb2xkZXJfaWRxA05VEl9hdXRoX3VzZXJfYmFja2VuZHEEVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEFVQ1fYXV0aF91c2VyX2lkcQZLAXUu	2013-10-11 11:39:37.047814+01
utulssog9dnu6ywoeo2fpbv6idoizps6	ODhiNWRhYzgwNmY4NGUwMTBmYjcwYTllNjQ4Y2IyZGZmOThjM2VmNTqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUNX2F1dGhfdXNlcl9pZHEDSwFVEl9hdXRoX3VzZXJfYmFja2VuZHEEVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEFdS4=	2013-10-11 18:13:23.829106+01
7kp45ksl55j86o3q201xl0l7cx0lj9vt	NTk0NTk0MzE4OTE5MDZkN2U2ZTIxYTNhZGVkYzg1NDYwNjQzMTgxZTqAAn1xAShVFGZpbGVyX2xhc3RfZm9sZGVyX2lkcQJOVQ1fYXV0aF91c2VyX2lkcQNLAVUSX2F1dGhfdXNlcl9iYWNrZW5kcQRVKWRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kcQV1Lg==	2013-10-11 16:21:46.400241+01
7rhx7tcfb9m80wg1o6a0wigdpk0cu1fl	YjNlYTY1YThmMzk2ODkyYzBiZGYyM2Q3YTA2NDQxN2JjMzMzMGJmZTqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUNX2F1dGhfdXNlcl9pZHEDSwFVEl9hdXRoX3VzZXJfYmFja2VuZHEEVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEFVRRmaWxlcl9sYXN0X2ZvbGRlcl9pZHEGWAEAAAAydS4=	2013-10-28 00:06:11.083763+01
952d908a4e31a3fab26961afac1fa3f2	ZDlkOGU2NjY0MTc4NjhlMGEyMDU4YzE1N2M0NjBhMTQ3YzJkOGZkYTqAAn1xAShVDmNtc19hZG1p\nbl9zaXRlcQJLAVUSX2F1dGhfdXNlcl9iYWNrZW5kcQNVKWRqYW5nby5jb250cmliLmF1dGguYmFj\na2VuZHMuTW9kZWxCYWNrZW5kcQRVDV9hdXRoX3VzZXJfaWRxBUsBdS4=\n	2013-10-28 04:25:22.647334+01
ruc50ije0hstigmqlpnhf4edddy8hsoq	OWRiMGM5MjBiYjljNTEzNTYwMGZiODlhYjAwYTI2YjNjZDM1NzdiODqAAn1xAVgKAAAAdGVzdGNvb2tpZXECWAYAAAB3b3JrZWRxA3Mu	2013-10-11 09:09:31.493564+01
qw78iln80998iajiikdfnc65d4ra0xkf	ODhiNWRhYzgwNmY4NGUwMTBmYjcwYTllNjQ4Y2IyZGZmOThjM2VmNTqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUNX2F1dGhfdXNlcl9pZHEDSwFVEl9hdXRoX3VzZXJfYmFja2VuZHEEVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEFdS4=	2013-10-11 09:14:39.902975+01
pitzqf6wzgrcasl3oqeqtzaubbip6ziy	ODhiNWRhYzgwNmY4NGUwMTBmYjcwYTllNjQ4Y2IyZGZmOThjM2VmNTqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUNX2F1dGhfdXNlcl9pZHEDSwFVEl9hdXRoX3VzZXJfYmFja2VuZHEEVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEFdS4=	2013-10-12 13:16:28.326512+01
kj7sm4dwvad6s6q1jk1erptqi6yq1cxf	MTFhYzQzYzMwNjEzZDJiNWQwYzM5ZTliMmE2NDliYjA0NWYzOTk0YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAVUUZmlsZXJfbGFzdF9mb2xkZXJfaWRxBU51Lg==	2013-10-11 09:52:48.335234+01
so1ha0rst5e8h4061l9yw4ru8snnswlc	ODhiNWRhYzgwNmY4NGUwMTBmYjcwYTllNjQ4Y2IyZGZmOThjM2VmNTqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUNX2F1dGhfdXNlcl9pZHEDSwFVEl9hdXRoX3VzZXJfYmFja2VuZHEEVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEFdS4=	2013-10-12 15:10:48.999767+01
0frao9s26hpnb0ui288go2pkxsbphtbl	YjNlYTY1YThmMzk2ODkyYzBiZGYyM2Q3YTA2NDQxN2JjMzMzMGJmZTqAAn1xAShVDmNtc19hZG1pbl9zaXRlcQJLAVUNX2F1dGhfdXNlcl9pZHEDSwFVEl9hdXRoX3VzZXJfYmFja2VuZHEEVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEFVRRmaWxlcl9sYXN0X2ZvbGRlcl9pZHEGWAEAAAAydS4=	2013-10-12 21:42:48.661159+01
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: easy_thumbnails_source; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY easy_thumbnails_source (id, name, modified, storage_hash) FROM stdin;
2	/media/public/filer/filer_public/2013/09/27/1234931504682_1.jpg	2013-09-27 08:43:58+01	f9bde26a1556cd667f742bd34ec7c55e
3	filer_public/2013/09/27/promo-sample.jpg	2013-09-27 09:46:17+01	52617e6399d6f38ea9cbf527bf430616
4	/media/public/filer/filer_public/2013/09/27/promo-sample.jpg	2013-09-27 09:46:17+01	f9bde26a1556cd667f742bd34ec7c55e
11	filer_public/2013/09/27/news-campaine2013.jpg	2013-09-27 17:38:30+01	52617e6399d6f38ea9cbf527bf430616
12	filer_public/2013/09/27/news-1993_2013.jpg	2013-09-27 17:38:30+01	52617e6399d6f38ea9cbf527bf430616
13	filer_public/2013/09/27/topicthumb-destination.jpg	2013-09-27 17:39:08+01	52617e6399d6f38ea9cbf527bf430616
14	filer_public/2013/09/27/topicthumb-service.jpg	2013-09-27 17:39:08+01	52617e6399d6f38ea9cbf527bf430616
15	filer_public/2013/09/27/topicthumb-visitgnv.jpg	2013-09-27 17:39:26+01	52617e6399d6f38ea9cbf527bf430616
16	filer_public/2013/09/27/promo-genes_tunis.jpg	2013-09-27 17:39:47+01	52617e6399d6f38ea9cbf527bf430616
17	filer_public/2013/09/27/homebanner-sample_1.jpg	2013-09-27 17:40:16+01	52617e6399d6f38ea9cbf527bf430616
18	/media/public/filer/filer_public/2013/09/27/promo-genes_tunis.jpg	2013-09-27 17:39:47+01	f9bde26a1556cd667f742bd34ec7c55e
19	filer_public/2013/09/27/promo-genes_maroc.jpg	2013-09-27 17:54:43+01	52617e6399d6f38ea9cbf527bf430616
20	filer_public/2013/09/27/promo-genova_maroc.jpg	2013-09-27 17:54:43+01	52617e6399d6f38ea9cbf527bf430616
21	/media/public/filer/filer_public/2013/09/27/promo-genova_maroc.jpg	2013-09-27 17:54:43+01	f9bde26a1556cd667f742bd34ec7c55e
22	/media/public/filer/filer_public/2013/09/27/promo-genes_maroc.jpg	2013-09-27 17:54:43+01	f9bde26a1556cd667f742bd34ec7c55e
23	filer_public/2013/09/27/destinationtopic-genes.jpg	2013-09-27 18:57:59+01	52617e6399d6f38ea9cbf527bf430616
24	filer_public/2013/09/27/destinationtopic-barcelona.jpg	2013-09-27 18:57:59+01	52617e6399d6f38ea9cbf527bf430616
25	filer_public/2013/09/27/destinationtopic-france.jpg	2013-09-27 18:57:59+01	52617e6399d6f38ea9cbf527bf430616
26	filer_public/2013/09/27/destinationtopic-maroc.jpg	2013-09-27 18:58:00+01	52617e6399d6f38ea9cbf527bf430616
27	filer_public/2013/09/27/destinationtopic-naples.jpg	2013-09-27 18:58:00+01	52617e6399d6f38ea9cbf527bf430616
28	filer_public/2013/09/27/destinationtopic-nador.jpg	2013-09-27 18:58:00+01	52617e6399d6f38ea9cbf527bf430616
29	filer_public/2013/09/27/destinationtopic-palerme.jpg	2013-09-27 18:58:00+01	52617e6399d6f38ea9cbf527bf430616
30	filer_public/2013/09/27/destinationtopic-rome.jpg	2013-09-27 18:58:01+01	52617e6399d6f38ea9cbf527bf430616
31	filer_public/2013/09/27/destinationtopic-sardaigne.jpg	2013-09-27 18:58:01+01	52617e6399d6f38ea9cbf527bf430616
32	filer_public/2013/09/27/destinationtopic-sicile.jpg	2013-09-27 18:58:01+01	52617e6399d6f38ea9cbf527bf430616
33	filer_public/2013/09/27/destinationtopic-tunis.jpg	2013-09-27 18:58:01+01	52617e6399d6f38ea9cbf527bf430616
34	filer_public/2013/09/27/servicestopics-fidelitycard.jpg	2013-09-27 19:21:12+01	52617e6399d6f38ea9cbf527bf430616
35	filer_public/2013/09/27/servicestopics-arrival.jpg	2013-09-27 19:21:12+01	52617e6399d6f38ea9cbf527bf430616
36	filer_public/2013/09/27/servicestopics-beforego.jpg	2013-09-27 19:21:12+01	52617e6399d6f38ea9cbf527bf430616
37	filer_public/2013/09/27/servicestopics-quality.jpg	2013-09-27 19:21:13+01	52617e6399d6f38ea9cbf527bf430616
38	filer_public/2013/09/27/servicestopics-onboard.jpg	2013-09-27 19:21:13+01	52617e6399d6f38ea9cbf527bf430616
39	filer_public/2013/09/27/servicestopics-guide.jpg	2013-09-27 19:21:13+01	52617e6399d6f38ea9cbf527bf430616
40	filer_public/2013/09/27/servicestopics-reunion.jpg	2013-09-27 19:21:14+01	52617e6399d6f38ea9cbf527bf430616
41	/media/public/filer/filer_public/2013/09/27/destinationtopic-naples.jpg	2013-09-27 18:58:00+01	f9bde26a1556cd667f742bd34ec7c55e
42	/media/public/filer/filer_public/2013/09/27/destinationtopic-palerme.jpg	2013-09-27 18:58:00+01	f9bde26a1556cd667f742bd34ec7c55e
43	/media/public/filer/filer_public/2013/09/27/destinationtopic-sicile.jpg	2013-09-27 18:58:01+01	f9bde26a1556cd667f742bd34ec7c55e
44	/media/public/filer/filer_public/2013/09/27/destinationtopic-sardaigne.jpg	2013-09-27 18:58:01+01	f9bde26a1556cd667f742bd34ec7c55e
45	/media/public/filer/filer_public/2013/09/27/destinationtopic-genes.jpg	2013-09-27 18:57:59+01	f9bde26a1556cd667f742bd34ec7c55e
46	/media/public/filer/filer_public/2013/09/27/destinationtopic-rome.jpg	2013-09-27 18:58:01+01	f9bde26a1556cd667f742bd34ec7c55e
47	/media/public/filer/filer_public/2013/09/27/destinationtopic-barcelona.jpg	2013-09-27 18:57:59+01	f9bde26a1556cd667f742bd34ec7c55e
48	/media/public/filer/filer_public/2013/09/27/destinationtopic-tunis.jpg	2013-09-27 18:58:01+01	f9bde26a1556cd667f742bd34ec7c55e
49	/media/public/filer/filer_public/2013/09/27/destinationtopic-maroc.jpg	2013-09-27 18:58:00+01	f9bde26a1556cd667f742bd34ec7c55e
50	/media/public/filer/filer_public/2013/09/27/destinationtopic-nador.jpg	2013-09-27 18:58:00+01	f9bde26a1556cd667f742bd34ec7c55e
51	/media/public/filer/filer_public/2013/09/27/destinationtopic-france.jpg	2013-09-27 18:57:59+01	f9bde26a1556cd667f742bd34ec7c55e
52	/media/public/filer/filer_public/2013/09/27/topicthumb-visitgnv.jpg	2013-09-27 17:39:26+01	f9bde26a1556cd667f742bd34ec7c55e
53	/media/public/filer/filer_public/2013/09/27/topicthumb-destination.jpg	2013-09-27 17:39:08+01	f9bde26a1556cd667f742bd34ec7c55e
54	/media/public/filer/filer_public/2013/09/27/topicthumb-service.jpg	2013-09-27 17:39:08+01	f9bde26a1556cd667f742bd34ec7c55e
55	/media/public/filer/filer_public/2013/09/27/servicestopics-beforego.jpg	2013-09-27 19:21:12+01	f9bde26a1556cd667f742bd34ec7c55e
56	/media/public/filer/filer_public/2013/09/27/servicestopics-onboard.jpg	2013-09-27 19:21:13+01	f9bde26a1556cd667f742bd34ec7c55e
57	/media/public/filer/filer_public/2013/09/27/servicestopics-guide.jpg	2013-09-27 19:21:13+01	f9bde26a1556cd667f742bd34ec7c55e
58	/media/public/filer/filer_public/2013/09/27/servicestopics-fidelitycard.jpg	2013-09-27 19:21:12+01	f9bde26a1556cd667f742bd34ec7c55e
59	/media/public/filer/filer_public/2013/09/27/servicestopics-quality.jpg	2013-09-27 19:21:13+01	f9bde26a1556cd667f742bd34ec7c55e
60	/media/public/filer/filer_public/2013/09/27/servicestopics-arrival.jpg	2013-09-27 19:21:12+01	f9bde26a1556cd667f742bd34ec7c55e
61	/media/public/filer/filer_public/2013/09/27/servicestopics-reunion.jpg	2013-09-27 19:21:14+01	f9bde26a1556cd667f742bd34ec7c55e
62	filer_public/2013/09/30/campaign2013-reception.jpg	2013-09-30 10:26:25+01	52617e6399d6f38ea9cbf527bf430616
63	filer_public/2013/09/30/campaign2013-commandant.jpg	2013-09-30 10:26:25+01	52617e6399d6f38ea9cbf527bf430616
64	filer_public/2013/09/30/campaign-banner.jpg	2013-09-30 10:37:41+01	52617e6399d6f38ea9cbf527bf430616
65	filer_public/2013/09/30/destinations-banner.jpg	2013-09-30 10:38:02+01	52617e6399d6f38ea9cbf527bf430616
66	filer_public/2013/09/30/visitgnv-banner.jpg	2013-09-30 10:38:03+01	52617e6399d6f38ea9cbf527bf430616
67	filer_public/2013/09/30/homear-banner.jpg	2013-09-30 10:38:03+01	52617e6399d6f38ea9cbf527bf430616
68	filer_public/2013/09/30/contact-banner.jpg	2013-09-30 10:52:23+01	52617e6399d6f38ea9cbf527bf430616
70	filer_public/2013/09/30/home-banner.jpg	2013-09-30 10:53:32+01	52617e6399d6f38ea9cbf527bf430616
71	filer_public/2013/09/30/services-banner.jpg	2013-09-30 10:55:47+01	52617e6399d6f38ea9cbf527bf430616
72	filer_public/2013/09/30/slide3.jpg	2013-09-30 10:58:16+01	52617e6399d6f38ea9cbf527bf430616
73	filer_public/2013/09/30/slide2.jpg	2013-09-30 10:58:16+01	52617e6399d6f38ea9cbf527bf430616
74	filer_public/2013/09/30/slide1.jpg	2013-09-30 10:58:16+01	52617e6399d6f38ea9cbf527bf430616
75	filer_public/2013/09/30/slide1_1.jpg	2013-09-30 12:05:14+01	52617e6399d6f38ea9cbf527bf430616
76	filer_public/2013/09/30/slide3_1.jpg	2013-09-30 12:05:14+01	52617e6399d6f38ea9cbf527bf430616
77	filer_public/2013/09/30/slide2_1.jpg	2013-09-30 12:05:14+01	52617e6399d6f38ea9cbf527bf430616
78	filer_public/2013/09/30/slider1.jpg	2013-09-30 12:54:37+01	52617e6399d6f38ea9cbf527bf430616
79	filer_public/2013/09/30/slider2.jpg	2013-09-30 12:54:37+01	52617e6399d6f38ea9cbf527bf430616
80	filer_public/2013/09/30/slider3.jpg	2013-09-30 12:54:37+01	52617e6399d6f38ea9cbf527bf430616
81	filer_public/2013/09/30/slider2_1.jpg	2013-09-30 12:54:55+01	52617e6399d6f38ea9cbf527bf430616
82	filer_public/2013/09/30/slider3_1.jpg	2013-09-30 12:54:55+01	52617e6399d6f38ea9cbf527bf430616
83	filer_public/2013/09/30/slider1_1.jpg	2013-09-30 12:54:55+01	52617e6399d6f38ea9cbf527bf430616
84	filer_public/2013/10/01/campaign2013-commandant.jpg	2013-10-01 15:24:52+01	52617e6399d6f38ea9cbf527bf430616
85	filer_public/2013/10/01/campaign2013-reception.jpg	2013-10-01 15:25:14+01	52617e6399d6f38ea9cbf527bf430616
86	filer_public/2013/10/01/campaign2013-sailor.jpg	2013-10-01 15:25:40+01	52617e6399d6f38ea9cbf527bf430616
87	filer_public/2013/10/01/campaign2013-service.jpg	2013-10-01 15:25:40+01	52617e6399d6f38ea9cbf527bf430616
88	filer_public/2013/10/01/tv-ads-2013.mp4	2013-10-01 15:34:26+01	52617e6399d6f38ea9cbf527bf430616
89	filer_public/2013/10/01/tv_ads_2013.ogv	2013-10-01 15:45:26+01	52617e6399d6f38ea9cbf527bf430616
90	filer_public/2013/10/01/tv_ads_2013.webm	2013-10-01 15:45:30+01	52617e6399d6f38ea9cbf527bf430616
91	filer_public/2013/10/13/home-banner.jpg	2013-10-13 22:29:03+01	52617e6399d6f38ea9cbf527bf430616
92	filer_public/2013/10/13/home-banner-fr.jpg	2013-10-13 22:32:33+01	52617e6399d6f38ea9cbf527bf430616
93	filer_public/2013/10/13/homefr-banner.jpg	2013-10-13 22:33:54+01	52617e6399d6f38ea9cbf527bf430616
94	filer_public/2013/10/13/fidelitycard.png	2013-10-13 22:46:25+01	52617e6399d6f38ea9cbf527bf430616
95	filer_public/2013/10/13/fidelitycard_1.png	2013-10-13 22:52:55+01	52617e6399d6f38ea9cbf527bf430616
\.


--
-- Data for Name: easy_thumbnails_thumbnail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY easy_thumbnails_thumbnail (id, name, modified, source_id, storage_hash) FROM stdin;
6	/media/public/filer/filer_public/2013/09/27/1234931504682_1.jpg.300x300_q85_crop.jpg	2013-09-27 08:48:24+01	2	d26becbf46ac48eda79c7a39a13a02dd
7	filer_public_thumbnails/filer_public/2013/09/27/promo-sample.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 09:46:17+01	3	52617e6399d6f38ea9cbf527bf430616
8	filer_public_thumbnails/filer_public/2013/09/27/promo-sample.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 09:46:17+01	3	52617e6399d6f38ea9cbf527bf430616
9	filer_public_thumbnails/filer_public/2013/09/27/promo-sample.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 09:46:17+01	3	52617e6399d6f38ea9cbf527bf430616
10	filer_public_thumbnails/filer_public/2013/09/27/promo-sample.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 09:46:17+01	3	52617e6399d6f38ea9cbf527bf430616
11	/media/public/filer/filer_public/2013/09/27/promo-sample.jpg.300x300_q85_crop.jpg	2013-09-27 09:53:00+01	4	d26becbf46ac48eda79c7a39a13a02dd
12	filer_public_thumbnails/filer_public/2013/09/27/promo-sample.jpg__300x300_q85_crop.jpg	2013-09-27 10:59:12+01	3	52617e6399d6f38ea9cbf527bf430616
39	filer_public_thumbnails/filer_public/2013/09/27/news-campaine2013.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	11	52617e6399d6f38ea9cbf527bf430616
40	filer_public_thumbnails/filer_public/2013/09/27/news-campaine2013.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	11	52617e6399d6f38ea9cbf527bf430616
41	filer_public_thumbnails/filer_public/2013/09/27/news-1993_2013.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	12	52617e6399d6f38ea9cbf527bf430616
42	filer_public_thumbnails/filer_public/2013/09/27/news-campaine2013.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	11	52617e6399d6f38ea9cbf527bf430616
43	filer_public_thumbnails/filer_public/2013/09/27/news-1993_2013.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	12	52617e6399d6f38ea9cbf527bf430616
44	filer_public_thumbnails/filer_public/2013/09/27/news-campaine2013.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	11	52617e6399d6f38ea9cbf527bf430616
45	filer_public_thumbnails/filer_public/2013/09/27/news-1993_2013.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	12	52617e6399d6f38ea9cbf527bf430616
46	filer_public_thumbnails/filer_public/2013/09/27/news-1993_2013.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:38:30+01	12	52617e6399d6f38ea9cbf527bf430616
47	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-destination.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	13	52617e6399d6f38ea9cbf527bf430616
48	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-destination.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	13	52617e6399d6f38ea9cbf527bf430616
49	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-destination.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	13	52617e6399d6f38ea9cbf527bf430616
50	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-destination.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	13	52617e6399d6f38ea9cbf527bf430616
51	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-service.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	14	52617e6399d6f38ea9cbf527bf430616
52	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-service.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	14	52617e6399d6f38ea9cbf527bf430616
53	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-service.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	14	52617e6399d6f38ea9cbf527bf430616
54	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-service.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:39:08+01	14	52617e6399d6f38ea9cbf527bf430616
55	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-visitgnv.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:39:27+01	15	52617e6399d6f38ea9cbf527bf430616
56	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-visitgnv.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:39:27+01	15	52617e6399d6f38ea9cbf527bf430616
57	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-visitgnv.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:39:27+01	15	52617e6399d6f38ea9cbf527bf430616
58	filer_public_thumbnails/filer_public/2013/09/27/topicthumb-visitgnv.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:39:27+01	15	52617e6399d6f38ea9cbf527bf430616
59	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_tunis.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:39:47+01	16	52617e6399d6f38ea9cbf527bf430616
60	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_tunis.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:39:47+01	16	52617e6399d6f38ea9cbf527bf430616
61	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_tunis.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:39:47+01	16	52617e6399d6f38ea9cbf527bf430616
62	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_tunis.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:39:47+01	16	52617e6399d6f38ea9cbf527bf430616
63	filer_public_thumbnails/filer_public/2013/09/27/homebanner-sample_1.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:40:16+01	17	52617e6399d6f38ea9cbf527bf430616
64	filer_public_thumbnails/filer_public/2013/09/27/homebanner-sample_1.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:40:16+01	17	52617e6399d6f38ea9cbf527bf430616
65	filer_public_thumbnails/filer_public/2013/09/27/homebanner-sample_1.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:40:16+01	17	52617e6399d6f38ea9cbf527bf430616
66	filer_public_thumbnails/filer_public/2013/09/27/homebanner-sample_1.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:40:16+01	17	52617e6399d6f38ea9cbf527bf430616
67	filer_public_thumbnails/filer_public/2013/09/27/homebanner-sample_1.jpg__210x10000_q85.jpg	2013-09-27 17:40:20+01	17	52617e6399d6f38ea9cbf527bf430616
68	filer_public_thumbnails/filer_public/2013/09/27/news-campaine2013.jpg__210x10000_q85.jpg	2013-09-27 17:42:20+01	11	52617e6399d6f38ea9cbf527bf430616
69	filer_public_thumbnails/filer_public/2013/09/27/news-1993_2013.jpg__300x300_q85_crop.jpg	2013-09-27 17:42:59+01	12	52617e6399d6f38ea9cbf527bf430616
70	filer_public_thumbnails/filer_public/2013/09/27/news-campaine2013.jpg__300x300_q85_crop.jpg	2013-09-27 17:42:59+01	11	52617e6399d6f38ea9cbf527bf430616
71	/media/public/filer/filer_public/2013/09/27/promo-genes_tunis.jpg.300x300_q85_crop.jpg	2013-09-27 17:50:34+01	18	d26becbf46ac48eda79c7a39a13a02dd
72	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_maroc.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	19	52617e6399d6f38ea9cbf527bf430616
73	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_maroc.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	19	52617e6399d6f38ea9cbf527bf430616
74	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_maroc.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	19	52617e6399d6f38ea9cbf527bf430616
75	filer_public_thumbnails/filer_public/2013/09/27/promo-genes_maroc.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	19	52617e6399d6f38ea9cbf527bf430616
76	filer_public_thumbnails/filer_public/2013/09/27/promo-genova_maroc.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	20	52617e6399d6f38ea9cbf527bf430616
77	filer_public_thumbnails/filer_public/2013/09/27/promo-genova_maroc.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	20	52617e6399d6f38ea9cbf527bf430616
78	filer_public_thumbnails/filer_public/2013/09/27/promo-genova_maroc.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	20	52617e6399d6f38ea9cbf527bf430616
79	filer_public_thumbnails/filer_public/2013/09/27/promo-genova_maroc.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 17:54:43+01	20	52617e6399d6f38ea9cbf527bf430616
80	/media/public/filer/filer_public/2013/09/27/promo-genova_maroc.jpg.300x300_q85_crop.jpg	2013-09-27 17:56:49+01	21	d26becbf46ac48eda79c7a39a13a02dd
81	/media/public/filer/filer_public/2013/09/27/promo-genes_maroc.jpg.300x300_q85_crop.jpg	2013-09-27 17:56:49+01	22	d26becbf46ac48eda79c7a39a13a02dd
82	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-genes.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	23	52617e6399d6f38ea9cbf527bf430616
83	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-france.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	25	52617e6399d6f38ea9cbf527bf430616
84	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-barcelona.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	24	52617e6399d6f38ea9cbf527bf430616
85	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-genes.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	23	52617e6399d6f38ea9cbf527bf430616
86	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-france.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	25	52617e6399d6f38ea9cbf527bf430616
87	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-barcelona.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	24	52617e6399d6f38ea9cbf527bf430616
88	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-genes.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	23	52617e6399d6f38ea9cbf527bf430616
89	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-france.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	25	52617e6399d6f38ea9cbf527bf430616
90	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-barcelona.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	24	52617e6399d6f38ea9cbf527bf430616
91	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-genes.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	23	52617e6399d6f38ea9cbf527bf430616
92	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-france.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	25	52617e6399d6f38ea9cbf527bf430616
93	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-barcelona.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:57:59+01	24	52617e6399d6f38ea9cbf527bf430616
94	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-maroc.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	26	52617e6399d6f38ea9cbf527bf430616
95	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-maroc.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	26	52617e6399d6f38ea9cbf527bf430616
96	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-maroc.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	26	52617e6399d6f38ea9cbf527bf430616
97	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-maroc.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	26	52617e6399d6f38ea9cbf527bf430616
98	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-naples.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	27	52617e6399d6f38ea9cbf527bf430616
99	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-naples.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	27	52617e6399d6f38ea9cbf527bf430616
100	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-nador.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	28	52617e6399d6f38ea9cbf527bf430616
101	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-naples.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	27	52617e6399d6f38ea9cbf527bf430616
102	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-nador.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	28	52617e6399d6f38ea9cbf527bf430616
103	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-naples.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	27	52617e6399d6f38ea9cbf527bf430616
104	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-nador.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	28	52617e6399d6f38ea9cbf527bf430616
105	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-nador.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	28	52617e6399d6f38ea9cbf527bf430616
106	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-palerme.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	29	52617e6399d6f38ea9cbf527bf430616
107	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-palerme.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:00+01	29	52617e6399d6f38ea9cbf527bf430616
108	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-palerme.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	29	52617e6399d6f38ea9cbf527bf430616
109	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-palerme.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	29	52617e6399d6f38ea9cbf527bf430616
110	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-rome.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	30	52617e6399d6f38ea9cbf527bf430616
111	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-rome.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	30	52617e6399d6f38ea9cbf527bf430616
112	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-rome.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	30	52617e6399d6f38ea9cbf527bf430616
113	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-rome.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	30	52617e6399d6f38ea9cbf527bf430616
114	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sardaigne.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	31	52617e6399d6f38ea9cbf527bf430616
115	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sardaigne.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	31	52617e6399d6f38ea9cbf527bf430616
116	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sardaigne.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	31	52617e6399d6f38ea9cbf527bf430616
117	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sardaigne.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	31	52617e6399d6f38ea9cbf527bf430616
118	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sicile.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	32	52617e6399d6f38ea9cbf527bf430616
119	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sicile.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	32	52617e6399d6f38ea9cbf527bf430616
120	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sicile.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	32	52617e6399d6f38ea9cbf527bf430616
121	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-sicile.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	32	52617e6399d6f38ea9cbf527bf430616
122	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-tunis.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	33	52617e6399d6f38ea9cbf527bf430616
123	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-tunis.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	33	52617e6399d6f38ea9cbf527bf430616
124	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-tunis.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	33	52617e6399d6f38ea9cbf527bf430616
125	filer_public_thumbnails/filer_public/2013/09/27/destinationtopic-tunis.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 18:58:01+01	33	52617e6399d6f38ea9cbf527bf430616
126	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-fidelitycard.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	34	52617e6399d6f38ea9cbf527bf430616
127	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-fidelitycard.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	34	52617e6399d6f38ea9cbf527bf430616
128	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-arrival.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	35	52617e6399d6f38ea9cbf527bf430616
129	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-fidelitycard.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	34	52617e6399d6f38ea9cbf527bf430616
130	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-arrival.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	35	52617e6399d6f38ea9cbf527bf430616
131	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-fidelitycard.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	34	52617e6399d6f38ea9cbf527bf430616
132	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-arrival.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	35	52617e6399d6f38ea9cbf527bf430616
133	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-arrival.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	35	52617e6399d6f38ea9cbf527bf430616
134	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-beforego.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	36	52617e6399d6f38ea9cbf527bf430616
135	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-beforego.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	36	52617e6399d6f38ea9cbf527bf430616
136	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-beforego.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	36	52617e6399d6f38ea9cbf527bf430616
137	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-beforego.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 19:21:12+01	36	52617e6399d6f38ea9cbf527bf430616
138	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-quality.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	37	52617e6399d6f38ea9cbf527bf430616
139	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-quality.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	37	52617e6399d6f38ea9cbf527bf430616
140	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-quality.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	37	52617e6399d6f38ea9cbf527bf430616
141	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-onboard.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	38	52617e6399d6f38ea9cbf527bf430616
142	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-quality.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	37	52617e6399d6f38ea9cbf527bf430616
143	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-guide.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	39	52617e6399d6f38ea9cbf527bf430616
144	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-onboard.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	38	52617e6399d6f38ea9cbf527bf430616
145	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-guide.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	39	52617e6399d6f38ea9cbf527bf430616
146	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-onboard.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	38	52617e6399d6f38ea9cbf527bf430616
147	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-guide.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	39	52617e6399d6f38ea9cbf527bf430616
148	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-onboard.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	38	52617e6399d6f38ea9cbf527bf430616
149	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-guide.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 19:21:13+01	39	52617e6399d6f38ea9cbf527bf430616
150	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-reunion.jpg__32x32_q85_crop_upscale.jpg	2013-09-27 19:21:14+01	40	52617e6399d6f38ea9cbf527bf430616
151	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-reunion.jpg__64x64_q85_crop_upscale.jpg	2013-09-27 19:21:14+01	40	52617e6399d6f38ea9cbf527bf430616
152	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-reunion.jpg__48x48_q85_crop_upscale.jpg	2013-09-27 19:21:14+01	40	52617e6399d6f38ea9cbf527bf430616
153	filer_public_thumbnails/filer_public/2013/09/27/servicestopics-reunion.jpg__16x16_q85_crop_upscale.jpg	2013-09-27 19:21:14+01	40	52617e6399d6f38ea9cbf527bf430616
154	/media/public/filer/filer_public/2013/09/27/destinationtopic-naples.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	41	d26becbf46ac48eda79c7a39a13a02dd
155	/media/public/filer/filer_public/2013/09/27/destinationtopic-palerme.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	42	d26becbf46ac48eda79c7a39a13a02dd
156	/media/public/filer/filer_public/2013/09/27/destinationtopic-sicile.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	43	d26becbf46ac48eda79c7a39a13a02dd
157	/media/public/filer/filer_public/2013/09/27/destinationtopic-sardaigne.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	44	d26becbf46ac48eda79c7a39a13a02dd
158	/media/public/filer/filer_public/2013/09/27/destinationtopic-genes.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	45	d26becbf46ac48eda79c7a39a13a02dd
159	/media/public/filer/filer_public/2013/09/27/destinationtopic-rome.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	46	d26becbf46ac48eda79c7a39a13a02dd
160	/media/public/filer/filer_public/2013/09/27/destinationtopic-barcelona.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	47	d26becbf46ac48eda79c7a39a13a02dd
161	/media/public/filer/filer_public/2013/09/27/destinationtopic-tunis.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	48	d26becbf46ac48eda79c7a39a13a02dd
162	/media/public/filer/filer_public/2013/09/27/destinationtopic-maroc.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	49	d26becbf46ac48eda79c7a39a13a02dd
163	/media/public/filer/filer_public/2013/09/27/destinationtopic-nador.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	50	d26becbf46ac48eda79c7a39a13a02dd
164	/media/public/filer/filer_public/2013/09/27/destinationtopic-france.jpg.300x300_q85_crop.jpg	2013-09-30 09:47:52+01	51	d26becbf46ac48eda79c7a39a13a02dd
165	/media/public/filer/filer_public/2013/09/27/topicthumb-visitgnv.jpg.300x300_q85_crop.jpg	2013-09-30 09:49:40+01	52	d26becbf46ac48eda79c7a39a13a02dd
166	/media/public/filer/filer_public/2013/09/27/topicthumb-destination.jpg.300x300_q85_crop.jpg	2013-09-30 09:49:41+01	53	d26becbf46ac48eda79c7a39a13a02dd
167	/media/public/filer/filer_public/2013/09/27/topicthumb-service.jpg.300x300_q85_crop.jpg	2013-09-30 09:49:41+01	54	d26becbf46ac48eda79c7a39a13a02dd
168	/media/public/filer/filer_public/2013/09/27/servicestopics-beforego.jpg.300x300_q85_crop.jpg	2013-09-30 10:01:24+01	55	d26becbf46ac48eda79c7a39a13a02dd
169	/media/public/filer/filer_public/2013/09/27/servicestopics-onboard.jpg.300x300_q85_crop.jpg	2013-09-30 10:01:24+01	56	d26becbf46ac48eda79c7a39a13a02dd
170	/media/public/filer/filer_public/2013/09/27/servicestopics-guide.jpg.300x300_q85_crop.jpg	2013-09-30 10:01:24+01	57	d26becbf46ac48eda79c7a39a13a02dd
171	/media/public/filer/filer_public/2013/09/27/servicestopics-fidelitycard.jpg.300x300_q85_crop.jpg	2013-09-30 10:01:24+01	58	d26becbf46ac48eda79c7a39a13a02dd
172	/media/public/filer/filer_public/2013/09/27/servicestopics-quality.jpg.300x300_q85_crop.jpg	2013-09-30 10:01:24+01	59	d26becbf46ac48eda79c7a39a13a02dd
173	/media/public/filer/filer_public/2013/09/27/servicestopics-arrival.jpg.300x300_q85_crop.jpg	2013-09-30 10:01:24+01	60	d26becbf46ac48eda79c7a39a13a02dd
174	/media/public/filer/filer_public/2013/09/27/servicestopics-reunion.jpg.300x300_q85_crop.jpg	2013-09-30 10:01:24+01	61	d26becbf46ac48eda79c7a39a13a02dd
175	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-reception.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	62	52617e6399d6f38ea9cbf527bf430616
176	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-commandant.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	63	52617e6399d6f38ea9cbf527bf430616
177	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-reception.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	62	52617e6399d6f38ea9cbf527bf430616
178	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-commandant.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	63	52617e6399d6f38ea9cbf527bf430616
179	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-reception.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	62	52617e6399d6f38ea9cbf527bf430616
180	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-commandant.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	63	52617e6399d6f38ea9cbf527bf430616
182	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-commandant.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	63	52617e6399d6f38ea9cbf527bf430616
181	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-reception.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:26:25+01	62	52617e6399d6f38ea9cbf527bf430616
183	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-commandant.jpg__210x10000_q85.jpg	2013-09-30 10:29:22+01	63	52617e6399d6f38ea9cbf527bf430616
184	filer_public_thumbnails/filer_public/2013/09/30/campaign2013-reception.jpg__210x10000_q85.jpg	2013-09-30 10:30:06+01	62	52617e6399d6f38ea9cbf527bf430616
185	filer_public_thumbnails/filer_public/2013/09/30/campaign-banner.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:37:41+01	64	52617e6399d6f38ea9cbf527bf430616
186	filer_public_thumbnails/filer_public/2013/09/30/campaign-banner.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:37:41+01	64	52617e6399d6f38ea9cbf527bf430616
187	filer_public_thumbnails/filer_public/2013/09/30/campaign-banner.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:37:41+01	64	52617e6399d6f38ea9cbf527bf430616
188	filer_public_thumbnails/filer_public/2013/09/30/campaign-banner.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:37:41+01	64	52617e6399d6f38ea9cbf527bf430616
189	filer_public_thumbnails/filer_public/2013/09/30/destinations-banner.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:38:02+01	65	52617e6399d6f38ea9cbf527bf430616
190	filer_public_thumbnails/filer_public/2013/09/30/destinations-banner.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:38:02+01	65	52617e6399d6f38ea9cbf527bf430616
191	filer_public_thumbnails/filer_public/2013/09/30/destinations-banner.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:38:02+01	65	52617e6399d6f38ea9cbf527bf430616
192	filer_public_thumbnails/filer_public/2013/09/30/destinations-banner.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:38:02+01	65	52617e6399d6f38ea9cbf527bf430616
193	filer_public_thumbnails/filer_public/2013/09/30/visitgnv-banner.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	66	52617e6399d6f38ea9cbf527bf430616
194	filer_public_thumbnails/filer_public/2013/09/30/visitgnv-banner.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	66	52617e6399d6f38ea9cbf527bf430616
195	filer_public_thumbnails/filer_public/2013/09/30/visitgnv-banner.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	66	52617e6399d6f38ea9cbf527bf430616
196	filer_public_thumbnails/filer_public/2013/09/30/visitgnv-banner.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	66	52617e6399d6f38ea9cbf527bf430616
197	filer_public_thumbnails/filer_public/2013/09/30/homear-banner.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	67	52617e6399d6f38ea9cbf527bf430616
198	filer_public_thumbnails/filer_public/2013/09/30/homear-banner.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	67	52617e6399d6f38ea9cbf527bf430616
199	filer_public_thumbnails/filer_public/2013/09/30/homear-banner.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	67	52617e6399d6f38ea9cbf527bf430616
200	filer_public_thumbnails/filer_public/2013/09/30/homear-banner.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:38:03+01	67	52617e6399d6f38ea9cbf527bf430616
201	filer_public_thumbnails/filer_public/2013/09/30/contact-banner.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:52:23+01	68	52617e6399d6f38ea9cbf527bf430616
202	filer_public_thumbnails/filer_public/2013/09/30/contact-banner.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:52:23+01	68	52617e6399d6f38ea9cbf527bf430616
203	filer_public_thumbnails/filer_public/2013/09/30/contact-banner.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:52:24+01	68	52617e6399d6f38ea9cbf527bf430616
204	filer_public_thumbnails/filer_public/2013/09/30/contact-banner.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:52:24+01	68	52617e6399d6f38ea9cbf527bf430616
209	filer_public_thumbnails/filer_public/2013/09/30/home-banner.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:53:32+01	70	52617e6399d6f38ea9cbf527bf430616
210	filer_public_thumbnails/filer_public/2013/09/30/home-banner.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:53:32+01	70	52617e6399d6f38ea9cbf527bf430616
211	filer_public_thumbnails/filer_public/2013/09/30/home-banner.jpg__210x10000_q85.jpg	2013-09-30 10:53:32+01	70	52617e6399d6f38ea9cbf527bf430616
212	filer_public_thumbnails/filer_public/2013/09/30/home-banner.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:53:37+01	70	52617e6399d6f38ea9cbf527bf430616
213	filer_public_thumbnails/filer_public/2013/09/30/home-banner.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:53:37+01	70	52617e6399d6f38ea9cbf527bf430616
214	filer_public_thumbnails/filer_public/2013/09/30/homear-banner.jpg__210x10000_q85.jpg	2013-09-30 10:54:20+01	67	52617e6399d6f38ea9cbf527bf430616
215	filer_public_thumbnails/filer_public/2013/09/30/services-banner.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:55:47+01	71	52617e6399d6f38ea9cbf527bf430616
216	filer_public_thumbnails/filer_public/2013/09/30/services-banner.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:55:47+01	71	52617e6399d6f38ea9cbf527bf430616
217	filer_public_thumbnails/filer_public/2013/09/30/services-banner.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:55:47+01	71	52617e6399d6f38ea9cbf527bf430616
218	filer_public_thumbnails/filer_public/2013/09/30/services-banner.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:55:47+01	71	52617e6399d6f38ea9cbf527bf430616
219	filer_public_thumbnails/filer_public/2013/09/30/slide3.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	72	52617e6399d6f38ea9cbf527bf430616
220	filer_public_thumbnails/filer_public/2013/09/30/slide3.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	72	52617e6399d6f38ea9cbf527bf430616
221	filer_public_thumbnails/filer_public/2013/09/30/slide2.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	73	52617e6399d6f38ea9cbf527bf430616
222	filer_public_thumbnails/filer_public/2013/09/30/slide3.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	72	52617e6399d6f38ea9cbf527bf430616
223	filer_public_thumbnails/filer_public/2013/09/30/slide2.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	73	52617e6399d6f38ea9cbf527bf430616
224	filer_public_thumbnails/filer_public/2013/09/30/slide3.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	72	52617e6399d6f38ea9cbf527bf430616
225	filer_public_thumbnails/filer_public/2013/09/30/slide2.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	73	52617e6399d6f38ea9cbf527bf430616
226	filer_public_thumbnails/filer_public/2013/09/30/slide2.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	73	52617e6399d6f38ea9cbf527bf430616
227	filer_public_thumbnails/filer_public/2013/09/30/slide1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	74	52617e6399d6f38ea9cbf527bf430616
228	filer_public_thumbnails/filer_public/2013/09/30/slide1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 10:58:16+01	74	52617e6399d6f38ea9cbf527bf430616
229	filer_public_thumbnails/filer_public/2013/09/30/slide1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 10:58:17+01	74	52617e6399d6f38ea9cbf527bf430616
230	filer_public_thumbnails/filer_public/2013/09/30/slide1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 10:58:17+01	74	52617e6399d6f38ea9cbf527bf430616
231	filer_public_thumbnails/filer_public/2013/09/30/slide1.jpg__210x10000_q85.jpg	2013-09-30 10:58:21+01	74	52617e6399d6f38ea9cbf527bf430616
232	filer_public_thumbnails/filer_public/2013/09/30/slide1_1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:05:14+01	75	52617e6399d6f38ea9cbf527bf430616
233	filer_public_thumbnails/filer_public/2013/09/30/slide1_1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:05:14+01	75	52617e6399d6f38ea9cbf527bf430616
234	filer_public_thumbnails/filer_public/2013/09/30/slide1_1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:05:14+01	75	52617e6399d6f38ea9cbf527bf430616
235	filer_public_thumbnails/filer_public/2013/09/30/slide3_1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:05:14+01	76	52617e6399d6f38ea9cbf527bf430616
236	filer_public_thumbnails/filer_public/2013/09/30/slide1_1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:05:14+01	75	52617e6399d6f38ea9cbf527bf430616
237	filer_public_thumbnails/filer_public/2013/09/30/slide3_1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:05:14+01	76	52617e6399d6f38ea9cbf527bf430616
238	filer_public_thumbnails/filer_public/2013/09/30/slide2_1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:05:14+01	77	52617e6399d6f38ea9cbf527bf430616
239	filer_public_thumbnails/filer_public/2013/09/30/slide3_1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:05:15+01	76	52617e6399d6f38ea9cbf527bf430616
240	filer_public_thumbnails/filer_public/2013/09/30/slide2_1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:05:15+01	77	52617e6399d6f38ea9cbf527bf430616
241	filer_public_thumbnails/filer_public/2013/09/30/slide3_1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:05:15+01	76	52617e6399d6f38ea9cbf527bf430616
242	filer_public_thumbnails/filer_public/2013/09/30/slide2_1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:05:15+01	77	52617e6399d6f38ea9cbf527bf430616
243	filer_public_thumbnails/filer_public/2013/09/30/slide2_1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:05:15+01	77	52617e6399d6f38ea9cbf527bf430616
244	filer_public_thumbnails/filer_public/2013/09/30/slide1_1.jpg__210x10000_q85.jpg	2013-09-30 12:05:37+01	75	52617e6399d6f38ea9cbf527bf430616
245	filer_public_thumbnails/filer_public/2013/09/30/slider1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:54:37+01	78	52617e6399d6f38ea9cbf527bf430616
246	filer_public_thumbnails/filer_public/2013/09/30/slider1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:54:37+01	78	52617e6399d6f38ea9cbf527bf430616
247	filer_public_thumbnails/filer_public/2013/09/30/slider1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:54:37+01	78	52617e6399d6f38ea9cbf527bf430616
248	filer_public_thumbnails/filer_public/2013/09/30/slider1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:54:37+01	78	52617e6399d6f38ea9cbf527bf430616
249	filer_public_thumbnails/filer_public/2013/09/30/slider2.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:54:37+01	79	52617e6399d6f38ea9cbf527bf430616
250	filer_public_thumbnails/filer_public/2013/09/30/slider2.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:54:37+01	79	52617e6399d6f38ea9cbf527bf430616
251	filer_public_thumbnails/filer_public/2013/09/30/slider2.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:54:38+01	79	52617e6399d6f38ea9cbf527bf430616
252	filer_public_thumbnails/filer_public/2013/09/30/slider3.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:54:38+01	80	52617e6399d6f38ea9cbf527bf430616
253	filer_public_thumbnails/filer_public/2013/09/30/slider2.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:54:38+01	79	52617e6399d6f38ea9cbf527bf430616
254	filer_public_thumbnails/filer_public/2013/09/30/slider3.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:54:38+01	80	52617e6399d6f38ea9cbf527bf430616
255	filer_public_thumbnails/filer_public/2013/09/30/slider3.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:54:38+01	80	52617e6399d6f38ea9cbf527bf430616
256	filer_public_thumbnails/filer_public/2013/09/30/slider3.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:54:38+01	80	52617e6399d6f38ea9cbf527bf430616
257	filer_public_thumbnails/filer_public/2013/09/30/slider2_1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:54:55+01	81	52617e6399d6f38ea9cbf527bf430616
258	filer_public_thumbnails/filer_public/2013/09/30/slider2_1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:54:55+01	81	52617e6399d6f38ea9cbf527bf430616
259	filer_public_thumbnails/filer_public/2013/09/30/slider3_1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:54:55+01	82	52617e6399d6f38ea9cbf527bf430616
260	filer_public_thumbnails/filer_public/2013/09/30/slider2_1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:54:55+01	81	52617e6399d6f38ea9cbf527bf430616
261	filer_public_thumbnails/filer_public/2013/09/30/slider3_1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:54:55+01	82	52617e6399d6f38ea9cbf527bf430616
262	filer_public_thumbnails/filer_public/2013/09/30/slider2_1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:54:56+01	81	52617e6399d6f38ea9cbf527bf430616
263	filer_public_thumbnails/filer_public/2013/09/30/slider3_1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:54:56+01	82	52617e6399d6f38ea9cbf527bf430616
264	filer_public_thumbnails/filer_public/2013/09/30/slider1_1.jpg__32x32_q85_crop_upscale.jpg	2013-09-30 12:54:56+01	83	52617e6399d6f38ea9cbf527bf430616
265	filer_public_thumbnails/filer_public/2013/09/30/slider3_1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:54:56+01	82	52617e6399d6f38ea9cbf527bf430616
266	filer_public_thumbnails/filer_public/2013/09/30/slider1_1.jpg__64x64_q85_crop_upscale.jpg	2013-09-30 12:54:56+01	83	52617e6399d6f38ea9cbf527bf430616
267	filer_public_thumbnails/filer_public/2013/09/30/slider1_1.jpg__48x48_q85_crop_upscale.jpg	2013-09-30 12:54:56+01	83	52617e6399d6f38ea9cbf527bf430616
268	filer_public_thumbnails/filer_public/2013/09/30/slider1_1.jpg__16x16_q85_crop_upscale.jpg	2013-09-30 12:54:56+01	83	52617e6399d6f38ea9cbf527bf430616
269	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-commandant.jpg__32x32_q85_crop_upscale.jpg	2013-10-01 15:24:53+01	84	52617e6399d6f38ea9cbf527bf430616
270	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-commandant.jpg__64x64_q85_crop_upscale.jpg	2013-10-01 15:24:53+01	84	52617e6399d6f38ea9cbf527bf430616
271	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-commandant.jpg__48x48_q85_crop_upscale.jpg	2013-10-01 15:24:53+01	84	52617e6399d6f38ea9cbf527bf430616
272	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-commandant.jpg__16x16_q85_crop_upscale.jpg	2013-10-01 15:24:53+01	84	52617e6399d6f38ea9cbf527bf430616
273	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-reception.jpg__32x32_q85_crop_upscale.jpg	2013-10-01 15:25:14+01	85	52617e6399d6f38ea9cbf527bf430616
274	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-reception.jpg__64x64_q85_crop_upscale.jpg	2013-10-01 15:25:14+01	85	52617e6399d6f38ea9cbf527bf430616
275	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-reception.jpg__48x48_q85_crop_upscale.jpg	2013-10-01 15:25:14+01	85	52617e6399d6f38ea9cbf527bf430616
276	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-reception.jpg__16x16_q85_crop_upscale.jpg	2013-10-01 15:25:14+01	85	52617e6399d6f38ea9cbf527bf430616
277	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-sailor.jpg__32x32_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	86	52617e6399d6f38ea9cbf527bf430616
278	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-sailor.jpg__64x64_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	86	52617e6399d6f38ea9cbf527bf430616
279	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-sailor.jpg__48x48_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	86	52617e6399d6f38ea9cbf527bf430616
280	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-service.jpg__32x32_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	87	52617e6399d6f38ea9cbf527bf430616
281	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-sailor.jpg__16x16_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	86	52617e6399d6f38ea9cbf527bf430616
282	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-service.jpg__64x64_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	87	52617e6399d6f38ea9cbf527bf430616
283	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-service.jpg__48x48_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	87	52617e6399d6f38ea9cbf527bf430616
284	filer_public_thumbnails/filer_public/2013/10/01/campaign2013-service.jpg__16x16_q85_crop_upscale.jpg	2013-10-01 15:25:40+01	87	52617e6399d6f38ea9cbf527bf430616
285	filer_public_thumbnails/filer_public/2013/09/30/slider3_1.jpg__210x10000_q85.jpg	2013-10-02 07:28:19+01	82	52617e6399d6f38ea9cbf527bf430616
286	filer_public_thumbnails/filer_public/2013/09/30/slide2_1.jpg__210x10000_q85.jpg	2013-10-02 07:28:33+01	77	52617e6399d6f38ea9cbf527bf430616
287	filer_public_thumbnails/filer_public/2013/09/30/slider1_1.jpg__210x10000_q85.jpg	2013-10-02 07:28:53+01	83	52617e6399d6f38ea9cbf527bf430616
288	filer_public_thumbnails/filer_public/2013/09/30/slider1.jpg__210x10000_q85.jpg	2013-10-02 07:30:16+01	78	52617e6399d6f38ea9cbf527bf430616
289	filer_public_thumbnails/filer_public/2013/10/13/home-banner.jpg__32x32_q85_crop_upscale.jpg	2013-10-13 22:29:03+01	91	52617e6399d6f38ea9cbf527bf430616
290	filer_public_thumbnails/filer_public/2013/10/13/home-banner.jpg__64x64_q85_crop_upscale.jpg	2013-10-13 22:29:03+01	91	52617e6399d6f38ea9cbf527bf430616
291	filer_public_thumbnails/filer_public/2013/10/13/home-banner.jpg__48x48_q85_crop_upscale.jpg	2013-10-13 22:29:03+01	91	52617e6399d6f38ea9cbf527bf430616
292	filer_public_thumbnails/filer_public/2013/10/13/home-banner.jpg__16x16_q85_crop_upscale.jpg	2013-10-13 22:29:03+01	91	52617e6399d6f38ea9cbf527bf430616
293	filer_public_thumbnails/filer_public/2013/10/13/home-banner.jpg__210x10000_q85.jpg	2013-10-13 22:32:11+01	91	52617e6399d6f38ea9cbf527bf430616
294	filer_public_thumbnails/filer_public/2013/10/13/home-banner-fr.jpg__32x32_q85_crop_upscale.jpg	2013-10-13 22:32:33+01	92	52617e6399d6f38ea9cbf527bf430616
295	filer_public_thumbnails/filer_public/2013/10/13/home-banner-fr.jpg__64x64_q85_crop_upscale.jpg	2013-10-13 22:32:33+01	92	52617e6399d6f38ea9cbf527bf430616
296	filer_public_thumbnails/filer_public/2013/10/13/home-banner-fr.jpg__48x48_q85_crop_upscale.jpg	2013-10-13 22:32:33+01	92	52617e6399d6f38ea9cbf527bf430616
297	filer_public_thumbnails/filer_public/2013/10/13/home-banner-fr.jpg__16x16_q85_crop_upscale.jpg	2013-10-13 22:32:33+01	92	52617e6399d6f38ea9cbf527bf430616
298	filer_public_thumbnails/filer_public/2013/10/13/home-banner-fr.jpg__210x10000_q85.jpg	2013-10-13 22:32:37+01	92	52617e6399d6f38ea9cbf527bf430616
299	filer_public_thumbnails/filer_public/2013/10/13/homefr-banner.jpg__32x32_q85_crop_upscale.jpg	2013-10-13 22:33:54+01	93	52617e6399d6f38ea9cbf527bf430616
300	filer_public_thumbnails/filer_public/2013/10/13/homefr-banner.jpg__64x64_q85_crop_upscale.jpg	2013-10-13 22:33:54+01	93	52617e6399d6f38ea9cbf527bf430616
301	filer_public_thumbnails/filer_public/2013/10/13/homefr-banner.jpg__48x48_q85_crop_upscale.jpg	2013-10-13 22:33:54+01	93	52617e6399d6f38ea9cbf527bf430616
302	filer_public_thumbnails/filer_public/2013/10/13/homefr-banner.jpg__16x16_q85_crop_upscale.jpg	2013-10-13 22:33:55+01	93	52617e6399d6f38ea9cbf527bf430616
303	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard.png__32x32_q85_crop_upscale.jpg	2013-10-13 22:46:25+01	94	52617e6399d6f38ea9cbf527bf430616
304	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard.png__64x64_q85_crop_upscale.jpg	2013-10-13 22:46:25+01	94	52617e6399d6f38ea9cbf527bf430616
305	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard.png__48x48_q85_crop_upscale.jpg	2013-10-13 22:46:25+01	94	52617e6399d6f38ea9cbf527bf430616
306	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard.png__16x16_q85_crop_upscale.jpg	2013-10-13 22:46:25+01	94	52617e6399d6f38ea9cbf527bf430616
307	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard.png__300x300_q85_crop.jpg	2013-10-13 22:46:40+01	94	52617e6399d6f38ea9cbf527bf430616
308	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard.png__200x100_q85_crop.jpg	2013-10-13 22:51:18+01	94	52617e6399d6f38ea9cbf527bf430616
309	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard.png__210x10000_q85.jpg	2013-10-13 22:52:45+01	94	52617e6399d6f38ea9cbf527bf430616
310	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard_1.png__32x32_q85_crop_upscale.jpg	2013-10-13 22:52:55+01	95	52617e6399d6f38ea9cbf527bf430616
311	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard_1.png__64x64_q85_crop_upscale.jpg	2013-10-13 22:52:55+01	95	52617e6399d6f38ea9cbf527bf430616
312	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard_1.png__48x48_q85_crop_upscale.jpg	2013-10-13 22:52:55+01	95	52617e6399d6f38ea9cbf527bf430616
313	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard_1.png__16x16_q85_crop_upscale.jpg	2013-10-13 22:52:55+01	95	52617e6399d6f38ea9cbf527bf430616
314	filer_public_thumbnails/filer_public/2013/10/13/fidelitycard_1.png__200x100_q85_crop.jpg	2013-10-13 22:52:58+01	95	52617e6399d6f38ea9cbf527bf430616
\.


--
-- Data for Name: filer_clipboard; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_clipboard (id, user_id) FROM stdin;
1	1
\.


--
-- Data for Name: filer_clipboarditem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_clipboarditem (id, file_id, clipboard_id) FROM stdin;
\.


--
-- Data for Name: filer_file; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_file (id, folder_id, file, _file_size, has_all_mandatory_data, original_filename, name, owner_id, uploaded_at, modified_at, description, is_public, sha1, polymorphic_ctype_id) FROM stdin;
37	9	filer_public/2013/10/01/campaign2013-commandant.jpg	89363	f	campaign2013-commandant.jpg		1	2013-09-30 10:26:25.730215+01	2013-10-01 15:24:52.965504+01		t	5a27d9daf104704a68ca57b008f7a99a22234a1d	14
36	9	filer_public/2013/10/01/campaign2013-reception.jpg	86821	f	campaign2013-reception.jpg		1	2013-09-30 10:26:25.721776+01	2013-10-01 15:25:14.31004+01		t	6becf4e62eadffec037b05d9d744891e07b18524	14
15	3	filer_public/2013/10/13/homefr-banner.jpg	179071	t	homeBanner-sample.jpg	home-banner.jpg	1	2013-09-27 17:40:16.222061+01	2013-10-13 22:33:54.597126+01		t	14ecc38a75a510750de3778d2247f280ed33428e	14
9	2	filer_public/2013/09/27/news-campaine2013.jpg	7391	f	news-campaine2013.jpg		1	2013-09-27 17:38:30.374489+01	2013-09-27 17:38:36.091461+01	\N	t	fad3c89d184b8b089c16f5d8e0fae89e33df9d16	14
10	2	filer_public/2013/09/27/news-1993_2013.jpg	11480	f	news-1993_2013.jpg		1	2013-09-27 17:38:30.407729+01	2013-09-27 17:38:36.124984+01	\N	t	96718aeb11ad1a0334332f03a2722acb5758ba1a	14
2	\N	filer_public/2013/09/27/promo-sample.jpg	7766	f	promo-sample.jpg		1	2013-09-27 09:46:17.421688+01	2013-09-27 17:38:40.80441+01	\N	t	f7c10b30ae81a7c7cbe48d5ba3b48a7554adb045	14
14	4	filer_public/2013/09/27/promo-genes_tunis.jpg	11909	f	promo-genes_tunis.jpg		1	2013-09-27 17:39:47.378629+01	2013-09-27 17:39:48.784395+01	\N	t	4d6bba909e16cb4c75c850ce1006fb21b9cd3b6d	14
16	4	filer_public/2013/09/27/promo-genes_maroc.jpg	10656	f	promo-genes_maroc.jpg		1	2013-09-27 17:54:43.304138+01	2013-09-27 17:54:45.093562+01	\N	t	1344d0f49f67250716d82db116286af083f29a37	14
17	4	filer_public/2013/09/27/promo-genova_maroc.jpg	10854	f	promo-genova_maroc.jpg		1	2013-09-27 17:54:43.687462+01	2013-09-27 17:54:45.141182+01	\N	t	7812471b6f40759d14eec4ca09c5d5636bb4bae5	14
11	5	filer_public/2013/09/27/topicthumb-destination.jpg	18766	f	topicThumb-destination.jpg		1	2013-09-27 17:39:08.626652+01	2013-09-27 18:57:26.330154+01	\N	t	8618a0cc4c5c9ff2b8c72b33f25968200eb47ed6	14
12	5	filer_public/2013/09/27/topicthumb-service.jpg	20068	f	topicThumb-service.jpg		1	2013-09-27 17:39:08.784792+01	2013-09-27 18:57:26.378834+01	\N	t	cb8527abffb7aa3200c310c519fc4b5d70d076c8	14
13	5	filer_public/2013/09/27/topicthumb-visitgnv.jpg	20273	f	topicThumb-visitGnv.jpg		1	2013-09-27 17:39:27.020049+01	2013-09-27 18:57:26.412154+01	\N	t	f15a95a9850cfcfbfcf73ceeef95f243388c3080	14
18	6	filer_public/2013/09/27/destinationtopic-genes.jpg	21863	f	destinationTopic-genes.jpg		1	2013-09-27 18:57:59.505127+01	2013-09-27 18:58:02.87321+01	\N	t	41068aa30028eb4504aa7cf72393e5c4aee5a5e4	14
19	6	filer_public/2013/09/27/destinationtopic-france.jpg	25962	f	destinationTopic-france.jpg		1	2013-09-27 18:57:59.513396+01	2013-09-27 18:58:02.908288+01	\N	t	5298f84919ddf12c4fc57a10382a0ad5e0a13652	14
20	6	filer_public/2013/09/27/destinationtopic-barcelona.jpg	24578	f	destinationTopic-barcelona.jpg		1	2013-09-27 18:57:59.513415+01	2013-09-27 18:58:02.941821+01	\N	t	16aded99466b6e473baceb8177b25ccb26fc5ea4	14
21	6	filer_public/2013/09/27/destinationtopic-maroc.jpg	16737	f	destinationTopic-maroc.jpg		1	2013-09-27 18:58:00.296326+01	2013-09-27 18:58:02.975055+01	\N	t	24643391cc4cc503de0f88685d310f79f6cb0f47	14
22	6	filer_public/2013/09/27/destinationtopic-naples.jpg	17754	f	destinationTopic-naples.jpg		1	2013-09-27 18:58:00.43776+01	2013-09-27 18:58:03.008255+01	\N	t	293e728c7e226c87993b320d19035d01594cffee	14
23	6	filer_public/2013/09/27/destinationtopic-nador.jpg	20256	f	destinationTopic-nador.jpg		1	2013-09-27 18:58:00.462905+01	2013-09-27 18:58:03.041476+01	\N	t	8f44e1afdeca3ecc014b3852f61782e82f54996c	14
24	6	filer_public/2013/09/27/destinationtopic-palerme.jpg	28302	f	destinationTopic-palerme.jpg		1	2013-09-27 18:58:00.92928+01	2013-09-27 18:58:03.075005+01	\N	t	fdbe41ef4876e7648e65b03bc7bf24c83f2b89e5	14
25	6	filer_public/2013/09/27/destinationtopic-rome.jpg	17316	f	destinationTopic-rome.jpg		1	2013-09-27 18:58:01.112463+01	2013-09-27 18:58:03.108025+01	\N	t	6c239d38a6025d06fc03a4a386ed659c0592c023	14
26	6	filer_public/2013/09/27/destinationtopic-sardaigne.jpg	12725	f	destinationTopic-sardaigne.jpg		1	2013-09-27 18:58:01.25408+01	2013-09-27 18:58:03.141365+01	\N	t	10ecb7eac9efe04debc0894b0eb04a9eca71db54	14
27	6	filer_public/2013/09/27/destinationtopic-sicile.jpg	16165	f	destinationTopic-sicile.jpg		1	2013-09-27 18:58:01.437276+01	2013-09-27 18:58:03.174786+01	\N	t	23b50710c6dc1579fff951d8becefab91c05078a	14
28	6	filer_public/2013/09/27/destinationtopic-tunis.jpg	12621	f	destinationTopic-tunis.jpg		1	2013-09-27 18:58:01.595561+01	2013-09-27 18:58:03.208119+01	\N	t	02bcfa11798c45da6a62bd6128b3bbdef2b214c5	14
29	7	filer_public/2013/09/27/servicestopics-fidelitycard.jpg	10010	f	servicesTopics-fidelityCard.jpg		1	2013-09-27 19:21:12.711147+01	2013-09-27 19:21:15.598702+01	\N	t	128325e3eadf68b9e1a8c85ed4de05d27bc43c6c	14
30	7	filer_public/2013/09/27/servicestopics-arrival.jpg	16982	f	servicesTopics-arrival.jpg		1	2013-09-27 19:21:12.761232+01	2013-09-27 19:21:15.647748+01	\N	t	5e8cbb29fcf796575b3a37340ad03375a5ca88fb	14
31	7	filer_public/2013/09/27/servicestopics-beforego.jpg	20170	f	servicesTopics-beforeGo.jpg		1	2013-09-27 19:21:12.861341+01	2013-09-27 19:21:15.68103+01	\N	t	80e00cfdcc6b156b14f62a5f08c68f0349f31fef	14
32	7	filer_public/2013/09/27/servicestopics-quality.jpg	18177	f	servicesTopics-quality.jpg		1	2013-09-27 19:21:13.610846+01	2013-09-27 19:21:15.714285+01	\N	t	55bf044bc22e2bc1a35c8a60a6092310344c0efb	14
33	7	filer_public/2013/09/27/servicestopics-onboard.jpg	19108	f	servicesTopics-onBoard.jpg		1	2013-09-27 19:21:13.669032+01	2013-09-27 19:21:15.747547+01	\N	t	5fcb37f7164383871f7dd7c1230fe22a66c2b2a8	14
34	7	filer_public/2013/09/27/servicestopics-guide.jpg	12964	f	servicesTopics-guide.jpg		1	2013-09-27 19:21:13.710868+01	2013-09-27 19:21:15.780975+01	\N	t	2dd65366a9f86714c6876e2788cbe5ee5c71e256	14
35	7	filer_public/2013/09/27/servicestopics-reunion.jpg	18368	f	servicesTopics-reunion.jpg		1	2013-09-27 19:21:14.168901+01	2013-09-27 19:21:15.814171+01	\N	t	976faa19f5a53ce6be7956246948b1c574a4f123	14
38	3	filer_public/2013/09/30/campaign-banner.jpg	218838	f	campaign-banner.jpg		1	2013-09-30 10:37:41.55838+01	2013-09-30 10:37:42.648977+01	\N	t	30a84106ed8745167a1c37c4cd661143429905e6	14
39	3	filer_public/2013/09/30/destinations-banner.jpg	185898	f	destinations-banner.jpg		1	2013-09-30 10:38:02.641308+01	2013-09-30 10:38:04.736372+01	\N	t	5696b8e5bf69b9bf15fba79ffa738df465e79f53	14
40	3	filer_public/2013/09/30/visitgnv-banner.jpg	201161	f	visitGnv-banner.jpg		1	2013-09-30 10:38:03.099415+01	2013-09-30 10:38:04.767375+01	\N	t	2b3d0ba36b55828c12fdf133cce4a248df02a317	14
42	3	filer_public/2013/09/30/contact-banner.jpg	271154	f	contact-banner.jpg		1	2013-09-30 10:52:23.85266+01	2013-09-30 10:52:24.707102+01	\N	t	ebaae0fe237d2bf5f52ad46ff0a49dbe72f61682	14
41	3	filer_public/2013/09/30/homear-banner.jpg	295859	f	homeAR-banner.jpg		1	2013-09-30 10:38:03.374247+01	2013-09-30 10:55:12.220261+01		t	082d2113cb2e4b596071f1e8941ecce60931fc8e	14
44	3	filer_public/2013/09/30/services-banner.jpg	134155	f	services-banner.jpg		1	2013-09-30 10:55:47.337021+01	2013-09-30 10:55:48.161882+01	\N	t	bf4a2a9de19d113a3813e0f8451981aad681d0db	14
45	11	filer_public/2013/09/30/slide3.jpg	229984	f	slide3.jpg		1	2013-09-30 10:58:16.28166+01	2013-09-30 10:58:17.99454+01	\N	t	84f80614c1ae1fd312a1259e3317a9d66dbc6f62	14
46	11	filer_public/2013/09/30/slide2.jpg	232446	f	slide2.jpg		1	2013-09-30 10:58:16.414909+01	2013-09-30 10:58:18.016322+01	\N	t	0d59972af7176494f8cb03fc240a71c3030789ab	14
47	11	filer_public/2013/09/30/slide1.jpg	237727	f	slide1.jpg		1	2013-09-30 10:58:16.839585+01	2013-09-30 10:58:18.033081+01	\N	t	e933d0c948d742c7e22f3a013764d08d088e6e37	14
48	12	filer_public/2013/09/30/slide1_1.jpg	266890	f	slide1.jpg		1	2013-09-30 12:05:14.641012+01	2013-09-30 12:05:15.972882+01	\N	t	6fefba4c9ed71ad1778dc51f1f078722f5690f08	14
49	12	filer_public/2013/09/30/slide3_1.jpg	252371	f	slide3.jpg		1	2013-09-30 12:05:14.782931+01	2013-09-30 12:05:16.00947+01	\N	t	abb5019676c37c0bee2c6a4044f22c165c874a24	14
50	12	filer_public/2013/09/30/slide2_1.jpg	259336	f	slide2.jpg		1	2013-09-30 12:05:14.882989+01	2013-09-30 12:05:16.026181+01	\N	t	42c3f0deb4e6a8645b22fbd16f5533b21476147d	14
51	13	filer_public/2013/09/30/slider1.jpg	268289	f	slider1.jpg		1	2013-09-30 12:54:37.369498+01	2013-09-30 12:54:39.042342+01	\N	t	63b75538fbf67d7349af781a8d878a0980a5c90c	14
52	13	filer_public/2013/09/30/slider2.jpg	316502	f	slider2.jpg		1	2013-09-30 12:54:37.786152+01	2013-09-30 12:54:39.06286+01	\N	t	51ee0683e2c2a7381daa83abbf5721cb589da4f2	14
53	13	filer_public/2013/09/30/slider3.jpg	294120	f	slider3.jpg		1	2013-09-30 12:54:37.952741+01	2013-09-30 12:54:39.079724+01	\N	t	133da0c14bf40adf036fa07f8fd09dec7c735a9a	14
54	14	filer_public/2013/09/30/slider2_1.jpg	233573	f	slider2.jpg		1	2013-09-30 12:54:55.755006+01	2013-09-30 12:54:56.707029+01	\N	t	a47914ddb2da30cc37f62373d3372ed299c97bfd	14
55	14	filer_public/2013/09/30/slider3_1.jpg	236135	f	slider3.jpg		1	2013-09-30 12:54:55.838147+01	2013-09-30 12:54:56.723834+01	\N	t	34c2211a6486c31675dc34d70b58c4e12d28a46f	14
56	14	filer_public/2013/09/30/slider1_1.jpg	251446	f	slider1.jpg		1	2013-09-30 12:54:55.996481+01	2013-09-30 12:54:56.740434+01	\N	t	7109b4d147179432f317a3f6c018c119887c48a2	14
57	9	filer_public/2013/10/01/campaign2013-sailor.jpg	80171	f	campaign2013-sailor.jpg		1	2013-10-01 15:25:40.279176+01	2013-10-01 15:25:41.197652+01	\N	t	c33ab417f574a880f2e6e8b8f42c277a5b0fda50	14
58	9	filer_public/2013/10/01/campaign2013-service.jpg	96440	f	campaign2013-service.jpg		1	2013-10-01 15:25:40.337547+01	2013-10-01 15:25:41.214326+01	\N	t	8a91ef083cf4fed6bcf0368992a3bf845a52bbc7	14
59	15	filer_public/2013/10/01/tv-ads-2013.mp4	13288045	f	tv-ads-2013.mp4		1	2013-10-01 15:34:26.393764+01	2013-10-01 15:34:47.82137+01	\N	t	400f592ab36b29e40d6bb8bdb7719eded7d05218	11
60	15	filer_public/2013/10/01/tv_ads_2013.ogv	6228387	f	tv_ads_2013.ogv		1	2013-10-01 15:45:26.602844+01	2013-10-01 15:46:44.372534+01	\N	t	c40516e9582e5bdbaeed76aa55d4d40d50fc8572	11
61	15	filer_public/2013/10/01/tv_ads_2013.webm	7542344	f	tv_ads_2013.webm		1	2013-10-01 15:45:30.408178+01	2013-10-01 15:46:44.409605+01	\N	t	8341ab1d2308ccb5c831c935a2c50c5e043184a6	11
62	2	filer_public/2013/10/13/fidelitycard_1.png	117943	f	fidelityCard.png		1	2013-10-13 22:46:25.313277+01	2013-10-13 22:52:55.456718+01		t	e748c2c9a891661d40ba28fc93e79a3636b58773	14
\.


--
-- Data for Name: filer_folder; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_folder (id, parent_id, name, owner_id, uploaded_at, created_at, modified_at, lft, rght, tree_id, level) FROM stdin;
2	\N	news	1	2013-09-27 17:34:09.912325+01	2013-09-27 17:34:09.912352+01	2013-09-27 17:34:09.912368+01	1	2	2	0
3	\N	banners	1	2013-09-27 17:34:18.440367+01	2013-09-27 17:34:18.440393+01	2013-09-27 17:34:18.440409+01	1	2	3	0
4	\N	promotions	1	2013-09-27 17:34:28.344934+01	2013-09-27 17:34:28.34496+01	2013-09-27 17:34:28.344976+01	1	2	4	0
5	1	home	1	2013-09-27 18:57:17.028335+01	2013-09-27 18:57:17.028363+01	2013-09-27 18:57:17.02838+01	2	3	1	1
6	1	destinations	1	2013-09-27 18:57:48.684229+01	2013-09-27 18:57:48.684256+01	2013-09-27 18:57:48.684272+01	4	5	1	1
1	\N	topics	1	2013-09-27 17:34:00.047134+01	2013-09-27 17:34:00.04716+01	2013-09-27 17:34:00.047176+01	1	8	1	0
7	1	services	1	2013-09-27 19:20:55.906786+01	2013-09-27 19:20:55.906812+01	2013-09-27 19:20:55.906827+01	6	7	1	1
8	\N	ads-campaingns	1	2013-09-30 10:26:00.102364+01	2013-09-30 10:26:00.10239+01	2013-09-30 10:26:00.102407+01	1	4	5	0
9	8	2013	1	2013-09-30 10:26:14.248713+01	2013-09-30 10:26:14.248739+01	2013-09-30 10:26:14.248755+01	2	3	5	1
11	10	restaurant	1	2013-09-30 10:56:15.229564+01	2013-09-30 10:56:15.229592+01	2013-09-30 12:03:54.480695+01	2	3	6	1
12	10	home	1	2013-09-30 12:04:01.234524+01	2013-09-30 12:04:01.234554+01	2013-09-30 12:04:01.23457+01	4	5	6	1
13	10	capitain	1	2013-09-30 12:54:16.819792+01	2013-09-30 12:54:16.819817+01	2013-09-30 12:54:16.819833+01	6	7	6	1
10	\N	360	1	2013-09-30 10:56:06.146246+01	2013-09-30 10:56:06.146275+01	2013-09-30 10:56:06.146291+01	1	10	6	0
14	10	piscine	1	2013-09-30 12:54:47.459753+01	2013-09-30 12:54:47.459779+01	2013-09-30 12:54:47.459796+01	8	9	6	1
15	\N	videos	1	2013-10-01 15:31:19.248162+01	2013-10-01 15:31:19.248191+01	2013-10-01 15:31:19.248207+01	1	2	7	0
\.


--
-- Data for Name: filer_folderpermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_folderpermission (id, folder_id, type, user_id, group_id, everybody, can_edit, can_read, can_add_children) FROM stdin;
\.


--
-- Data for Name: filer_image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_image (file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location) FROM stdin;
41	590	1004	2013-09-30 10:38:03.354411+01			\N	f	f	
44	277	1004	2013-09-30 10:55:47.30753+01	\N	\N	\N	f	f	\N
45	604	1004	2013-09-30 10:58:16.254522+01	\N	\N	\N	f	f	\N
9	98	162	2013-09-27 17:38:30.335755+01	\N	\N	\N	f	f	\N
10	84	186	2013-09-27 17:38:30.38523+01	\N	\N	\N	f	f	\N
2	97	162	2013-09-27 09:46:17.375039+01	\N	\N	\N	f	f	\N
46	604	1004	2013-09-30 10:58:16.396643+01	\N	\N	\N	f	f	\N
47	604	1004	2013-09-30 10:58:16.825328+01	\N	\N	\N	f	f	\N
14	97	162	2013-09-27 17:39:47.342688+01	\N	\N	\N	f	f	\N
16	96	128	2013-09-27 17:54:43.267006+01	\N	\N	\N	f	f	\N
17	103	137	2013-09-27 17:54:43.649208+01	\N	\N	\N	f	f	\N
48	604	1004	2013-09-30 12:05:14.60872+01	\N	\N	\N	f	f	\N
49	604	1004	2013-09-30 12:05:14.769212+01	\N	\N	\N	f	f	\N
11	140	209	2013-09-27 17:39:08.598964+01	\N	\N	\N	f	f	\N
12	140	209	2013-09-27 17:39:08.760399+01	\N	\N	\N	f	f	\N
13	139	209	2013-09-27 17:39:26.981487+01	\N	\N	\N	f	f	\N
18	131	209	2013-09-27 18:57:59.450678+01	\N	\N	\N	f	f	\N
19	131	209	2013-09-27 18:57:59.482714+01	\N	\N	\N	f	f	\N
20	131	209	2013-09-27 18:57:59.457288+01	\N	\N	\N	f	f	\N
21	131	209	2013-09-27 18:58:00.25358+01	\N	\N	\N	f	f	\N
22	131	209	2013-09-27 18:58:00.407818+01	\N	\N	\N	f	f	\N
23	131	209	2013-09-27 18:58:00.427378+01	\N	\N	\N	f	f	\N
24	131	209	2013-09-27 18:58:00.906778+01	\N	\N	\N	f	f	\N
25	131	209	2013-09-27 18:58:01.086834+01	\N	\N	\N	f	f	\N
26	131	208	2013-09-27 18:58:01.221351+01	\N	\N	\N	f	f	\N
27	131	209	2013-09-27 18:58:01.416633+01	\N	\N	\N	f	f	\N
28	131	209	2013-09-27 18:58:01.55633+01	\N	\N	\N	f	f	\N
29	138	209	2013-09-27 19:21:12.660015+01	\N	\N	\N	f	f	\N
30	138	209	2013-09-27 19:21:12.728982+01	\N	\N	\N	f	f	\N
31	138	209	2013-09-27 19:21:12.82867+01	\N	\N	\N	f	f	\N
32	138	209	2013-09-27 19:21:13.56746+01	\N	\N	\N	f	f	\N
33	138	209	2013-09-27 19:21:13.643232+01	\N	\N	\N	f	f	\N
34	138	209	2013-09-27 19:21:13.680556+01	\N	\N	\N	f	f	\N
35	138	209	2013-09-27 19:21:14.143066+01	\N	\N	\N	f	f	\N
38	591	1004	2013-09-30 10:37:41.513223+01	\N	\N	\N	f	f	\N
39	278	1004	2013-09-30 10:38:02.632153+01	\N	\N	\N	f	f	\N
40	280	1004	2013-09-30 10:38:03.084648+01	\N	\N	\N	f	f	\N
50	604	1004	2013-09-30 12:05:14.864439+01	\N	\N	\N	f	f	\N
42	450	1004	2013-09-30 10:52:23.835947+01	\N	\N	\N	f	f	\N
51	604	1004	2013-09-30 12:54:37.356087+01	\N	\N	\N	f	f	\N
52	604	1004	2013-09-30 12:54:37.769748+01	\N	\N	\N	f	f	\N
53	604	1004	2013-09-30 12:54:37.942567+01	\N	\N	\N	f	f	\N
54	604	1004	2013-09-30 12:54:55.744654+01	\N	\N	\N	f	f	\N
55	604	1004	2013-09-30 12:54:55.827732+01	\N	\N	\N	f	f	\N
56	604	1004	2013-09-30 12:54:55.979507+01	\N	\N	\N	f	f	\N
37	205	328	2013-09-30 10:26:25.699538+01			\N	f	f	
36	205	328	2013-09-30 10:26:25.695209+01			\N	f	f	
57	205	328	2013-10-01 15:25:40.268102+01	\N	\N	\N	f	f	\N
58	205	328	2013-10-01 15:25:40.323801+01	\N	\N	\N	f	f	\N
15	590	1004	2013-09-27 17:40:16.181583+01			\N	f	f	
62	351	482	2013-10-13 22:46:25.29106+01			\N	f	f	
\.


--
-- Data for Name: menus_cachekey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY menus_cachekey (id, language, site, key) FROM stdin;
120	ar	1	menu_cache_menu_nodes_ar_1_1_user
121	fr	1	menu_cache_menu_nodes_fr_1_1_user
\.


--
-- Data for Name: snippet_snippet; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY snippet_snippet (id, name, html, template) FROM stdin;
1	Moteur de recherche	<iframe id="searchModule" src="https://ssl.gnv.it/SceltaViaggio.aspx?codAge=XXXX&Lingua=fr-FR" frameborder="0" ></iframe>\r\n<style> #sideBar{ display: none !important; }</style>	
3	TVads2013	<video width="1004" controls autoplay>\r\n\t<source src="http://www.gnv.cre8izm.com/media/public/filer/filer_public/2013/10/01/tv-ads-2013.mp4" type="video/mp4">\r\n\t<source src="http://www.gnv.cre8izm.com/media/public/filer/filer_public/2013/10/01/tv-ads-2013.webm" type="video/webm">\r\n\t<source src="http://www.gnv.cre8izm.com/media/public/filer/filer_public/2013/10/01/tv-ads-2013.ogv" type="video/ogg">\r\n</video>	
2	360°		snippets/sliders/visit.html
\.


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY south_migrationhistory (id, app_name, migration, applied) FROM stdin;
1	filer	0001_initial	2013-09-27 08:24:58.881808+01
2	filer	0002_rename_file_field	2013-09-27 08:24:58.921954+01
3	filer	0003_add_description_field	2013-09-27 08:24:59.082729+01
4	filer	0004_auto__del_field_file__file__add_field_file_file__add_field_file_is_pub	2013-09-27 08:24:59.309475+01
5	filer	0005_auto__add_field_file_sha1__chg_field_file_file	2013-09-27 08:24:59.62837+01
6	filer	0006_polymorphic__add_field_file_polymorphic_ctype	2013-09-27 08:24:59.788544+01
7	filer	0007_polymorphic__content_type_data	2013-09-27 08:24:59.818263+01
8	filer	0008_polymorphic__del_field_file__file_type_plugin_name	2013-09-27 08:24:59.841498+01
9	filer	0009_auto__add_field_folderpermission_can_edit_new__add_field_folderpermiss	2013-09-27 08:24:59.868385+01
10	filer	0010_folderpermissions	2013-09-27 08:24:59.894069+01
11	filer	0011_auto__del_field_folderpermission_can_add_children__del_field_folderper	2013-09-27 08:24:59.926233+01
12	filer	0012_renaming_folderpermissions	2013-09-27 08:24:59.958139+01
13	filer	0013_remove_null_file_name	2013-09-27 08:24:59.986029+01
14	filer	0014_auto__add_field_image_related_url__chg_field_file_name	2013-09-27 08:25:00.17263+01
15	cms	0001_initial	2013-09-27 08:25:05.8627+01
16	cms	0002_auto_start	2013-09-27 08:25:05.902688+01
17	cms	0003_remove_placeholder	2013-09-27 08:25:05.943876+01
18	cms	0004_textobjects	2013-09-27 08:25:05.988046+01
19	cms	0005_mptt_added_to_plugins	2013-09-27 08:25:06.027215+01
20	cms	0006_apphook	2013-09-27 08:25:06.071131+01
21	cms	0007_apphook_longer	2013-09-27 08:25:06.110517+01
22	cms	0008_redirects	2013-09-27 08:25:06.152259+01
23	cms	0009_added_meta_fields	2013-09-27 08:25:06.196103+01
24	cms	0010_5char_language	2013-09-27 08:25:06.235764+01
25	cms	0011_title_overwrites	2013-09-27 08:25:06.277339+01
26	cms	0012_publisher	2013-09-27 08:25:06.321379+01
27	cms	0013_site_copy	2013-09-27 08:25:06.360509+01
28	cms	0014_sites_removed	2013-09-27 08:25:06.404334+01
29	cms	0015_modified_by_added	2013-09-27 08:25:06.443743+01
30	cms	0016_author_copy	2013-09-27 08:25:06.485452+01
31	cms	0017_author_removed	2013-09-27 08:25:06.582253+01
32	cms	0018_site_permissions	2013-09-27 08:25:06.617597+01
33	cms	0019_public_table_renames	2013-09-27 08:25:06.661641+01
34	cms	0020_advanced_permissions	2013-09-27 08:25:06.701647+01
35	cms	0021_publisher2	2013-09-27 08:25:06.743559+01
36	cms	0022_login_required_added	2013-09-27 08:25:06.786991+01
37	cms	0023_plugin_table_naming_function_changed	2013-09-27 08:25:06.82666+01
38	cms	0024_added_placeholder_model	2013-09-27 08:25:06.987257+01
39	cms	0025_placeholder_migration	2013-09-27 08:25:07.026689+01
40	cms	0026_finish_placeholder_migration	2013-09-27 08:25:07.068153+01
41	cms	0027_added_width_to_placeholder	2013-09-27 08:25:07.112654+01
42	cms	0028_limit_visibility_in_menu_step1of3	2013-09-27 08:25:07.152037+01
43	cms	0029_limit_visibility_in_menu_step2of3_data	2013-09-27 08:25:07.195567+01
44	cms	0030_limit_visibility_in_menu_step3of3	2013-09-27 08:25:07.234681+01
45	cms	0031_improved_language_code_support	2013-09-27 08:25:07.276921+01
46	cms	0032_auto__del_field_cmsplugin_publisher_public__del_field_cmsplugin_publis	2013-09-27 08:25:07.320917+01
47	cms	0033_auto__del_field_title_publisher_is_draft__del_field_title_publisher_st	2013-09-27 08:25:07.359836+01
48	cms	0034_auto__chg_field_title_language__chg_field_cmsplugin_language__add_fiel	2013-09-27 08:25:07.401331+01
49	cms	0035_auto__add_field_globalpagepermission_can_view__add_field_pagepermissio	2013-09-27 08:25:07.445376+01
50	cms	0036_auto__add_field_cmsplugin_changed_date	2013-09-27 08:25:07.484857+01
51	cms	0037_auto__del_pagemoderator__del_field_globalpagepermission_can_moderate__	2013-09-27 08:25:07.529638+01
52	cms	0038_publish_page_permission	2013-09-27 08:25:07.572645+01
53	cms	0039_auto__del_field_page_moderator_state	2013-09-27 08:25:07.608637+01
54	web	0001_initial	2013-09-27 08:25:10.837943+01
55	web	0002_auto__add_news	2013-09-27 08:25:10.963122+01
56	web	0003_auto__add_newspluginmodel	2013-09-27 08:25:11.022897+01
57	menus	0001_initial	2013-09-27 08:25:11.2805+01
58	reversion	0001_initial	2013-09-27 08:25:11.599457+01
59	reversion	0002_auto__add_field_version_type	2013-09-27 08:25:11.842253+01
60	reversion	0003_auto__add_field_version_object_id_int	2013-09-27 08:25:12.02697+01
61	reversion	0004_populate_object_id_int	2013-09-27 08:25:12.048385+01
62	reversion	0005_auto__add_field_revision_manager_slug	2013-09-27 08:25:12.270129+01
63	easy_thumbnails	0001_initial	2013-09-27 08:25:12.598474+01
64	easy_thumbnails	0002_filename_indexes	2013-09-27 08:25:12.682322+01
65	easy_thumbnails	0003_auto__add_storagenew	2013-09-27 08:25:12.783014+01
66	easy_thumbnails	0004_auto__add_field_source_storage_new__add_field_thumbnail_storage_new	2013-09-27 08:25:13.07667+01
67	easy_thumbnails	0005_storage_fks_null	2013-09-27 08:25:13.421874+01
68	easy_thumbnails	0006_copy_storage	2013-09-27 08:25:13.435666+01
69	easy_thumbnails	0007_storagenew_fks_not_null	2013-09-27 08:25:13.716798+01
70	easy_thumbnails	0008_auto__del_field_source_storage__del_field_thumbnail_storage	2013-09-27 08:25:13.737268+01
71	easy_thumbnails	0009_auto__del_storage	2013-09-27 08:25:13.750104+01
72	easy_thumbnails	0010_rename_storage	2013-09-27 08:25:13.769543+01
73	easy_thumbnails	0011_auto__add_field_source_storage_hash__add_field_thumbnail_storage_hash	2013-09-27 08:25:14.126782+01
74	easy_thumbnails	0012_build_storage_hashes	2013-09-27 08:25:14.143351+01
75	easy_thumbnails	0013_auto__del_storage__del_field_source_storage__del_field_thumbnail_stora	2013-09-27 08:25:14.202003+01
76	easy_thumbnails	0014_auto__add_unique_source_name_storage_hash__add_unique_thumbnail_name_s	2013-09-27 08:25:14.269063+01
77	easy_thumbnails	0015_auto__del_unique_thumbnail_name_storage_hash__add_unique_thumbnail_sou	2013-09-27 08:25:14.319111+01
78	link	0001_initial	2013-09-27 08:25:14.486712+01
79	link	0002_link_rename	2013-09-27 08:25:14.505256+01
80	link	0003_page_link	2013-09-27 08:25:14.524311+01
81	link	0004_larger_link_names	2013-09-27 08:25:14.547043+01
82	link	0005_publisher	2013-09-27 08:25:14.563659+01
83	link	0006_table_rename	2013-09-27 08:25:14.580648+01
84	link	0007_publisher2	2013-09-27 08:25:14.596781+01
85	link	0008_mailto	2013-09-27 08:25:14.613342+01
86	link	0009_add_target	2013-09-27 08:25:14.629848+01
87	text	0001_initial	2013-09-27 08:25:14.838521+01
88	text	0002_freeze	2013-09-27 08:25:14.848888+01
89	text	0003_publisher	2013-09-27 08:25:14.85747+01
90	text	0004_table_rename	2013-09-27 08:25:14.865494+01
91	text	0005_publisher2	2013-09-27 08:25:14.874062+01
92	text	0006_2_1_4_upgrade	2013-09-27 08:25:14.890464+01
93	flash	0001_initial	2013-09-27 08:25:15.0054+01
94	flash	0002_freeze	2013-09-27 08:25:15.0161+01
95	flash	0003_publisher	2013-09-27 08:25:15.032594+01
96	flash	0004_table_rename	2013-09-27 08:25:15.049181+01
97	flash	0005_publisher2	2013-09-27 08:25:15.068382+01
98	picture	0001_initial	2013-09-27 08:25:15.21211+01
99	picture	0002_link_rename	2013-09-27 08:25:15.232748+01
100	picture	0003_freeze	2013-09-27 08:25:15.255435+01
101	picture	0004_publisher	2013-09-27 08:25:15.271785+01
102	picture	0005_table_rename	2013-09-27 08:25:15.288707+01
103	picture	0006_float_added	2013-09-27 08:25:15.305053+01
104	picture	0007_publisher2	2013-09-27 08:25:15.321949+01
105	picture	0008_longdesc_added	2013-09-27 08:25:15.338574+01
106	twitter	0001_initial	2013-09-27 08:25:15.557133+01
107	twitter	0002_auto__add_twittersearch	2013-09-27 08:25:15.575771+01
108	snippet	0001_initial	2013-09-27 08:25:15.822798+01
109	snippet	0002_publisher	2013-09-27 08:25:15.841993+01
110	snippet	0003_table_rename	2013-09-27 08:25:15.858281+01
111	snippet	0004_publisher2	2013-09-27 08:25:15.875005+01
112	snippet	0005_template_added	2013-09-27 08:25:15.891818+01
113	googlemap	0001_initial	2013-09-27 08:25:16.024724+01
114	googlemap	0002_table_rename	2013-09-27 08:25:16.042426+01
115	googlemap	0003_address_unified	2013-09-27 08:25:16.058486+01
116	googlemap	0004_copy_address	2013-09-27 08:25:16.075709+01
117	googlemap	0005_delete_street	2013-09-27 08:25:16.092098+01
118	googlemap	0006_publisher2	2013-09-27 08:25:16.108653+01
119	googlemap	0007_latlng	2013-09-27 08:25:16.12785+01
120	googlemap	0008_routeplaner	2013-09-27 08:25:16.142016+01
121	googlemap	0009_routeplaner_title	2013-09-27 08:25:16.158592+01
122	googlemap	0010_auto__chg_field_googlemap_content	2013-09-27 08:25:16.175334+01
123	googlemap	0011_remove_zoom_null_values	2013-09-27 08:25:16.191803+01
124	googlemap	0012_auto__add_field_googlemap_width__add_field_googlemap_height__chg_field	2013-09-27 08:25:16.208631+01
125	web	0004_auto__add_field_topic_published__add_field_deal_caption__add_field_dea	2013-09-27 10:51:12.810879+01
126	web	0005_auto__add_field_deal_sidebar__add_field_news_sidebar	2013-09-27 11:22:57.084627+01
127	cmsplugin_contact	0001_initial	2013-09-27 11:43:11.436874+01
128	cmsplugin_contact	0002_auto__chg_field_contact_thanks	2013-09-27 11:43:11.54313+01
129	web	0006_auto__del_field_topic_description__del_field_topic_title__add_field_to	2013-09-28 09:17:19.769978+01
\.


--
-- Data for Name: web_deal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY web_deal (id, url, price, end_date, start_date, image_id, collection_id, published, sidebar, title_fr, title_en, title_ar, caption_fr, caption_en, caption_ar, description_fr, description_en, description_ar) FROM stdin;
4	#	450	\N	\N	16	1	t	t	GENES - MAROC OFFRE FAMILIALE	\N	GENES - MAROC OFFRE FAMILIALE	Offrir aux familles	\N	Offrir aux familles	POUR 2 ADULTES, 2 ENFANTS ET UNE VOITURE\r\n(OFFRE VALABLE POUR UNE TRAVERSÉ AVANT LE\r\n22 JUILLET, QUANTITÉS LIMITÉES)	\N	2 بالغين و 2 أطفال وسيارة (يسري العرض لعبور قبل 22 يوليو كميات محدودة)
2	#	424	2013-09-27	2013-09-27	14	1	t	t	GENES - TUNIS OFFRE FAMILIALE	Deal1	GENES - TUNIS OFFRE FAMILIALE	Offrir aux familles	Caption1	Offrir aux familles	POUR 2 ADULTES, 2 ENFANTS ET UNE VOITURE\r\n(OFFRE VALABLE POUR UNE TRAVERSÉ AVANT LE\r\n22 JUILLET, QUANTITÉS LIMITÉES)	is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum	2 بالغين و 2 أطفال وسيارة (يسري العرض لعبور قبل 22 يوليو كميات محدودة)
3	#	181	2013-09-27	2013-09-27	17	1	t	t	Promotion MAROC - GENOVA	Deal2	Promotion MAROC - GENOVA	Offrir aux familles	Caption2	Offrir aux familles	Jusqu'au 30 Juin 2013 si vous réservez un voyage\r\nallée/rétour de Gênes ou de Barcelone pour Tanger\r\nou de Sète pour Tanger ou Nador vous aurez le 20%\r\nde reduction sur le billet du rétour	is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum	2 بالغين و 2 أطفال وسيارة (يسري العرض لعبور قبل 22 يوليو كميات محدودة)
\.


--
-- Data for Name: web_dealscollection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY web_dealscollection (id, title_fr, title_en, title_ar) FROM stdin;
1	Promotions1	Deals1	عروض1
\.


--
-- Data for Name: web_news; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY web_news (id, url, image_id, published, sidebar, title_fr, title_en, title_ar, caption_fr, caption_en, caption_ar, description_fr, description_en, description_ar) FROM stdin;
9	la-carte/ 	62	t	t	LE PROGRAMME DE FIDÉLITÉ	News1	برنامج الولاء	INSCRIVEZ VOUS ET VOYAGEZ GRATUITEMENT	Caption1	تسجيل والسفر مجانا	 	is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum	
\.


--
-- Data for Name: web_topic; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY web_topic (id, url, image_id, collection_id, published, title_fr, title_en, title_ar, description_fr, description_en, description_ar) FROM stdin;
25	visit-gnv/ 	13	5	t	VISITEZ GNV	\N	إبدأ الزيارة	Vivez l’expérience GNV grâce à notre visite virtuelle, Explorez l’univers de votre traversée !	\N	تجربة GNV من خلال جولتنا الافتراضية, استكشاف العالم من المسار الخاص بك!
26	destinations/ 	11	5	t	DESTINATIONS	\N	رحلات	Le groupe possède dix navires et assure les liaisons vers la Sardaigne, la Sicile, l'Espagne, la Tunisie, le Maroc et la France.	\N	الفريق لديه عشرة السفن ويوفر وصلات الى سردينيا وصقلية وإسبانيا وتونس والمغرب وفرنسا.
27	services/ 	12	5	t	SERVICES	\N	الخدمات	Découvrez ce que vous réserve GNV pour un agréable séjour à bord de ses ferries.	\N	معرفة ما تقدمها GNV لإقامة ممتعة على متن العبارات لها.
39	#	31	7	t	AVANT DE PARTIR	\N	قبل المغادرة		\N	
40	#	33	7	t	SERVICES À BORD	\N	ادارة خدمات		\N	
41	#	34	7	t	GUIDE	\N	دليل		\N	
42	#	29	7	t	CARTE FIDÉLITÉ	\N	بطاقة الخصم		\N	
43	#	32	7	t	QUALITÉ	\N	الجودة		\N	
44	#	30	7	t	ARRIVÉE	\N	الوصول		\N	
45	#	35	7	t	REUNION 	\N	إجتماع	 	\N	
29		24	6	t	PALERME (SICILE)	\N	باليرمو (صقلية)		\N	
30		27	6	t	TERMINI IMERSE (SICILE)	\N	TERMINI IMERSE (SICILE)		\N	
31		26	6	t	PORTO TORRES (SARDAIGNE)	\N	PORTO TORRES (SARDAIGNE)		\N	
32		18	6	t	GÉNES ( LIGURIE)	\N	GÉNES ( LIGURIE)		\N	
33		25	6	t	 CIVITAVECCHIA (ROME)	\N	 CIVITAVECCHIA (ROME)		\N	
34		20	6	t	BARCELONE (ESPAGNE)	\N	BARCELONE (ESPAGNE)		\N	
35		28	6	t	TUNIS (TUNISIE)	\N	TUNIS (TUNISIE)		\N	
36		21	6	t	TANGER (MAROC)	\N	TANGER (MAROC)		\N	
37		23	6	t	NADOR (MAROC)	\N	NADOR (MAROC)		\N	
38		19	6	t	SÉTE (FRANCE)	\N	SÉTE (FRANCE)		\N	
28		22	6	t	NAPLES (CAMPANIE)	\N	نابولي (CAMPANIE)		\N	
\.


--
-- Data for Name: web_topicscollection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY web_topicscollection (id, title_fr, title_en, title_ar) FROM stdin;
5	Accueil	\N	إستقبال
7	Services	\N	الخدمات
6	Destinations	\N	
\.


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: cms_cmsplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_pkey PRIMARY KEY (id);


--
-- Name: cms_globalpageper_globalpagepermission_id_1ffcd9d4c6c0cbfa_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpageper_globalpagepermission_id_1ffcd9d4c6c0cbfa_uniq UNIQUE (globalpagepermission_id, site_id);


--
-- Name: cms_globalpagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_globalpagepermission
    ADD CONSTRAINT cms_globalpagepermission_pkey PRIMARY KEY (id);


--
-- Name: cms_globalpagepermission_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermission_sites_pkey PRIMARY KEY (id);


--
-- Name: cms_page_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT cms_page_pkey PRIMARY KEY (id);


--
-- Name: cms_page_placeholders_page_id_598353cf6a0df834_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_page_id_598353cf6a0df834_uniq UNIQUE (page_id, placeholder_id);


--
-- Name: cms_page_placeholders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_pkey PRIMARY KEY (id);


--
-- Name: cms_page_publisher_public_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT cms_page_publisher_public_id_key UNIQUE (publisher_public_id);


--
-- Name: cms_pagemoderatorstate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_pagemoderatorstate
    ADD CONSTRAINT cms_pagemoderatorstate_pkey PRIMARY KEY (id);


--
-- Name: cms_pagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_pkey PRIMARY KEY (id);


--
-- Name: cms_pageuser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_pageuser
    ADD CONSTRAINT cms_pageuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: cms_pageusergroup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_pageusergroup
    ADD CONSTRAINT cms_pageusergroup_pkey PRIMARY KEY (group_ptr_id);


--
-- Name: cms_placeholder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_placeholder
    ADD CONSTRAINT cms_placeholder_pkey PRIMARY KEY (id);


--
-- Name: cms_title_language_7a69dc7eaf856287_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT cms_title_language_7a69dc7eaf856287_uniq UNIQUE (language, page_id);


--
-- Name: cms_title_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT cms_title_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_dealspluginmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_dealspluginmodel
    ADD CONSTRAINT cmsplugin_dealspluginmodel_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_flash_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_flash
    ADD CONSTRAINT cmsplugin_flash_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_googlemap_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_googlemap
    ADD CONSTRAINT cmsplugin_googlemap_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_link_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_link
    ADD CONSTRAINT cmsplugin_link_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_newspluginmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_newspluginmodel
    ADD CONSTRAINT cmsplugin_newspluginmodel_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_picture_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_picture
    ADD CONSTRAINT cmsplugin_picture_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_snippetptr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_snippetptr
    ADD CONSTRAINT cmsplugin_snippetptr_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_text_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_text
    ADD CONSTRAINT cmsplugin_text_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_topicpluginmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_topicpluginmodel
    ADD CONSTRAINT cmsplugin_topicpluginmodel_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_twitterrecententries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_twitterrecententries
    ADD CONSTRAINT cmsplugin_twitterrecententries_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_twittersearch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cmsplugin_twittersearch
    ADD CONSTRAINT cmsplugin_twittersearch_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source_name_7549c98cc6dd6969_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_name_7549c98cc6dd6969_uniq UNIQUE (name, storage_hash);


--
-- Name: easy_thumbnails_source_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnail_source_id_1f50d53db8191480_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_source_id_1f50d53db8191480_uniq UNIQUE (source_id, name, storage_hash);


--
-- Name: filer_clipboard_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filer_clipboard
    ADD CONSTRAINT filer_clipboard_pkey PRIMARY KEY (id);


--
-- Name: filer_clipboarditem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filer_clipboarditem
    ADD CONSTRAINT filer_clipboarditem_pkey PRIMARY KEY (id);


--
-- Name: filer_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT filer_file_pkey PRIMARY KEY (id);


--
-- Name: filer_folder_parent_id_30ad83e34d901e49_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT filer_folder_parent_id_30ad83e34d901e49_uniq UNIQUE (parent_id, name);


--
-- Name: filer_folder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT filer_folder_pkey PRIMARY KEY (id);


--
-- Name: filer_folderpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_pkey PRIMARY KEY (id);


--
-- Name: filer_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filer_image
    ADD CONSTRAINT filer_image_pkey PRIMARY KEY (file_ptr_id);


--
-- Name: menus_cachekey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY menus_cachekey
    ADD CONSTRAINT menus_cachekey_pkey PRIMARY KEY (id);


--
-- Name: snippet_snippet_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY snippet_snippet
    ADD CONSTRAINT snippet_snippet_name_key UNIQUE (name);


--
-- Name: snippet_snippet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY snippet_snippet
    ADD CONSTRAINT snippet_snippet_pkey PRIMARY KEY (id);


--
-- Name: south_migrationhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);


--
-- Name: web_deal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY web_deal
    ADD CONSTRAINT web_deal_pkey PRIMARY KEY (id);


--
-- Name: web_dealscollection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY web_dealscollection
    ADD CONSTRAINT web_dealscollection_pkey PRIMARY KEY (id);


--
-- Name: web_news_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY web_news
    ADD CONSTRAINT web_news_pkey PRIMARY KEY (id);


--
-- Name: web_topic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY web_topic
    ADD CONSTRAINT web_topic_pkey PRIMARY KEY (id);


--
-- Name: web_topicscollection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY web_topicscollection
    ADD CONSTRAINT web_topicscollection_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_name_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_username_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: cms_cmsplugin_language; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_language ON cms_cmsplugin USING btree (language);


--
-- Name: cms_cmsplugin_language_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_language_like ON cms_cmsplugin USING btree (language varchar_pattern_ops);


--
-- Name: cms_cmsplugin_level; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_level ON cms_cmsplugin USING btree (level);


--
-- Name: cms_cmsplugin_lft; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_lft ON cms_cmsplugin USING btree (lft);


--
-- Name: cms_cmsplugin_parent_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_parent_id ON cms_cmsplugin USING btree (parent_id);


--
-- Name: cms_cmsplugin_placeholder_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_placeholder_id ON cms_cmsplugin USING btree (placeholder_id);


--
-- Name: cms_cmsplugin_plugin_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_plugin_type ON cms_cmsplugin USING btree (plugin_type);


--
-- Name: cms_cmsplugin_plugin_type_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_plugin_type_like ON cms_cmsplugin USING btree (plugin_type varchar_pattern_ops);


--
-- Name: cms_cmsplugin_rght; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_rght ON cms_cmsplugin USING btree (rght);


--
-- Name: cms_cmsplugin_tree_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_cmsplugin_tree_id ON cms_cmsplugin USING btree (tree_id);


--
-- Name: cms_globalpagepermission_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_globalpagepermission_group_id ON cms_globalpagepermission USING btree (group_id);


--
-- Name: cms_globalpagepermission_sites_globalpagepermission_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_globalpagepermission_sites_globalpagepermission_id ON cms_globalpagepermission_sites USING btree (globalpagepermission_id);


--
-- Name: cms_globalpagepermission_sites_site_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_globalpagepermission_sites_site_id ON cms_globalpagepermission_sites USING btree (site_id);


--
-- Name: cms_globalpagepermission_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_globalpagepermission_user_id ON cms_globalpagepermission USING btree (user_id);


--
-- Name: cms_page_in_navigation; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_in_navigation ON cms_page USING btree (in_navigation);


--
-- Name: cms_page_level; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_level ON cms_page USING btree (level);


--
-- Name: cms_page_lft; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_lft ON cms_page USING btree (lft);


--
-- Name: cms_page_limit_visibility_in_menu; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_limit_visibility_in_menu ON cms_page USING btree (limit_visibility_in_menu);


--
-- Name: cms_page_navigation_extenders; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_navigation_extenders ON cms_page USING btree (navigation_extenders);


--
-- Name: cms_page_navigation_extenders_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_navigation_extenders_like ON cms_page USING btree (navigation_extenders varchar_pattern_ops);


--
-- Name: cms_page_parent_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_parent_id ON cms_page USING btree (parent_id);


--
-- Name: cms_page_placeholders_page_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_placeholders_page_id ON cms_page_placeholders USING btree (page_id);


--
-- Name: cms_page_placeholders_placeholder_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_placeholders_placeholder_id ON cms_page_placeholders USING btree (placeholder_id);


--
-- Name: cms_page_publication_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_publication_date ON cms_page USING btree (publication_date);


--
-- Name: cms_page_publication_end_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_publication_end_date ON cms_page USING btree (publication_end_date);


--
-- Name: cms_page_publisher_is_draft; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_publisher_is_draft ON cms_page USING btree (publisher_is_draft);


--
-- Name: cms_page_publisher_state; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_publisher_state ON cms_page USING btree (publisher_state);


--
-- Name: cms_page_reverse_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_reverse_id ON cms_page USING btree (reverse_id);


--
-- Name: cms_page_reverse_id_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_reverse_id_like ON cms_page USING btree (reverse_id varchar_pattern_ops);


--
-- Name: cms_page_rght; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_rght ON cms_page USING btree (rght);


--
-- Name: cms_page_site_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_site_id ON cms_page USING btree (site_id);


--
-- Name: cms_page_soft_root; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_soft_root ON cms_page USING btree (soft_root);


--
-- Name: cms_page_tree_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_page_tree_id ON cms_page USING btree (tree_id);


--
-- Name: cms_pagemoderatorstate_page_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_pagemoderatorstate_page_id ON cms_pagemoderatorstate USING btree (page_id);


--
-- Name: cms_pagemoderatorstate_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_pagemoderatorstate_user_id ON cms_pagemoderatorstate USING btree (user_id);


--
-- Name: cms_pagepermission_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_pagepermission_group_id ON cms_pagepermission USING btree (group_id);


--
-- Name: cms_pagepermission_page_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_pagepermission_page_id ON cms_pagepermission USING btree (page_id);


--
-- Name: cms_pagepermission_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_pagepermission_user_id ON cms_pagepermission USING btree (user_id);


--
-- Name: cms_pageuser_created_by_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_pageuser_created_by_id ON cms_pageuser USING btree (created_by_id);


--
-- Name: cms_pageusergroup_created_by_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_pageusergroup_created_by_id ON cms_pageusergroup USING btree (created_by_id);


--
-- Name: cms_placeholder_slot; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_placeholder_slot ON cms_placeholder USING btree (slot);


--
-- Name: cms_placeholder_slot_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_placeholder_slot_like ON cms_placeholder USING btree (slot varchar_pattern_ops);


--
-- Name: cms_title_application_urls; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_application_urls ON cms_title USING btree (application_urls);


--
-- Name: cms_title_application_urls_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_application_urls_like ON cms_title USING btree (application_urls varchar_pattern_ops);


--
-- Name: cms_title_has_url_overwrite; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_has_url_overwrite ON cms_title USING btree (has_url_overwrite);


--
-- Name: cms_title_language; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_language ON cms_title USING btree (language);


--
-- Name: cms_title_language_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_language_like ON cms_title USING btree (language varchar_pattern_ops);


--
-- Name: cms_title_page_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_page_id ON cms_title USING btree (page_id);


--
-- Name: cms_title_path; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_path ON cms_title USING btree (path);


--
-- Name: cms_title_path_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_path_like ON cms_title USING btree (path varchar_pattern_ops);


--
-- Name: cms_title_slug; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_slug ON cms_title USING btree (slug);


--
-- Name: cms_title_slug_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cms_title_slug_like ON cms_title USING btree (slug varchar_pattern_ops);


--
-- Name: cmsplugin_dealspluginmodel_deals_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cmsplugin_dealspluginmodel_deals_id ON cmsplugin_dealspluginmodel USING btree (deals_id);


--
-- Name: cmsplugin_link_page_link_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cmsplugin_link_page_link_id ON cmsplugin_link USING btree (page_link_id);


--
-- Name: cmsplugin_picture_page_link_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cmsplugin_picture_page_link_id ON cmsplugin_picture USING btree (page_link_id);


--
-- Name: cmsplugin_snippetptr_snippet_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cmsplugin_snippetptr_snippet_id ON cmsplugin_snippetptr USING btree (snippet_id);


--
-- Name: cmsplugin_topicpluginmodel_topics_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cmsplugin_topicpluginmodel_topics_id ON cmsplugin_topicpluginmodel USING btree (topics_id);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_session_key_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_name ON easy_thumbnails_source USING btree (name);


--
-- Name: easy_thumbnails_source_storage_hash; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_storage_hash ON easy_thumbnails_source USING btree (storage_hash);


--
-- Name: easy_thumbnails_thumbnail_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_name ON easy_thumbnails_thumbnail USING btree (name);


--
-- Name: easy_thumbnails_thumbnail_source_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_source_id ON easy_thumbnails_thumbnail USING btree (source_id);


--
-- Name: easy_thumbnails_thumbnail_storage_hash; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash ON easy_thumbnails_thumbnail USING btree (storage_hash);


--
-- Name: filer_clipboard_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_clipboard_user_id ON filer_clipboard USING btree (user_id);


--
-- Name: filer_clipboarditem_clipboard_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_clipboarditem_clipboard_id ON filer_clipboarditem USING btree (clipboard_id);


--
-- Name: filer_clipboarditem_file_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_clipboarditem_file_id ON filer_clipboarditem USING btree (file_id);


--
-- Name: filer_file_folder_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_file_folder_id ON filer_file USING btree (folder_id);


--
-- Name: filer_file_owner_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_file_owner_id ON filer_file USING btree (owner_id);


--
-- Name: filer_file_polymorphic_ctype_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_file_polymorphic_ctype_id ON filer_file USING btree (polymorphic_ctype_id);


--
-- Name: filer_folder_level; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folder_level ON filer_folder USING btree (level);


--
-- Name: filer_folder_lft; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folder_lft ON filer_folder USING btree (lft);


--
-- Name: filer_folder_owner_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folder_owner_id ON filer_folder USING btree (owner_id);


--
-- Name: filer_folder_parent_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folder_parent_id ON filer_folder USING btree (parent_id);


--
-- Name: filer_folder_rght; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folder_rght ON filer_folder USING btree (rght);


--
-- Name: filer_folder_tree_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folder_tree_id ON filer_folder USING btree (tree_id);


--
-- Name: filer_folderpermission_folder_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folderpermission_folder_id ON filer_folderpermission USING btree (folder_id);


--
-- Name: filer_folderpermission_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folderpermission_group_id ON filer_folderpermission USING btree (group_id);


--
-- Name: filer_folderpermission_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filer_folderpermission_user_id ON filer_folderpermission USING btree (user_id);


--
-- Name: snippet_snippet_name_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snippet_snippet_name_like ON snippet_snippet USING btree (name varchar_pattern_ops);


--
-- Name: web_deal_collection_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX web_deal_collection_id ON web_deal USING btree (collection_id);


--
-- Name: web_deal_image_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX web_deal_image_id ON web_deal USING btree (image_id);


--
-- Name: web_news_image_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX web_news_image_id ON web_news USING btree (image_id);


--
-- Name: web_topic_collection_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX web_topic_collection_id ON web_topic USING btree (collection_id);


--
-- Name: web_topic_image_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX web_topic_image_id ON web_topic USING btree (image_id);


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clipboard_id_refs_id_fd11d4a0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboarditem
    ADD CONSTRAINT clipboard_id_refs_id_fd11d4a0 FOREIGN KEY (clipboard_id) REFERENCES filer_clipboard(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_04625004; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_twittersearch
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_04625004 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_1639aa84; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_link
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_1639aa84 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_34ad5108; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_text
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_34ad5108 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_6069d307; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_snippetptr
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_6069d307 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_6e3a69f0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_picture
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_6e3a69f0 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_77bc83b6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_googlemap
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_77bc83b6 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_8a5d824b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_twitterrecententries
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_8a5d824b FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_9e53586e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_topicpluginmodel
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_9e53586e FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_d7845ad9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_newspluginmodel
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_d7845ad9 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_e1d01d16; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_dealspluginmodel
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_e1d01d16 FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_ptr_id_refs_id_faf66a7f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_flash
    ADD CONSTRAINT cmsplugin_ptr_id_refs_id_faf66a7f FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: collection_id_refs_id_6b713270; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_topic
    ADD CONSTRAINT collection_id_refs_id_6b713270 FOREIGN KEY (collection_id) REFERENCES web_topicscollection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: collection_id_refs_id_783acf74; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_deal
    ADD CONSTRAINT collection_id_refs_id_783acf74 FOREIGN KEY (collection_id) REFERENCES web_dealscollection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_d043b34a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: created_by_id_refs_id_9da6953b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageuser
    ADD CONSTRAINT created_by_id_refs_id_9da6953b FOREIGN KEY (created_by_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: created_by_id_refs_id_f9e2dd36; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageusergroup
    ADD CONSTRAINT created_by_id_refs_id_f9e2dd36 FOREIGN KEY (created_by_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: deals_id_refs_id_ae2a9fdc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_dealspluginmodel
    ADD CONSTRAINT deals_id_refs_id_ae2a9fdc FOREIGN KEY (deals_id) REFERENCES web_dealscollection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: file_id_refs_id_a869c276; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboarditem
    ADD CONSTRAINT file_id_refs_id_a869c276 FOREIGN KEY (file_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: file_ptr_id_refs_id_7375d00c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_image
    ADD CONSTRAINT file_ptr_id_refs_id_7375d00c FOREIGN KEY (file_ptr_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: folder_id_refs_id_41348f39; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT folder_id_refs_id_41348f39 FOREIGN KEY (folder_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: folder_id_refs_id_b2c18baf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT folder_id_refs_id_b2c18baf FOREIGN KEY (folder_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: globalpagepermission_id_refs_id_09b826f9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT globalpagepermission_id_refs_id_09b826f9 FOREIGN KEY (globalpagepermission_id) REFERENCES cms_globalpagepermission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_5f980916; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission
    ADD CONSTRAINT group_id_refs_id_5f980916 FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_65c5081c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT group_id_refs_id_65c5081c FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_f4b32aac; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_f86eccc7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT group_id_refs_id_f86eccc7 FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_ptr_id_refs_id_b270d7fc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageusergroup
    ADD CONSTRAINT group_ptr_id_refs_id_b270d7fc FOREIGN KEY (group_ptr_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: image_id_refs_file_ptr_id_014828f1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_topic
    ADD CONSTRAINT image_id_refs_file_ptr_id_014828f1 FOREIGN KEY (image_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: image_id_refs_file_ptr_id_19b6199b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_deal
    ADD CONSTRAINT image_id_refs_file_ptr_id_19b6199b FOREIGN KEY (image_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: image_id_refs_file_ptr_id_fe106e8a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_news
    ADD CONSTRAINT image_id_refs_file_ptr_id_fe106e8a FOREIGN KEY (image_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: owner_id_refs_id_a6490af8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT owner_id_refs_id_a6490af8 FOREIGN KEY (owner_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: owner_id_refs_id_a9e7dcbc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT owner_id_refs_id_a9e7dcbc FOREIGN KEY (owner_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_id_refs_id_0d5e81b6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT page_id_refs_id_0d5e81b6 FOREIGN KEY (page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_id_refs_id_88abf373; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT page_id_refs_id_88abf373 FOREIGN KEY (page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_id_refs_id_9d95001c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT page_id_refs_id_9d95001c FOREIGN KEY (page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_id_refs_id_e5b132d9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagemoderatorstate
    ADD CONSTRAINT page_id_refs_id_e5b132d9 FOREIGN KEY (page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_link_id_refs_id_5f4cbf5b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_link
    ADD CONSTRAINT page_link_id_refs_id_5f4cbf5b FOREIGN KEY (page_link_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_link_id_refs_id_71e93dc6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_picture
    ADD CONSTRAINT page_link_id_refs_id_71e93dc6 FOREIGN KEY (page_link_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: parent_id_refs_id_42b2c54f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT parent_id_refs_id_42b2c54f FOREIGN KEY (parent_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: parent_id_refs_id_8d462df0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin
    ADD CONSTRAINT parent_id_refs_id_8d462df0 FOREIGN KEY (parent_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: parent_id_refs_id_ca1f299f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT parent_id_refs_id_ca1f299f FOREIGN KEY (parent_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: placeholder_id_refs_id_0c0bdf3b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT placeholder_id_refs_id_0c0bdf3b FOREIGN KEY (placeholder_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: placeholder_id_refs_id_6e7caeb8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin
    ADD CONSTRAINT placeholder_id_refs_id_6e7caeb8 FOREIGN KEY (placeholder_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: polymorphic_ctype_id_refs_id_a03609f3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT polymorphic_ctype_id_refs_id_a03609f3 FOREIGN KEY (polymorphic_ctype_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: publisher_public_id_refs_id_ca1f299f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT publisher_public_id_refs_id_ca1f299f FOREIGN KEY (publisher_public_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: site_id_refs_id_3757c4f9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT site_id_refs_id_3757c4f9 FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: site_id_refs_id_5f61f09c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT site_id_refs_id_5f61f09c FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: snippet_id_refs_id_c4f29a99; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_snippetptr
    ADD CONSTRAINT snippet_id_refs_id_c4f29a99 FOREIGN KEY (snippet_id) REFERENCES snippet_snippet(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: source_id_refs_id_0df57a91; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT source_id_refs_id_0df57a91 FOREIGN KEY (source_id) REFERENCES easy_thumbnails_source(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: topics_id_refs_id_ea83b749; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cmsplugin_topicpluginmodel
    ADD CONSTRAINT topics_id_refs_id_ea83b749 FOREIGN KEY (topics_id) REFERENCES web_topicscollection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_1f964794; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission
    ADD CONSTRAINT user_id_refs_id_1f964794 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_2bc29acf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT user_id_refs_id_2bc29acf FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_40c41112; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_40c41112 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_4dc23c39; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_4dc23c39 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_5b2d2788; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboard
    ADD CONSTRAINT user_id_refs_id_5b2d2788 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_b89b787e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagemoderatorstate
    ADD CONSTRAINT user_id_refs_id_b89b787e FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_e927b528; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT user_id_refs_id_e927b528 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_ptr_id_refs_id_9da6953b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageuser
    ADD CONSTRAINT user_ptr_id_refs_id_9da6953b FOREIGN KEY (user_ptr_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

